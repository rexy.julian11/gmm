<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* load the MX_Router class */
class Frontend_Controller extends MY_Controller
{	
	protected $data;
	function __construct() 
	{
		parent::__construct();
		$this->output->disable_cache();
		$this->load->database();
		$this->load->model('admin_site_options/Site_options_model', 'settings');
		$this->load->library('breadcrumb');
		// $this->load->library(['ion_auth_member', 'form_validation','user_agent']);
		$this->load->library(['form_validation','user_agent']);
		
		$this->load->helper('common_element');
		$this->load->helper(['url', 'language']);
		
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth_member'), $this->config->item('error_end_delimiter', 'ion_auth_member'));
		$this->lang->load('auth');

		$this->breadcrumb->add('Home', site_url());
		$this->data['title'] = $this->lang->line('dashboard_title');
		
		$this->data['current_uri'] = $this->uri->segment(1);
		
		
		
        if ($this->ion_auth_member->logged_in()) {
			$this->data['mydata'] = $this->ion_auth_member->user()->row();
           
		}
		// $this->output->enable_profiler(TRUE);
		//grab my login detail
	}

	
	
	

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
