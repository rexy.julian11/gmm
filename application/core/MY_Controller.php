<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class MY_Controller extends MX_Controller
{	

	private $data = array();
	
	function __construct() 
	{
		parent::__construct();
		$this->_hmvc_fixes();
		
		$this->load->helper('asset');
		$this->lang->load('gt');
		$this->load->model('admin_site_options/Site_options_model','option');
		
	

		
		if($this->uri->segment(1) == 'admin' && !$this->uri->segment(2)){
			redirect('admin/dashboard');
		}
		
	
	}
	
	function _hmvc_fixes()
	{		
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
	}

	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return [$key => $value];
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 * 
	 * Render full html page
	 */
	public function _render_full_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 * 
	 * Render just partial content
	 */
	public function _render_page($page, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		
		$this->load->library('ion_auth');
		$this->load->library('ion_auth_acl');
		
		$viewdata = array_merge($this->data,$data);
		$viewdata['content'] = $page;

	
		$viewdata['breadcrumb'] = $this->breadcrumb->render();
		$viewdata['logged_user'] = $this->ion_auth->user()->row();	
		$view_html = $this->load->view('template/backend/index', $viewdata, $returnhtml);
		

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 * 
	 * Render just partial content
	 */
	public function _render_frontend_page($page, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->load->model('portofolio/portofolio_model', 'portofolio');
		$this->load->model('blog/blog_model', 'foot_blog');
		$this->load->model('home/home_model', 'foot_product');
		
		$this->load->library('ion_auth_member');
		$viewdata = (empty($data)) ? $this->data : $data;
		$viewdata['content'] = $page;
		$viewdata['breadcrumb'] = $this->breadcrumb->render();
		$viewdata['logged_user'] = $this->ion_auth_member->user()->row();
		
		$viewdata['site_opt'] = $this->settings->get_info('site_info');
		$viewdata['portofolio'] = $this->portofolio->get_all(12);
		$viewdata['foot_blog'] = $this->foot_blog->get_blogs(2, 0);
		$viewdata['foot_product'] = $this->foot_product->get_products(6);
		$view_html = $this->load->view('template/frontend/index', $viewdata, $returnhtml);
		

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
	
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
