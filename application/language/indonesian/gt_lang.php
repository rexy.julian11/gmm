<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  GT Lang - Indonesia
*
* Description:  Indonesia language file for GT views
*
*/

// Dashboard
$lang['dashboard_title'] = 'Dashboard';
$lang['welcome'] = 'Selamat Datang';

// general

$lang['profile'] = "Profil";
$lang['logout'] = "Keluar";
//action
$lang['action'] = 'Aksi';
$lang['delete'] = 'Hapus';
$lang['back'] = 'Kembali';
$lang['success_add'] = 'Berhasil menambahkan data';
$lang['success_edit'] = 'Berhasil mengubah data';
$lang['success_delete'] = 'Berhasil menghapus data';

$lang['failed_edit'] = 'Gagal mengubah data';
$lang['failed_add'] = 'Gagal menambahkan data';

//form

$lang['active'] = 'Aktif';
$lang['inactive'] = 'Tidak Aktif';
$lang['icon_find_link'] = 'Anda bisa melihat ikon dari ';
// menu

$lang['user_management'] = "Manajemen pengguna";
$lang['permissions'] = 'Perizinan';

$lang['submit_permission_group_success']    = "Grup perizinan berhasil ditambah";

//Title
$lang['permissions_categories']     = 'Kategori Perizinan';

//Days
$lang['sunday'] ='Sunday';
$lang['monday'] ='Monnday';
