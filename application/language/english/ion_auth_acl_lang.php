<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  Ion Auth ACL - English
 *
 * Version: 1.0.0
 *
 * Author: Steve Goodwin
 *		   steve@weblogics.co.uk
 *         @steveg1987
 *
 * Location: http://github.com/steve-goodwin/ion_auth_acl
 *
 * Created:  18.09.2015
 *
 * Description:  Add's ACL (access control list) based on the existing Ion Auth library for codeigniter
 *
 * Requirements: PHP5 or above
 *
 */

// Permissions
$lang['permission_key_required']                    =   'Permission key is a required field';
$lang['permission_already_exists']                  =   'Permission key is already taken';
$lang['permission_creation_successful']             =   'Permission created successfully';
$lang['permission_update_successful']               =   'Permission updated successfully';
$lang['permission_delete_unsuccessful']             =   'Unable to delete permission';
$lang['permission_already_exists']                  =   'Permission key already exists';
$lang['permission_key_admin_not_alter']             =   'Admin permission key cannot be changed';
$lang['permission_delete_notallowed']               =   'Can\'t delete the administrators\' permission';

// Create Permissions
$lang['create_permissions_title']               = 'Create Permission';
$lang['perm_key_validation_label']              = 'Permission Key';
$lang['create_permissions_btn']                 = 'Create Permission';
$lang['permissions_groups_heading']             = 'Create Permission';
$lang['perm_name_validation_label']             = 'Permission Name';

// Create Permissions Categories
$lang['create_permissions_categories_title']    = 'Create Permission Category';
$lang['cat_name_validation_label']              = 'Category Name';
$lang['category_id_validation_label']           = 'Permissions Category';
$lang['create_categories_submit']               = 'Create Permission Category';
$lang['category_name_required']                 = 'Category Name Required';
$lang['category_name_already_exist']             = 'Category Name Already Exist';
$lang['permission_categories_creation_successful']   = 'Category Added Succesfully';

// Edit Permissions Categories
$lang['edit_permissions_categories_title']      = 'Edit Permission Category';
$lang['edit_categories_submit']                 = 'Save Permission Category';
$lang['category_update_successful']             = 'Category Was Update Successfully';
$lang['categories_delete_successful']             = 'Category Was Deleted Successfully';
$lang['category_id_calidation_label']           = 'Category';


// Group Permissions
$lang['group_permissions_group_id_required']        =   'Group ID is a required field';
$lang['group_permissions_permission_id_required']   =   'Permission ID is a required field';
$lang['group_permissions_value_required']           =   'Value is a required field';
$lang['group_permission_add_unsuccessful']          =   'Permission could not be added to this group';
$lang['group_permission_delete_unsuccessful']       =   'Permission could not be removed from this group';
$lang['group_permission_delete_successful']         =   'Permission was successfully removed from this group';

// User Permissions
$lang['user_permissions_user_id_required']          =   'User ID is a required field';
$lang['user_permissions_permission_id_required']    =   'Permission ID is a required field';
$lang['user_permissions_value_required']            =   'Value is a required field';
$lang['user_permission_add_unsuccessful']           =   'Permission could not be added to this user';
$lang['user_permission_delete_unsuccessful']        =   'Permission could not be removed from this user';
$lang['user_permission_delete_successful']          =   'Permission was successfully removed from this user';
