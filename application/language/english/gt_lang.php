<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  GT Lang - English
*
* Description:  English language file for GT views
*
*/

// Dashboard
$lang['dashboard_title'] = 'Dashboard';
$lang['welcome'] = 'Welcome';

// general

$lang['profile'] = "Profile";
$lang['logout'] = "Logout";
//action
$lang['action'] = 'Action';
$lang['delete'] = 'delete';
$lang['back'] = 'Back';
$lang['success_add'] = 'Success adding data';
$lang['success_edit'] = 'Success updating data';
$lang['success_delete'] = 'Success deleting data';
$lang['success_send'] = 'You message was sent successfully';

$lang['failed_edit'] = 'Failed to update data';
$lang['failed_add'] = 'Failed to insert data';

//form

$lang['active'] = 'Active';
$lang['inactive'] = 'Not Active';
$lang['icon_find_link'] = 'You can find icons from ';
// menu

$lang['user_management'] = "User Management";
$lang['permissions'] = 'Permissions';

$lang['submit_permission_group_success']    = "Permissions group added successfully";

//Title
$lang['permissions_categories']     = 'Permissions Categories';

$lang['floors_heading']     ='Floors';


//Days
$lang['sunday'] ='Sunday';
$lang['monday'] ='Monnday';
