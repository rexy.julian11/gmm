<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Controllerslist {


    private $CI;

    private $aControllers;

    private $exclude;


    function __construct() {

        $this->CI = get_instance();
        
    }


    public function getControllers($exclude = array()) {

        $this->exclude = $exclude;
        $this->setControllers($this->exclude);
        return $this->aControllers;
    }


    public function setControllerMethods($p_sControllerName, $p_aControllerMethods) {
        $this->aControllers[$p_sControllerName] = $p_aControllerMethods;
    }

    private function setControllers() {

        foreach(glob(APPPATH . 'modules/*') as $controller) {


            if(is_dir($controller)) {

                $dirname = basename($controller);

                foreach(glob(APPPATH . 'modules/'.$dirname.'/controllers/*') as $subdircontroller) {
                    $subdircontrollername = basename($subdircontroller, EXT);
                    
                    if (! in_array(strtolower($subdircontrollername), $this->exclude)) {
                        if (!class_exists($subdircontrollername)) {
                            $this->CI->load->file($subdircontroller);
                        }

                        $f = new ReflectionClass($subdircontrollername);
                        $aUserMethods = array();
                        foreach ($f->getMethods(ReflectionMethod::IS_FINAL) as $m) {
                            //use Final keyword on every function to get list in here
                            if ($m->class == $f->name) {
                                //this is for exclude parent function
                                $aUserMethods[] = $m->name;
                            }
                        }

                    
                        // $aMethods = get_class_methods($subdircontrollername);
                        // $aUserMethods = array();
                        // foreach($aMethods as $method) {
                        //     if($method != '__construct' && $method != 'get_instance' && $method != $subdircontrollername) {
                        //         $aUserMethods[] = $method;
                        //     }
                        // }
                        $this->setControllerMethods($subdircontrollername, $aUserMethods);
                    }                                  
                }
            }
            else if(pathinfo($controller, PATHINFO_EXTENSION) == "php"){

                $controllername = basename($controller, EXT);


                if(!class_exists($controllername)) {
                    $this->CI->load->file($controller);
                }


                $aMethods = get_class_methods($controllername);
                $aUserMethods = array();
                if(is_array($aMethods)){
                    foreach($aMethods as $method) {
                        if($method != '__construct' && $method != 'get_instance' && $method != $controllername) {
                            $aUserMethods[] = $method;
                        }
                    }
                }

                $this->setControllerMethods($controllername, $aUserMethods);                                
            }
        }   
    }
}
