<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
require 'vendor/autoload.php';


if (!function_exists('sendmail')) {
    /**
     * Send Email using ci default library
     * @param array $data['action']
     * @param array $data['recipient']
     * @param array $data['booking_code']
     * @param array $data['sales_code']
     * @return bool
     * 
     * 
     */
    function sendmail($data = array()){
        $CI =& get_instance();

        
        //kirim email ke 
        
        $CI->load->library('email');
        $smtp = $CI->option->get_info('smtp');
        $CI->email->from($smtp->from_email, $smtp->from_name);
        $CI->email->to($data['recipient']);
        // $CI->email->cc('another@example.com');
        // $CI->email->bcc('and@another.com');

        $CI->email->subject($data['subject']);
        $CI->email->message($data['body']);
        
        try {
            $CI->email->send();
            return true;
        } catch (Exception $e) {
            
            log_message('error',$CI->email->print_debugger());
            return false;
        }
    }
}
?>
