<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

if (!function_exists('btn_edit')) {
    function btn_edit($uri ='',$param=''){
        return '<a href="'.site_url().$uri.$param.'" class="btn btn-icon btn-sm btn-success mr-1" title="edit"><i class="fa fa-pencil"></i></a>';
    }
}

if (!function_exists('btn_delete')) {
    function btn_delete($uri ='',$param=''){
        return '<a href="'.site_url().$uri.$param.'" class="btn btn-icon btn-sm btn-danger mr-1" title="'.lang('delete').'"><i class="fa fa-trash-o"></i></a>';
    }
}

if (!function_exists('btn_add')) {
    function btn_add($uri ='',$label='', $title='Add'){
        return '<a href="'.site_url().$uri.'" class="btn btn-primary" title="'.$title.'"><i class="fa fa-plus"></i> '.$label.' </a>';
    }
}

if (!function_exists('btn_submit')) {
    function btn_submit($label='Submit'){
        return '<button type="submit" role="button" class="btn btn-primary">'.$label.'</button>';
    }
}

if (!function_exists('btn_modal')) {
    function btn_modal($target='', $label='Submit'){
        return '<button type="button" role="button" class="btn btn-primary" data-toggle="modal" data-target="'.$target.'">'.$label.'</button>';
    }
}

if (!function_exists('btn_submit')) {
    function btn_submit($label='Submit'){
        return '<button type="submit" class="btn btn-primary">'.$label.'</button>';
    }
}

if (!function_exists('btn_back')) {
    function btn_back(){
        $ci = get_instance();
        return '<button type="button" class="btn btn-white" onClick="window.history.back()">'.$ci->lang->line('back').'</button>';
    }
}

if (!function_exists('active')) {
    function active($state = 1){
        $ci = get_instance();
        if($state == 1){
            $active = $ci->lang->line('active');
            $color = 'primary';
        }
        else if ($state == 0){
            $active = $ci->lang->line('inactive');
            $color = 'danger';
        }
        return '<span class="badge badge-'.$color.'">'.$active.'</span>';
    }
}

if (!function_exists('rupiah')) {
    function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
 
}
}


?>
