<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>404 Page Not Founds</title>
    <style type="text/css">
    @import url('https://fonts.googleapis.com/css2?family=Manrope:wght@200&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Girassol&display=swap');

    html,
    body {
        height: 100vh
    }

    body {
        display: grid;
        place-items: center;
        background: #000;
        color: #fff
    }

    .not-found {
        font-size: 120px;
        font-weight: 700;
        font-family: 'Girassol', cursive
    }

    .oops-text {
        background: #fff;
        padding: 10px;
        color: #000;
        border-radius: 5px
    }

    .not-found-text {
        font-family: 'Manrope', sans-serif;
        font-size: 20px
    }

    .send {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
        border-radius: 54px;
        font-family: 'Manrope', sans-serif;
        box-shadow: 2px 3px #dc3545;
        text-decoration: none;
        padding: 5px 15px;
    }

    .send:hover {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
        border-radius: 54px;
        box-shadow: 2px 3px #dc3545
    }

    </style>
</head>

<body>
    <div class="container-fluid text-center">
        <div class="px-5 py-5">
            <h1 class="not-found text-center">404</h1> <span class="oops-text">Oops! page not found</span>
            <h4 class="not-found-text mt-4">The page you were looking for does not exist! you may have mistyped <br> the
                address or the page may have moved</h4>
            <div class="text-center mt-4 mb-5">
                <a href='<?php echo base_url();?>' class="btn btn-success send px-3"><i
                        class="fa fa-long-arrow-left mr-1"></i> Back to homepage</a>
            </div>
        </div>
    </div>
    <!-- <div id="container">
        <h1><?php echo $heading; ?></h1>
        <?php echo $message; ?>
    </div> -->
</body>

</html>
