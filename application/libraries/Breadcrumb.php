<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Breadcrumb {

	private $breadcrumbs = array();
	private $tags = "";
	
	function __construct()
	{
	
	}

	function add($title, $href){		
		if (!$title OR !$href) return;
		$this->breadcrumbs[] = array('title' => $title, 'href' => $href);
	}
	
	
	function render(){

        $output = '';
		
		$count = count($this->breadcrumbs)-1;
		foreach($this->breadcrumbs as $index => $breadcrumb){
		
			if($index == $count){
				$output .= '<li class="breadcrumb-item active"><strong>';
				$output .= $breadcrumb['title'];
				$output .= '</strong></li>';
			}else{
				$output .= '<li class="breadcrumb-item">';
				$output .= '<a href="'.$breadcrumb['href'].'">';
				$output .= $breadcrumb['title'];
				$output .= '</a>';
				$output .= '</li>';
			}
			
		}
		
		if(!empty($this->tags['open'])){
			$output .= $this->tags['close'];
		}else{
			$output .= "</ol>";
		}		
		

		return $output;
	}

}