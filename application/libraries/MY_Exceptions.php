<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions{
    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    
    

    function show_404($page=''){    

        $this->config =& get_config();
        $base_url = $this->config['base_url'];

        $_SESSION['error_message'] = 'Error message';
        header("location: ".$base_url.'errors/error_404');
        exit;
    }
}
