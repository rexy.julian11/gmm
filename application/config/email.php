<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$CI =& get_instance();
$CI->load->model('admin_site_options/Site_options_model','option');

$smtp = $CI->option->get_info('smtp');

$config = array(
    'protocol' => 'smtp',               // 'mail', 'sendmail', or 'smtp'
    'useragent' => 'GinkTech',               // 'mail', 'sendmail', or 'smtp'
    'smtp_host' => $smtp->host, 
    'smtp_port' => $smtp->port,
    'smtp_user' => $smtp->username,
    'smtp_pass' => $smtp->password,
    'smtp_crypto' => $smtp->security,   //can be 'ssl' or 'tls' for example
    'mailtype' => $smtp->format,        //plaintext 'text' mails or 'html'
    'smtp_timeout' => '5',            //in seconds
    'charset' => 'iso-8859-1',
    'wordwrap' => TRUE,
    'newline' => "\r\n"
);
