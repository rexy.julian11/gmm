<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'admin_dashboard';
$route['404_override'] = 'errors/';
$route['translate_uri_dashes'] = TRUE;


$route['admin/(:any)'] = 'admin_$1';
$route['admin/(:any)/(:any)'] = 'admin_$1/$2';
$route['admin/(:any)/(:any)/(:any)'] = 'admin_$1/$2/$3';
