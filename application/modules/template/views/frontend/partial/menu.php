<!--::header part start::-->
<header class="main_menu home_menu">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="<?=site_url('home')?>"> <img src="<?php echo frontend_assets('shotgear')?>img/<?php echo isset($site_opt->logo) && !empty($site_opt->logo) ?
                                                                             $site_opt->logo : 'logo.png'?>"
                            alt="logo"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_icon"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url(); ?>">Beranda</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('about'); ?>">Tentang</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('services'); ?>">Layanan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('portofolio'); ?>">Portofolio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('blog'); ?>">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('contact')?>">Hubungi Kami</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url('member'); ?>">Member Area</a>
                            </li>
                        </ul>
                    </div>
                    <div class="dropdown cart">
                        <!-- <a class="dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-bag"></i>
                        </a> -->
                        <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <div class="single_product">
    
                                    </div>
                                </div> -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- Header part end-->
