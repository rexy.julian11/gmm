 <!--::footer_part start::-->
 <footer class="footer-area">
     <div class="container">
         <div class="row justify-content-between">
             <div class="col-sm-6 col-md-6 col-lg-3">
                 <div class="single-footer-widget">
                     <h4>Layanan Kami</h4>
                     <?php if (isset($foot_product) && !empty($foot_product)) :?>
                     <ul>
                         <?php foreach ($foot_product as $fp) :?>
                         <li><a
                                 href="<?=site_url('services')?>"><?php echo isset($fp->title) ? ucwords($fp->title) : NULL;?></a>
                         </li>
                         <?php endforeach;?>
                     </ul>
                     <?php endif;?>

                 </div>
             </div>
             <div class="col-sm-6 col-md-6 col-lg-3">
                 <div class="single-footer-widget footer_icon">
                     <h4>Hubungi Kami</h4>
                     <p><?php echo isset($site_opt) ? $site_opt->address : NULL?></p>
                     <ul>
                         <?php if (isset($site_opt)) :?>
                         <li><a><i class="ti-mobile"></i><?=$site_opt->phone?></a></li>
                         <li><a><i class="ti-email"></i><?=$site_opt->email?></a></li>
                         <li><a><i class="ti-world"></i> domain.com</a></li>
                         <?php endif;?>
                 </div>
             </div>
             <div class="col-sm-6 col-md-6 col-lg-3">
                 <div class="single-footer-widget footer_3">
                     <h4>Portofolio</h4>
                     <div class="footer_img">
                         <?php if ($portofolio && !empty($portofolio)) :
                            foreach ($portofolio as $p) : ?>
                         <a href="<?php echo isset($p->slug) ? site_url('portofolio/single/'.$p->slug) : NULL;?>">
                             <div class="single_footer_img">
                                 <?php 
                                        $dir = dirname(__DIR__, 6);
                                        $img = is_file($dir . "/uploads/portofolio/".$p->image) &&
                                                file_exists($dir . "/uploads/portofolio/".$p->image)
                                                ? base_url("uploads/portofolio/".$p->image)
                                                : base_url("uploads/no-image.jpg");
                                    ?>
                                 <img src="<?=$img?>" alt="<?php echo isset($p->title) ? $p->title : NULL;?>">
                             </div>
                         </a>
                         <?php endforeach; endif;?>
                     </div>
                 </div>
             </div>
             <?php if($foot_blog && !empty($foot_blog)) :?>
             <div class="col-sm-6 col-md-6 col-lg-3">
                 <div class="single-footer-widget footer_icon">
                     <h4>Artikel</h4>
                     <ul>
                         <?php foreach ($foot_blog as $fb) :?>
                         <li>
                             <img src="<?=$fb->image?>" width="20%"
                                 alt="<?php echo isset($fb->title) ? $fb->title : NULL;?>">
                             <a
                                 href="<?=site_url("blog/single/".$fb->slug)?>"><?php echo isset($fb->content) ? character_limiter($fb->content, 50) : NULL;?></a>
                         </li>
                         <?php endforeach;?>
                     </ul>
                     <!-- <div class="social_icon">
                                <a href="#"> <i class="ti-facebook"></i> </a>
                                <a href="#"> <i class="ti-twitter-alt"></i> </a>
                                <a href="#"> <i class="ti-instagram"></i> </a>
                                <a href="#"> <i class="ti-skype"></i> </a>
                            </div> -->
                 </div>
             </div>
             <?php endif;?>
             <div class="col-sm-6 col-md-6 col-lg-3">
                 <div class="single-footer-widget footer_2">
                 </div>
             </div>
         </div>
     </div>
     <div class="container-fluid">
         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="copyright_part_text text-center">
                     <p class="footer-text m-0">
                         <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                         Copyright &copy;Domain.com <script>
                         document.write(new Date().getFullYear());
                         </script> All rights reserved | This template is made with <i class="ti-heart"
                             aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                         <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                     </p>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 <!--::footer_part end::-->
