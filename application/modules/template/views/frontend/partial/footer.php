<!-- jquery plugins here-->
<script src="<?php echo frontend_assets('shotgear')?>js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="<?php echo frontend_assets('shotgear')?>js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="<?php echo frontend_assets('shotgear')?>js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<!-- swiper js -->
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.filterizr.min.js"></script>
<!-- particles js -->
<script src="<?php echo frontend_assets('shotgear')?>js/owl.carousel.min.js"></script>
<!-- slick js -->
<script src="<?php echo frontend_assets('shotgear')?>js/slick.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/swiper.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.counterup.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/waypoints.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.form.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.validate.min.js"></script>
<script src="<?php echo frontend_assets('shotgear')?>js/jquery.oLoader.min.js"></script>

<!-- Load JS Library  -->
<?php if(isset($js_library) && !empty($js_library)): ?>
<?php foreach($js_library as $key => $param):
    if(is_numeric($key) && !is_array($param)):
    ?>

<script type='text/javascript' src="<?php echo $param; ?>"></script>
<?php else : ?>
<script type='text/javascript' src="<?php echo $key?>" <?php foreach($param as $k=>$p){
    echo "$k='$p'";
}  ?>></script>
<?php endif; ?>
<?php endforeach; ?>
<?php endif; ?>


<!-- Load JS SCRIPTPAGE  -->
<?php if(isset($js_script) && !empty($js_script)): ?>
<?php foreach($js_script as $js_script){
        $this->load->view($js_script);
    }
    ?>
<?php endif; ?>


<script src="<?php echo frontend_assets('shotgear')?>js/mail-script.js"></script>
<!-- custom js -->
<script src="<?php echo frontend_assets('shotgear')?>js/custom.js"></script>
</body>

</html>
