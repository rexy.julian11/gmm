<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php if(isset($meta) && !empty($meta)): ?>
    <meta name="description" content="<?php echo $meta; ?>">
    <?php endif ?>

    <?php if(isset($keywords) && !empty($keywords)): ?>
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <?php endif ?>

    <title>Frontend</title>
    <link rel="icon" href="<?php echo frontend_assets('shotgear')?>img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/owl.carousel.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/flaticon.css">
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/swiper.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="<?php echo frontend_assets('shotgear')?>css/style.css">

    <?php if(isset($header) && !empty($header)): ?>
    <?php foreach($header as $header): ?>
    <link href="<?php echo $header; ?>" rel="stylesheet">
    <?php endforeach ?>
    <?php endif ?>
</head>
