<?php include('partial/header.php'); ?>

<body>
    <div id="wrapper">
        <?php include('partial/menu.php'); ?>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <?php include('partial/top_navbar.php'); ?>

            <!-- breadcrumb -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= isset($title) ? $title : ''; ?></h2>
                    <ol class="breadcrumb">
                        <?= (isset($breadcrumb))? $breadcrumb : ''; ?>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <!-- breadcrumb -->

            <div class="wrapper wrapper-content  animated fadeInRight">

                <?php $this->load->view($content); ?>

            </div>
            <div class="footer">
                <div class="float-right">
                    <!-- 10GB of <strong>250GB</strong> Free. -->
                </div>
                <div>
                    <strong>Copyright</strong> Gink Technology &copy; <?php echo date("Y"); ?>
                </div>
            </div>
        </div>



    </div>
    <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
    <!-- <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div> -->

    <?php include('partial/footer.php'); ?>
