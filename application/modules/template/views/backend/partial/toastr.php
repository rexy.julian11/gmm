<?php if($this->session->flashdata('message')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success("<?= $this->session->flashdata('message'); ?>",
            "<?= $this->lang->line('welcome'); ?>");

    }, 300);

});
</script>
<?php endif;?>

<?php if($this->session->flashdata('success')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success("<?= $this->session->flashdata('success'); ?>",
            "<?= $this->lang->line('success_add'); ?>");

    }, 300);
});
</script>

<?php endif;?>

<?php if($this->session->flashdata('success_edit')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success("<?= $this->session->flashdata('success_edit'); ?>",
            "<?= $this->lang->line('success_edit'); ?>");

    }, 300);
});
</script>
<?php endif;?>

<?php if($this->session->flashdata('success_delete')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success("<?= $this->session->flashdata('success_delete'); ?>",
            "<?= $this->lang->line('success_delete'); ?>");

    }, 300);
});
</script>
<?php endif;?>

<?php if($this->session->flashdata('failed')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.error("<?= $this->session->flashdata('failed'); ?>",
            "<?= $this->lang->line('failed_add'); ?>");

    }, 300);
});
</script>
<?php endif;?>

<?php if($this->session->flashdata('failed_edit')): ?>
<script>
$(document).ready(function() {
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.error("<?= $this->session->flashdata('failed_edit'); ?>",
            "<?= $this->lang->line('failed_add'); ?>");

    }, 300);
});
</script>
<?php endif;?>
