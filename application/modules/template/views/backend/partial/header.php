<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title; ?></title>
    <link href="<?php echo theme_assets('inspina') ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Data Table -->
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link
        href="<?php echo theme_assets('inspina') ?>css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
        rel="stylesheet">

    <!-- Switchery -->
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/switchery/switchery.css" rel="stylesheet">

    <!-- Select 2 -->
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/select2/select2.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo theme_assets('inspina') ?>js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/style.css" rel="stylesheet">

    <link href="<?php echo theme_assets('inspina') ?>css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/taginput/taginput.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/plugins/editable/jqueryui-editable.css" rel="stylesheet">

    <?php if(isset($header) && !empty($header)): ?>
    <?php foreach($header as $header): ?>
    <link href="<?php echo $header; ?>" rel="stylesheet">
    <?php endforeach ?>
    <?php endif ?>

</head>
