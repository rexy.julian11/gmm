<!-- Mainly scripts -->
<script src="<?php echo theme_assets('inspina') ?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/popper.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/bootstrap.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo theme_assets('inspina') ?>js/inspinia.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?php echo theme_assets('inspina') ?>js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/toastr/toastr.min.js"></script>

<!-- Switchery -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/switchery/switchery.js"></script>

<!-- Select2 -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/select2/select2.full.min.js"></script>

<!-- Data Table -->

<script src="<?php echo theme_assets('inspina') ?>js/plugins/dataTables/datatables.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

<!-- Moment JS -->
<script src="<?php echo theme_assets('inspina') ?>js/moment.min.js"></script>

<!-- Data picker -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Taginput -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/taginput/taginput.js"></script>

<!-- Bootstrap Taginput -->
<script src="<?php echo theme_assets('inspina') ?>js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/editable/jqueryui-editable.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/priceformat/jquery.priceformat.min.js"></script>
<script src="<?php echo theme_assets('inspina') ?>js/plugins/notify/notify.min.js"></script>

<!-- Load JS Library  -->
<?php if(isset($js_library) && !empty($js_library)): ?>
<?php foreach($js_library as $js_library): ?>
<script type='text/javascript' src="<?php echo $js_library?>"></script>
<?php endforeach; ?>
<?php endif; ?>


<!-- Load JS SCRIPTPAGE  -->
<?php if(isset($js_script) && !empty($js_script)): ?>
<?php foreach($js_script as $js_script){
        $this->load->view($js_script);
    }
    ?>
<?php endif; ?>


<?php include('toastr.php'); ?>


<script>
$(document).ready(function() {

    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
    if ($('.js-switch').length) {
        // Multiple switches
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function(html) {
            var switchery = new Switchery(html);
        });
    }
    $(".notif-checkbox").click(function() {
        $(".notification").toggle();
    });

    $('.datatable').DataTable({
        "scrollX": true
    });
    // $(".select2").select2();
    // $(".select2-with-tags").select2({
    //     tags:true,
    //     tokenSeparators: [',', ' ']
    // });
    $(".datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'dd MM yyyy'
    });

    $("#duration").on('change', function() {
        let duration = $(this).val();
        let checkin = $("input[name='checkin']").val();
        var checkout = moment(checkin).add(duration, 'days').format("DD MMMM YYYY");
        $("#checkout").html(checkout);
    });

    $("#checkin").on('change', function() {
        let checkin = $(this).val();
        var checkout = moment(checkin).add(duration, 'days').format("DD MMMM YYYY");
        $("#checkout").html(checkout);
    })
});
</script>
</body>

</html>
