<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle img img-responsive" style="max-width:70px"
                        src="<?php echo $avatar; ?>" />
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span
                            class="block m-t-xs font-bold"><?php echo $logged_user->first_name." ".$logged_user->last_name; ?></span>
                        <span class="text-muted text-xs block"><?php echo $logged_user->username; ?><b
                                class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item"
                                href="<?php echo site_url('admin/profile'); ?>"><?php echo $this->lang->line('profile'); ?></a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item"
                                href="<?php echo site_url('auth/logout') ?>"><?php echo $this->lang->line('logout'); ?></a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    GT
                </div>
            </li>
            <li class="<?php echo ($current_uri == 'admin/dashboard') ? 'active' : ''; ?>">
                <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-th-large"></i> <span
                        class="nav-label">Dashboard</span></a>
            </li>
            <li class="">
                <a href=""><i class="fa fa-money "></i> <span class="nav-label">Transaction</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <?php foreach ($transaction as $row) { ?>
                    <li class=""><a href="<?php echo site_url('admin/transaction/sub/').$row->ts_id?>"><?php echo $row->ts_name?></a></li>
                    <?php }?>
                </ul>
            </li>
            <?php foreach($my_menus['parent'] as $m):
                        $sub = array();
                        if(isset($my_menus['sub'][$m->menu_id])){
                            $sub = $my_menus['sub'][$m->menu_id];
                        }
                        $cek = is_in_array($sub,'url',$current_uri);
                    ?>
            <?php if($m->parent == 0): ?>

            <li class="<?php echo ($current_uri == $m->url || $cek == 1) ? 'active' : ''; ?>">
                <a href="<?php echo site_url().$m->url; ?>"><i class="fa <?php echo $m->icon; ?> "></i> <span
                        class="nav-label">
                        <?php echo $m->title; ?></span><?php echo ($m->has_child == 1 && isset($my_menus['sub'][$m->menu_id])) ? '<span class="fa arrow"></span>': ''; ?></a>
                <?php if($m->has_child == 1 && isset($my_menus['sub'][$m->menu_id])): ?>
                <ul class="nav nav-second-level collapse">
                    <?php foreach($my_menus['sub'][$m->menu_id] as $sub):?>
                    <li class="<?php echo ($current_uri == $sub->url) ? 'active' : ''; ?>"><a
                            href="<?php echo site_url(). $sub->url; ?>"><?php echo $sub->title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>
                       
            <?php if(ENVIRONMENT == 'development'):
                    //only show this feature on development stage    
                    ?>
            <li>
                <a href="<?php echo site_url("admin/modules"); ?>"><i class="fa fa-server"></i> <span
                        class="nav-label">Modules Generator</span></a>
            </li>
            <?php endif; ?>
        </ul>

    </div>
</nav>
