<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends Backend_Controller {

    public function index()
    {
        $this->data['title'] = 'Error 404';
       $this->_render_page('404', $this->data);

    }

}

/* End of file Error_404.php */
