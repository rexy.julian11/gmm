<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Error_403 extends Backend_Controller {

    public function index()
    {
        $this->data['title'] = 'Error 403';
       $this->_render_page('403', $this->data);

    }

}

/* End of file Error_404.php */
