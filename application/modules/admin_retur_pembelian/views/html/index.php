<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                Retur Pembelian
            </header>
            <div class="card-body">
                <div class="pull-right" style="margin-bottom:7px;">
                    <a href="<?php echo site_url('admin/retur_pembelian/add')?>" class="btn btn-primary">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover datatables">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Kode Retur</td>
                                <td>Tanggal</td>
                                <td>Total</td>
                                <td>Pemasok</td>
                                <td>Detail</td>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="modalDetail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Retur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                    <div class="ibox-content p-xl">
                                        <div class="table-responsive m-t">
                                            <table class="table invoice-table">
                                                <thead>
                                                    <tr>
                                                        <th>Barang</th>
                                                        <th>Harga Satuan</th>
                                                        <th>Jumlah</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="modalTblDetail">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">

                    </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
