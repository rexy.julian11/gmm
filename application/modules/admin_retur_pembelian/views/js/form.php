<script>
    $(document).ready(function(){
        var selected = [];
        Array.prototype.remove = function() {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };
        function terbilang(bilangan, sufix){
            if(bilangan=="" || bilangan==null || bilangan=="null" || bilangan==undefined){
                return "";
            } else {
                bilangan = bilangan.replace(/[^,\d]/g, '');
                var kalimat="";
                var angka   = new Array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
                var kata    = new Array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
                var tingkat = new Array('','Ribu','Juta','Milyar','Triliun');
                var panjang_bilangan = bilangan.length;
        
                /* pengujian panjang bilangan */
                if(panjang_bilangan > 15){
                    kalimat = "Diluar Batas";
                }else{
                    /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
                    for(i = 1; i <= panjang_bilangan; i++) {
                        angka[i] = bilangan.substr(-(i),1);
                    }
        
                    var i = 1;
                    var j = 0;
        
                    /* mulai proses iterasi terhadap array angka */
                    while(i <= panjang_bilangan){
                        subkalimat = "";
                        kata1 = "";
                        kata2 = "";
                        kata3 = "";
        
                        /* untuk Ratusan */
                        if(angka[i+2] != "0"){
                            if(angka[i+2] == "1"){
                                kata1 = "Seratus";
                            }else{
                                kata1 = kata[angka[i+2]] + " Ratus";
                            }
                        }
        
                        /* untuk Puluhan atau Belasan */
                        if(angka[i+1] != "0"){
                            if(angka[i+1] == "1"){
                                if(angka[i] == "0"){
                                    kata2 = "Sepuluh";
                                }else if(angka[i] == "1"){
                                    kata2 = "Sebelas";
                                }else{
                                    kata2 = kata[angka[i]] + " Belas";
                                }
                            }else{
                                kata2 = kata[angka[i+1]] + " Puluh";
                            }
                        }
        
                        /* untuk Satuan */
                        if (angka[i] != "0"){
                            if (angka[i+1] != "1"){
                                kata3 = kata[angka[i]];
                            }
                        }
        
                        /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
                        if ((angka[i] != "0") || (angka[i+1] != "0") || (angka[i+2] != "0")){
                            subkalimat = kata1+" "+kata2+" "+kata3+" "+tingkat[j]+" ";
                        }
        
                        /* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
                        kalimat = subkalimat + kalimat;
                        i = i + 3;
                        j = j + 1;
                    }
        
                    /* mengganti Satu Ribu jadi Seribu jika diperlukan */
                    if ((angka[5] == "0") && (angka[6] == "0")){
                        kalimat = kalimat.replace("Satu Ribu","Seribu");
                    }
                }
                return sufix == undefined ? kalimat : kalimat + sufix;
            }
        }
        function countTotal()
        {
            var totalBarang = 0;
            var totalJasa   = 0;
            $('.btnAddBarang').closest('div.divSection').find('.tblTotal').each(function(){
                var sub_total = parseInt($(this).html());
                totalBarang = totalBarang + sub_total ;
                $(this).closest('table').find('.grandTotal').html(totalBarang);
                $('input[name="rp_bill"]').val(totalBarang);
            });
            $('.btnAddJasa').closest('div.divSection').find('.tblTotal').each(function(){
                var sub_total = parseInt($(this).html());
                totalJasa = totalJasa + sub_total ;
                $(this).closest('table').find('.grandTotal').html(totalJasa);
                $('input[name="rp_bill"]').val(totalJasa);
            });
            var total = totalBarang + totalJasa;
            $('.inputGrandTotal').val(total);
            $('.terbilang').val(terbilang(total.toString(), 'Rupiah'));
        }
        function countsubtotal(){
            $('.input-number').unbind().change(function(){
                var qty = parseInt($(this).closest('tr').find('.qty').val());
                var price = parseInt($(this).closest('tr').find('.tblPrice').html());
                if ($(this).closest('tr').find('input').hasClass('durasi')) {
                    var durasi = parseInt($(this).closest('tr').find('.durasi').val());
                    var subtotal = qty * durasi * price;
                }
                else{
                    var subtotal = qty * price;
                }
                $(this).closest('tr').find('.tblTotal').html(subtotal);
                if (qty > 1) 
                {
                    $(this).closest('td').find('.btnMinus').attr('disabled', false);
                }else
                {
                    $(this).closest('td').find('.btnMinus').attr('disabled', true);
                }
                countTotal();
            });
            $('.btnPlus').unbind().click(function(){
                var current = $(this).closest('td').find('.input-number');
                if (current > 1) {
                    current = 1;
                }
                var new_number = parseInt(current.val()) + 1;
                current.val(new_number).trigger('change');
            }); 
            $('.btnMinus').unbind().click(function(){
                var current = $(this).closest('td').find('.input-number');
                if (current > 1) {
                    current = 1;
                }
                var new_number = parseInt(current.val()) - 1;
                current.val(new_number).trigger('change');
            });

            $('.btnRemove').unbind().click(function(){
                let item = $(this).closest('tr').find('.tdItemId').html();
                if ($(this).closest('tr').is('tr:only-child')) {
                    $(this).closest('table').find('.grandTotal').html('');
                    $('input[name="rp_bill"]').val(0);
                    selected.remove(item);
                    $(this).closest('tr').remove();
                    countTotal();
                }
                else{
                    $(this).closest('tr').remove();
                    selected.remove(item);
                    countTotal();
                }
            });

            countTotal();

            $('.btnSubmit').unbind().click(function(){
                var rp_number         = $('input[name="rp_number"]').val();
                var rp_date           = $('input[name="rp_date"]').val();
                var rp_bill           = $('input[name="rp_bill"]').val();
                var rp_date_expired   = $('input[name="rp_date_expired"]').val();
                var supplier_id       = $('.selectSupplier').val();
                var barang            = $('.tblBarang').find('tr');
                var jasa              = $('.tblJasa').find('tr');
                var data_barang = [];
                var data_jasa   = [];
                $.each(barang, function(i, item){
                    var td = $(this).find('td');
                    var item_id         = td.eq(0).html();
                    var item_buy_price  = td.eq(3).html();
                    var item_qty    = td.eq(4).find('.input-number').val();
                    var item_total  = td.eq(5).html();
                    data_barang.push({'item_id' : item_id, 'item_buy_price' : item_buy_price, 'item_qty' : item_qty, 'item_total' : item_total});
                });
                $.each(jasa, function(i, item){
                    var td = $(this).find('td');
                    var service_id   = td.eq(0).html();
                    var pos_price    = td.eq(3).html();
                    var pos_unit     = td.eq(4).html();
                    var pos_qty      = td.eq(5).find('.input-number').val();
                    var pos_duration = td.eq(6).find('.input-number').val();
                    var pos_total    = td.eq(7).html();
                    data_jasa.push({'service_id' : service_id, 'pos_price' : pos_price, 'pos_unit' : pos_unit,
                                    'pos_qty' : pos_qty, 'pos_duration' : pos_duration, 'pos_total' : pos_total
                                    })  
                });
                if (barang.length === 0 ||  supplier_id === "" || rp_date === "" || rp_date_expired === "") {
                    $.notify("Please Check Your Data", "warn");
                }
                else{
                    $.ajax({
                        url : '<?= site_url('admin/retur-pembelian/insert')?>',
                        type : 'POST',
                        data : {rp_number : rp_number,
                                rp_date   : rp_date,
                                rp_bill   : rp_bill,
                                rp_date_expired : rp_date_expired,
                                supplier_id : supplier_id,
                                data_barang : data_barang,
                                data_jasa   : data_jasa
                                }
                    }).done(function(response){
                        if (response == 'true') {
                            window.location.href = "<?= site_url('admin/retur-pembelian')?>";
                        }
                        else{
                            $.notify("Check Your Data", "warn");
                        }
                    });
                }
            });
        }
        $('.btnSubmit').unbind().click(function(){
            $.notify("Please fill the form", "warn");
        });
        $('.selectSupplier').select2({
            placeholder : 'Pilih Pemasok',
            ajax: {
                url: '<?= site_url('admin/retur-pembelian/getsupplier')?>',
                processResults: function (data) {
                return {
                    results: data
                };
                }
            }
        });
        $('.selectSupplier').unbind().change(function(){
            var supplier_id = $(this).val();
            $.ajax({
                url : '<?= site_url('admin/retur-pembelian/getsupplierdata/')?>' + supplier_id,
                type : 'GET'
            }).done(function(response){
                $('#inputNama').val(response.name);
                $('#inputTelp').val(response.phone);
                $('#inputAlamat').val(response.address);
            });
        });

        $('.selectBarang').select2({
            placeholder : 'Pilih Barang',
            ajax: {
                url: '<?= site_url('admin/retur-pembelian/getitems')?>',
                processResults: function (data) {
                return {
                    results: data
                };
                }
            }
        });
        
        $('.btnAddBarang').unbind().click(function(){
            var id = $('.selectBarang').val();
            if (selected.includes(id)) {
                $.notify("This item is already selected", "warn");
            }
            else{
                selected.push(id)
                $.ajax({
                    url : '<?= site_url('admin/retur-pembelian/getitemdata/')?>' + id,
                    type : 'GET'
                }).done(function(response){
                    var html = `
                        <tr>
                            <td style="display:none" class="tdItemId">${response.item_id}</td>
                            <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                            <td>${response.item_name}</td>
                            <td class="tblPrice">${response.item_buy_price}</td>
                            <td><div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btnMinus" disabled="disabled">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" name="" class="form-control input-number qty" value="1" min="1" max="10">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btnPlus">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div></td>
                            <td class="tblTotal pull-right">${response.item_buy_price}</td>
                        </tr>
                    `;
                    $('.tblBarang').append(html);
                    countsubtotal();

                
                });
            }
        });

    }); 
</script>