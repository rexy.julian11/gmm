<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_retur_pembelian/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columnDefs: [
                                { "searchable": false, 
                                  "targets"   : 0 }
                            ],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    
                    {"data": "id"},
                    {"data": "rp_date"},
                    {"data": "rp_bill"},
                    {"data": "supplier_name"},
                    {
                        "className":      'detail',
                        "orderable":      false,
                        "data":           'detail',
                        "defaultContent": ''
                    },
                ], 
            });
            
            $('.datatables tbody').on('click', 'td.detail', function () {
                var tr = $(this).closest('tr');
                var row = datatable.row( tr );
                var id = row.data().id;
                $.ajax({
                    url : '<?= site_url('admin/retur-pembelian/getreturdetail/')?>' + id,
                    type : 'GET'
                }).done(function(r){
                    var  html = ``;
                    $('#modalTblDetail').html('');
                    $.each(r, function(i, data){
                        html += `
                                    <tr>
                                        <td>${data.item_name}</td>
                                        <td>${data.item_buy_price}</td>
                                        <td>${data.item_qty}</td>
                                        <td>${data.item_total}</td>
                                    </tr>
                                `;
                    });
                    $('#modalTblDetail').append(html);
                    $('#modalDetail').modal('show');  
        
                });
            });
    
        });
        </script>