<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function get_my_widgets(){
        $mygroups = $this->ion_auth->get_users_groups()->row();
        
       
        $this->db->select('w.*');
        $this->db->from('widgets as w');
        $this->db->join('group_widget as gw', 'gw.widget_id = w.id');
        $this->db->where('gw.group_id',$mygroups->id);
        $this->db->where('gw.active',1);
        return $this->db->get()->result();

    }
}

/* End of file Model_name.php */
