<?php echo form_open_multipart(); ?>
<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>

                </div>
                <div class='ibox-content'>
                    <div class="form-group">
                        <label for="foto" class="control-label">Foto <span class="text-danger">*</span></label>
                        <div class="custom-file">
                            <input id="logo" type="file" name="foto" class="custom-file-input">
                            <label for="logo" class="custom-file-label">Pilih</label>
                        </div>
                        <?php echo form_error('foto', '<div class="error" style="color:red">','</div>'); ?>

                    </div>
                </div>
                <div class='ibox-footer'>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
