<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_dashboard extends Backend_Controller
{

    public function index()
    
    {
       
        $this->load->model('Dashboard_model');
        
        $this->data['my_widgets'] = $this->Dashboard_model->get_my_widgets();
        
        $this->load->module('widget/Widget');
        $js = array();
        foreach($this->data['my_widgets'] as $w){
            $name = $w->name;
            if(file_exists(APPPATH.'modules/widget/views/js/'.$name.'.php')){
                array_push($js,'widget/js/'.$name);
            }
        }
        $this->data['js_script'] = $js;
        
        
        $this->data['title'] = 'Dashboard';
        $this->_render_page('admin_dashboard/index', $this->data, false);
    }

    
}

/* End of file Admin_dashboard.php */
