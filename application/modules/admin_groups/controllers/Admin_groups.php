<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin_groups extends Backend_Controller
{
    protected $table;
    protected $class;
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->breadcrumb->add('Groups', base_url('admin_groups'));
        $this->tables = $this->config->item('tables', 'ion_auth');
    }

    public function datatables()
    {
        if ($this->input->server('REQUEST_METHOD')=='POST') {
            $matrix = "<button data-toggle='modal' data-target='#settingPermissions-$1' class='m-2 btn btn-icon btn-primary btn-sm' title='Setting Permissions'><i class='fa fa-sliders'></i></button>";
            $edit = btn_edit('admin/groups/edit/', '$1');
            $delete = btn_delete('admin/groups/delete/', '$1');
            $this->load->library('Datatables');
            $this->datatables->select('id,name, description');
            $this->datatables->from('groups');
            $this->datatables->add_column(
                'action',
                $matrix.$edit.$delete,
                "id"
            );
            $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output($this->datatables->generate());
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_status_header(401)
            ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    final public function index()
    {
        //get permissions for each method
        if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
            redirect('errors/error_403', 'refresh');
        }

        $this->data['title'] = $this->lang->line('groups_heading');
        $this->data['groups'] = $this->ion_auth->groups()->result();

        $this->data['js_script'] = array('js/js','js/datatables');
        $this->data['perm_categories'] = $this->ion_auth_acl->permissions_categories('full');


        $this->_render_page('admin_groups/html/index', $this->data);
    }

    final public function add()
    {

//get permissions for each method
        if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
            redirect('errors/error_403', 'refresh');
        }

        $this->data['title'] = $this->lang->line('index_create_group_link');
        $this->breadcrumb->add('Add', base_url('admin_groups/add'));

        $this->form_validation->set_rules(
            'name',
            $this->lang->line('create_group_validation_name_label'),
            'trim|required|alpha_dash|is_unique[' . $this->tables['groups'] . '.name]'
        );
        $this->form_validation->set_rules(
            'description',
            $this->lang->line('create_group_validation_desc_label'),
            'trim|required'
        );


        if ($this->form_validation->run() == true) {
            # code...
            $data = $this->security->xss_clean($this->input->post());
            if ($this->ion_auth->create_group($data['name'], $data['description'])) {
                $this->session->set_flashdata('success', $this->ion_auth->messages());
                redirect('admin/groups', 'refresh');
            }
        } else {
            # code...
            $this->_render_page('admin_groups/html/form', $this->data);
        }
    }

    final public function edit($id)
    {

//get permissions for each method
        if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
            redirect('errors/error_403', 'refresh');
        }
        if ($id == null) {
            //redirect user if there is no $id
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->data['title'] = $this->lang->line('edit_group_title');
        $this->breadcrumb->add('edit', site_url('admin_groups/edit/'.$id));
        if (!$this->ion_auth->is_admin() && !($this->ion_auth->group()->row()->id == $id)) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->form_validation->set_rules(
                'name',
                $this->lang->line('edit_group_validation_name_label'),
                'trim|required|alpha_dash'
            );
            $this->form_validation->set_rules(
                'description',
                $this->lang->line('edit_group_validation_desc_label'),
                'trim|required'
            );

            $this->data['group'] = $this->ion_auth->group($id)->row();

            if ($this->form_validation->run() == false) {
                # code...
                $this->_render_page('admin_groups/html/form', $this->data);
            } else {
                # code...
                $data = $this->security->xss_clean($this->input->post());

                $group_update = $this->ion_auth->update_group($data['id'], $data['name'], array(
                                'description' => $data['description']
                                ));
                if ($group_update) {
                    $this->session->set_flashdata('success_edit', $this->ion_auth->messages());
                    redirect('admin/groups', 'refresh');
                } else {
                    $this->session->set_flashdata('failed_edit', $this->ion_auth->errors());
                    redirect('admin_groups/edit/'.$id, 'refresh');
                }
            }
        }
    }

    final public function delete($id)
    {
        //get permissions for each method
        if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
            redirect('errors/error_403', 'refresh');
        }

        if ($id == null) {
            //redirect user if there is no $id
            redirect($_SERVER['HTTP_REFERER']);
        }

        if ($this->ion_auth->delete_group($id)) {
            $this->ion_auth_acl->remove_group_from_all_permission($id);
            $this->session->set_flashdata('success_delete', $this->ion_auth->messages());
            redirect('admin/groups', 'refresh');
        }
    }

    public function _nameCheck($str)
    {
        if ($this->ion_auth->groupname_check($str)) {
            return true;
        } else {
            return false;
        }
    }

    final public function update_permission()
    {
        //get permissions for each method
        if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
            redirect('errors/error_403', 'refresh');
        }

        $perm_id = $this->input->post('permission');
        $group_id = $this->input->post('group');
        $val = $this->input->post('val');

        if ($val == "") {
            $value = 1;
        } else {
            $value = 0;
        }

        $result = array();
        $result['value'] = $value;
        if ($this->ion_auth_acl->update_group_permissions($perm_id, $group_id, $value)) {
            $result['status'] = 'OK';
        } else {
            $result['status'] = 'Failed';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}

/* End of file Controllername.php */
