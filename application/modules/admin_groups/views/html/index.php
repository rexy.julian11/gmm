<div class="page-content">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?php echo btn_add('admin/groups/add',$this->lang->line('index_create_group_link')) ?>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped" id="groups">
                            <thead>
                                <th>No</th>
                                <th><?php echo  $this->lang->line('create_group_validation_name_label'); ?></th>
                                <th><?php echo  $this->lang->line('create_group_validation_desc_label'); ?></th>
                                <th><?php echo $this->lang->line('action'); ?></th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- end row -->

</div>

<?php foreach($groups as $g): ?>
<div class="modal inmodal" id="settingPermissions-<?php echo $g->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xlg modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <p class="text-center">
                <h4 class="modal-title">Permissions Setting</h4>
                <small>Update permissions for Administrator</small>
                </p>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th>Group</th>
                            <th>Permissions</th>
                        </thead>
                        <tbody>
                            <?php 
                                $group_permissions = $this->ion_auth_acl->get_group_permissions($g->id);
                                

                                foreach($perm_categories as $cat): 
                                    $permissions = $this->ion_auth_acl->permissions_by_category($cat['id']);
                                    
                           ?>
                            <tr>
                                <td><?php echo $cat['name']; ?></td>
                                <td>
                                    <div class="row">

                                        <?php 
                                        arsort($permissions);
                                        foreach($permissions as $perm): 
                                            $permission = $this->ion_auth_acl->has_permission($perm->perm_key, $group_permissions);
                                            
                                            $checked = "";
                                            if($permission == 1){
                                                $checked = "checked";
                                            }
                                           
                                        ?>
                                        <div class="col-3 mb-3 "><input data-permission="<?php echo $perm->id; ?>"
                                                value="<?php echo $permission; ?>" data-group="<?php echo $g->id; ?>"
                                                type="checkbox" class="js-switch permission_switch"
                                                <?php echo $checked; ?> />
                                            <?php echo $perm->perm_name; ?></div>
                                        <?php endforeach; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
