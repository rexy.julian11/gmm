<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><?php echo $title; ?></h5><br>
                    <span><?php echo $this->lang->line('create_group_subheading');?></span>
                </div>
                <?php echo form_open(); ?>
                <div class="ibox-content">
                    <?php if(isset($group)): ?>
                    <input type="hidden" name="id" value="<?php echo $group->id; ?>">
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="name"><?php echo $this->lang->line('create_group_name_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input placeholder="<?php echo $this->lang->line('create_group_validation_name_label'); ?>"
                            name="name" id="" class="form-control" autofocus
                            value="<?php echo isset($group) ? $group->name : set_value("name"); ?>">
                        <?php echo form_error('name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <div class="form-group">
                        <label for="description"><?php echo $this->lang->line('create_group_desc_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input placeholder="<?php echo $this->lang->line('create_group_validation_desc_label'); ?>"
                            name="description" id="" class="form-control"
                            value="<?php echo isset($group) ? $group->description :set_value("description"); ?>">
                        <?php echo form_error('description', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>


                </div>
                <div class="ibox-footer">
                    <button type="button" class="btn btn-white"
                        onClick="window.history.back()"><?php echo $this->lang->line('back'); ?></button>
                    <?php echo btn_submit(isset($group) ? $this->lang->line('edit_group_submit_btn') : $this->lang->line('create_group_submit_btn')); ?>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
