<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
var groups = null;
$(document).ready(function() {
    groups = $('#groups').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "ajax": {
            "url": "<?php echo site_url('admin_groups/datatables'); ?>",
            "type": "POST"
        },
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [
            [1, 'asc']
        ],
        "columns": [{
                "data": '',
                "sortable": false,
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                "data": "name"
            },
            {
                "data": "description"
            },
            {
                "data": "action",
                "sortable": false
            }
        ],

    });
});
</script>
