<script>
$(document).ready(function() {
    let name = $("#formCreateGroup").serialize();
    let btn = $("#createGroupForm").find('#submit');
    btn.on('click', function() {
        console.log(name);
    });

    $(".permission_switch").on('change', function() {
        let permission = $(this).data('permission');
        let group = $(this).data('group');
        let val = $(this).val();
        $.ajax({
            url: "<?php echo base_url(); ?>" + 'admin_groups/update_permission',
            data: {
                permission: permission,
                group: group,
                val: val
            },
            type: "POST",
            success: function(e) {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                if (e.value == 1) {
                    toastr.success("Access was granted successfully",
                        "Access Permission changed");
                } else {
                    toastr.error("Access was terminated",
                        "Access Permission changed");
                }
                $(this).val(e.value);
            }
        });
    });
});
</script>
