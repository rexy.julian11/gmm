<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_account extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_account_model');
        $this->breadcrumb->add('Admin account',base_url().'admin_account');
        $this->data['title'] = 'Admin account';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_account/js/datatables'); //load aditional js script
        $this->data['group'] = $this->Admin_account_model->getDataAccount();
        $this->_render_page('admin_account/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add($parent_number)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        $this->data['header'] = array('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css'); //load aditional stylesheets
        $this->data['js_library'] = array('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js'); //load aditional js library


        $this->load->library('form_validation');
        $this->form_validation->set_rules('account_number', 'Account Number', 'required|is_unique[accounts.account_number]');
        $this->form_validation->set_rules('account_name', 'Account Name', 'required');
        // $this->form_validation->set_rules('account_primary', 'Account Primary', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->data['parents'] = $this->Admin_account_model->getDataParent();
            $this->_render_page('admin_account/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if (isset($data['account_primary'])) {
                $data['account_primary'] = 1;
            }
            $data['parent_number'] = str_replace("_","-",$parent_number);
            $data['group_number'] = $parent_number[0];
            if ($this->Admin_account_model->insert($data)) {
                $this->session->set_flashdata('success', 'Successfull create account');
                redirect('admin/account', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/account/form', 'refresh');
            }
        
        }
        

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->data['header'] = array('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css'); //load aditional stylesheets
        $this->data['js_library'] = array('https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js'); //load aditional js library


        $this->load->library('form_validation');
        $this->form_validation->set_rules('account_number', 'Account Number', 'required');
        $this->form_validation->set_rules('account_name', 'Account Name', 'required');
        // $this->form_validation->set_rules('account_primary', 'Account Primary', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->data['accounts'] = $this->Admin_account_model->getData(str_replace("_","-",$id));
            $this->data['parents'] = $this->Admin_account_model->getDataParent();
            $this->_render_page('admin_account/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if (isset($data['account_primary'])) {
                $data['account_primary'] = 1;
            }
            if ($this->Admin_account_model->update(str_replace("_","-",$id), $data)) {
                $this->session->set_flashdata('success', 'Successfull update account');
                redirect('admin/account', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/account/form', 'refresh');
            }
        
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }
        $account = $this->Admin_account_model->getData($id);
        if ($account->account_primary == 0) {
            if ($this->Admin_account_model->delete($id)) {
                $this->session->set_flashdata('success', 'Successfull delete account');
                redirect('admin/account', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/account', 'refresh');
            }
        }
        else{
            $this->session->set_flashdata('failed', 'This data can not be deleted');
            redirect('admin/account', 'refresh');
        }
    }
    
}