<div class="page-content">
    <div class="row">
        <div class="col-md-6">
        <?php 
        $i = 0;
        foreach($group AS $row){
            $i++;
            if($i == 4){
                echo '</div><div class="col-md-6">';
            }
        ?>
            <div class="card mb-3">
                <div class="card-header"><?= $row->account_name;?></div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr class="table-secondary">
                                    <th scope="col" colspan='2'>Name</th>
                                    <th scope="col">Normal Balance</th>
                                    <th scope="col"><a href="<?php echo site_url('admin/account/add/').$row->account_number?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($row->control as $row_a) { ?>
                                <tr>
                                    <td colspan='2'><?php echo $row_a->account_name?></td>
                                    <td><?php echo $row_a->account_normal_balance?></td>
                                    <td>
                                        <a href="<?php echo site_url('admin/account/edit/').$row_a->account_number?>" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <?php if ($row_a->account_primary != 1) { ?>
                                            <a href="<?php echo site_url('admin/account/delete/').$row_a->account_number?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        <?php } ?>
                                        <a href="<?php echo site_url('admin/account/add/').$row_a->account_number?>" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                    <?php foreach ($row_a->account as $row_b) { ?>
                                        <tr>
                                                <td></td>
                                                <td><?php echo $row_b->account_name;?></td>  
                                                <td><?php echo $row_b->account_normal_balance;?></td>  
                                                <td><a href="<?php echo site_url('admin/account/edit/').$row_b->account_number?>" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <?php if ($row_b->account_primary != 1) { ?>
                                                        <a href="<?php echo site_url('admin/account/delete/').$row_b->account_number?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    <?php } ?>
                                                </td>
                                        </tr>
                                    <?php } ?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>

