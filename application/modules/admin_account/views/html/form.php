<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Account Form    
                </div>
                <div class="card-body">
                    <?php echo form_open()?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Number *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="account_number" value="<?php echo isset($accounts) ? $accounts->account_number : ''?>" <?php echo isset($accounts) ? 'readonly' : ''?>>
                                <?php echo form_error('account_number', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="account_name" value="<?php echo isset($accounts) ? $accounts->account_name : ''?>">
                                <?php echo form_error('account_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Normal Balance *</label>
                            <div class="col-sm-3">
                                <select name="account_normal_balance" class="form-control" id="">
                                    <!-- <option selected disabled>Select Normal Balance</option> -->
                                    <option value="Db" <?php echo isset($accounts) && $accounts->account_normal_balance == 'Db' ? 'selected' : ''?>>Db</option>
                                    <option value="Cr" <?php echo isset($accounts) && $accounts->account_normal_balance == 'Cr' ? 'selected' : ''?>>Cr</option>
                                </select>
                            </div>
                        </div>
                        <?php if (!isset($accounts)) { ?>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Account Type *</label>
                                <div class="col-sm-3">
                                    <select name="account_type" class="form-control" id="">
                                        <!-- <option selected disabled>Select Account Type</option> -->
                                        <option value="Account" <?php echo isset($accounts) && $accounts->account_normal_balance == 'Account' ? 'selected' : ''?>>Account</option>
                                        <option value="Group" <?php echo isset($accounts) && $accounts->account_normal_balance == 'Group' ? 'selected' : ''?>>Group</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Account Primary</label>
                                <div class="col-sm-3">
                                    <input type="checkbox" name="account_primary" <?php isset($accounts) ? 'checked' : ''?> data-toggle="toggle" data-on="Not Deletable" data-off="Deletable" data-size="sm">
                                </div>
                            </div>
                        <?php }else{?>
                            
                        <?php }?>
                        
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button class="btn btn-primary pull-right" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>

