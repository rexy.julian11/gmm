<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_account_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getDataAccount(){
        $group = $this->db->where('account_type', 'Group')
                          ->get('accounts')->result();
        foreach ($group as $key => $row) {
            $control = $this->db->where('account_type', 'Account')
                                ->where('parent_number', $row->account_number)
                                ->get('accounts')->result();
            $group[$key]->control = $control;
            foreach ($control as $keys => $row_a) {
                $account = $this->db->where('account_type', 'Account')
                                    ->where('parent_number', $row_a->account_number)
                                    ->get('accounts')->result();
                $group[$key]->control[$keys]->account = $account;
            }
        }
        return $group;
    }
    public function getDataParent()
    {
        $this->db->where('account_type', 'Group');
        return $this->db->get('accounts')->result();
    }
    public function insert($data)
    {
        return $this->db->insert('accounts', $data);
    }
    public function getData($account_number)
    {
        $this->db->where('account_number', $account_number);
        return $this->db->get('accounts')->row();
    }
    public function update($account_number, $data)
    {
        $this->db->where('account_number', $account_number);
        return $this->db->update('accounts', $data);
    }
    public function delete($account_number)
    {
        $this->db->where('account_number', $account_number);
        return $this->db->delete('accounts');
    }
}