<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_journal_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getAccounts()
    {
        $this->db->select('account_number as id, account_name as text')
                 ->where('account_type', 'Account');
        return $this->db->get('accounts')->result();
    }
    public function insertJournal($data)
    {
        $this->db->insert('journals', $data);
        return $this->db->insert_id();
    }
    public function insertLedger($data)
    {
        return $this->db->insert('ledgers', $data);
    }
    public function getDataJournal($journal_id)
    {
        $journal = $this->db->where('journal_id', $journal_id)
                            ->get('journals')->row();
        $ledger  = $this->db->where('journal_id', $journal_id)
                            ->order_by('ledger_id', 'ASC')
                            ->get('ledgers')->result();
        $journal->ledger = $ledger;

        return $journal;
    }
    public function updateJournal($journal_id, $data)
    {
        $this->db->where('journal_id', $journal_id);
        return $this->db->update('journals', $data);
    }
    public function updateLedger($ledger_id, $data)
    {
        $this->db->where('ledger_id', $ledger_id);
        return $this->db->update('ledgers', $data);
    }
    public function getDataLedger($journal_id)
    {
        $this->db->where('journal_id', $journal_id);
        return $this->db->get('ledgers')->result();
    }
    public function deleteJournal($journal_id)
    {
        $this->db->where('journal_id', $journal_id);
        return $this->db->delete('journals');
    }
    public function deleteLedger($ledger_id)
    {
        $this->db->where('ledger_id', $ledger_id);
        return $this->db->delete('ledgers');
    }
    public function deleteLedgerByJournal($journal_id)
    {
        return $this->db->where('journal_id', $journal_id)
                        ->delete('ledgers');  
    }
}