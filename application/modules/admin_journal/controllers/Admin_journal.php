<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_journal extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_journal_model');
        $this->breadcrumb->add('Admin journal',base_url().'admin_journal');
        $this->data['title'] = 'Admin journal';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_journal/js/datatables'); //load aditional js script
        $this->_render_page('admin_journal/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('journal_id as id, journal_datetime, journal_description, journal_memo, journal_status');
            $this->datatables->from('journals');
            $this->datatables->add_column('ledger', function($row){
                $ledger = $this->db->select_sum('ledger_amount')
                                   ->where('journal_id', $row['id'])
                                   ->where('ledger_increase_to', 'Db')
                                   ->get('ledgers')->row();
                return $ledger->ledger_amount;
            });
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="journal/edit/$1" title="edit">
                 <i class="fa fa-pencil">
                 </i>
                 <a class="btn btn-icon btn-sm btn-danger mr-1" href="journal/delete/$1" title="delete">
                 <i class="fa fa-trash-o">
                 </i>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('journal_description', 'Journal Description', 'required');
        $this->form_validation->set_rules('journal_memo', 'Journal Memo', 'trim');

        if ($this->form_validation->run() == FALSE)
        {
            $this->data['js_script'] = array('admin_journal/js/index'); //load aditional js script
            $this->data['accounts'] = $this->Admin_journal_model->getAccounts();
            $this->_render_page('admin_journal/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            
            $insertJournal['journal_description']   = $data['journal_description'];
            $insertJournal['journal_memo']          = $data['journal_memo'];
            if ($journal = $this->Admin_journal_model->insertJournal($insertJournal)) {
                foreach ($data['ledger'] as $key => $row) {
                    $insertLedger['journal_id']         = $journal;
                    $insertLedger['account_number']     = $row['account_number'];
                    if (empty($row['increase_cr'])) {
                        $insertLedger['ledger_increase_to'] = 'Db';
                        $insertLedger['ledger_amount']      = (float)$row['increase_db'];
                    }
                    else{
                        $insertLedger['ledger_increase_to'] = 'Cr';
                        $insertLedger['ledger_amount']      = (float)$row['increase_cr'];
                    }
                    $this->Admin_journal_model->insertLedger($insertLedger);
                }
                $this->session->set_flashdata('success', 'Successfull create journal');
                redirect('admin/journal', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/journal/form', 'refresh');
            }
        }
    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('journal_description', 'Journal Description', 'required');
        $this->form_validation->set_rules('journal_memo', 'Journal Memo', 'trim');

        if ($this->form_validation->run() == FALSE)
        {
            $this->data['js_script'] = array('admin_journal/js/index'); //load aditional js script
            $this->data['accounts'] = $this->Admin_journal_model->getAccounts();
            $this->data['journal'] = $this->Admin_journal_model->getDataJournal($id);
            $this->_render_page('admin_journal/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();   
            $updateJournal['journal_description']   = $data['journal_description'];
            $updateJournal['journal_memo']          = $data['journal_memo'];
            if ($journal = $this->Admin_journal_model->updateJournal($id, $updateJournal)) {
                $this->Admin_journal_model->deleteLedgerByJournal($id);
                foreach ($data['ledger'] as $key => $row) {
                    $updateLedger['journal_id']         = $id;
                    $updateLedger['account_number']     = $row['account_number'];
                    if (empty($row['increase_cr'])) {
                        $updateLedger['ledger_increase_to'] = 'Db';
                        $updateLedger['ledger_amount']      = (float)$row['increase_db'];
                    }
                    else{
                        $updateLedger['ledger_increase_to'] = 'Cr';
                        $updateLedger['ledger_amount']      = (float)$row['increase_cr'];
                    }
                    $this->Admin_journal_model->insertLedger($updateLedger);
                    
                }
                $this->session->set_flashdata('success', 'Successfull update journal');
                redirect('admin/journal', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/journal/form', 'refresh');
            }
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }
        if ($journal = $this->Admin_journal_model->deleteJournal($id)) {
            $ledgers = $this->Admin_journal_model->getDataLedger($id);
            foreach ($ledgers as $row) {
                $this->Admin_journal_model->deleteLedger($row->ledger_id);
            }
            $this->session->set_flashdata('success', 'Successfull delete journal');
            redirect('admin/journal', 'refresh');
        }
        else{
            $this->session->set_flashdata('failed', 'Something error');
            redirect('admin/journal', 'refresh');
        }
        
    }
    public function getAccounts()
    {
        $accounts = $this->Admin_journal_model->getAccounts();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($accounts));
    }
    
    
}