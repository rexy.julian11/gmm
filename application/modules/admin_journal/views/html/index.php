<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                Tickets
            </header>
            <div class="card-body">
                <div class='pull-right' style="margin-bottom:10px;">
                    <a href="<?php echo site_url('admin/journal/add') ?>" class="btn btn-primary">Add New Journal <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="adv-table" style="margin-top:5px;">
                    <div class="table-responsive">
                        <table class="display table table-bordered table-striped" id="journal">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Date</td>
                                    <td>Description</td>
                                    <td>Amount</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                        </table>        
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
