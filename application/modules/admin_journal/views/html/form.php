<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Journal Entry
                </div>
                <div class="card-body">
                    <?php 
                        $attributes = array('id' => 'journal');
                        echo form_open('', $attributes);?>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="journal_description" value="<?php echo isset($journal) ? $journal->journal_description : '' ;?>">
                                <?php echo form_error('journal_description', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="accounts-group">
                            <div class="form-group row accounts">
                                <label for="" class="col-sm-2 col-form-label">Accounts</label>
                                <div class="col-sm-5">
                                    <?php if (isset($journal)) { ?>
                                        <input type="hidden" name="ledger[0][ledger_id]" value="<?php echo $journal->ledger[0]->ledger_id?>">
                                    <?php } ?>
                                    <select name="ledger[0][account_number]" class="form-control selectAccounts" required> 
                                        <option selected disabled>--Select Accounts--</option>
                                        <?php foreach ($accounts as $row) { ?>
                                            <option value="<?php echo $row->id?>" <?php echo isset($journal) && $journal->ledger[0]->account_number == $row->id ? 'selected' : ''?>><?php echo $row->text?></option>
                                        <?php }?>    
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control form-control-sm formDb" name="ledger[0][increase_db]" placeholder="Db" value="<?php echo isset($journal) ? (int)$journal->ledger[0]->ledger_amount : ''?>">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control form-control-sm formCr" name="ledger[0][increase_cr]" placeholder="Cr" disabled>
                                </div>
                                <div class="col-sm-1">
                                    <!-- <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button> -->
                                </div>
                            </div>
                            <div class="form-group row accounts">
                                <label for="" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-5">
                                    <?php if (isset($journal)) { ?>
                                        <input type="hidden" name="ledger[1][ledger_id]" value="<?php echo $journal->ledger[1]->ledger_id?>">
                                    <?php } ?>
                                    <select name="ledger[1][account_number]" class="form-control selectAccounts" required>
                                        <option selected disabled>--Select Accounts--</option>
                                        <?php foreach ($accounts as $row) { ?>
                                            <option value="<?php echo $row->id?>" <?php echo isset($journal) && $journal->ledger[1]->account_number == $row->id ? 'selected' : ''?>><?php echo $row->text?></option>
                                        <?php }?>    
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control form-control-sm formDb" name="ledger[1][increase_db]" placeholder="Db" disabled>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control form-control-sm formCr" name="ledger[1][increase_cr]" placeholder="Cr" value="<?php echo isset($journal) ? (int)$journal->ledger[1]->ledger_amount : ''?>">
                                </div>
                                <div class="col-sm-1">
                                    <!-- <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button> -->
                                </div>
                            </div>
                        <?php if (isset($journal) && count($journal->ledger) > 2) { 
                            for ($i=2; $i < count($journal->ledger) ; $i++) { 
                                if ($journal->ledger[$i]->ledger_increase_to == 'Db') { ?>
                                    <div class="form-group row accounts">
                                        <label for="" class="col-sm-2 col-form-label"></label>
                                        <div class="col-sm-5">
                                            <?php if (isset($journal)) { ?>
                                                <input type="hidden" name="ledger[<?php echo $i?>][ledger_id]" value="<?php echo $journal->ledger[$i]->ledger_id?>">
                                            <?php } ?>
                                            <select name="ledger[<?php echo $i?>][account_number]" class="form-control selectAccounts" required>
                                                <option selected disabled>--Select Accounts--</option>
                                                <?php foreach ($accounts as $row) { ?>
                                                    <option value="<?php echo $row->id?>" <?php echo isset($journal) && $journal->ledger[$i]->account_number == $row->id ? 'selected' : ''?>><?php echo $row->text?></option>
                                                <?php }?>    
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm formDb" name="ledger[<?php echo $i?>][increase_db]" placeholder="Db" value="<?php echo isset($journal) ? (int)$journal->ledger[$i]->ledger_amount : ''?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm formCr" name="ledger[<?php echo $i?>][increase_cr]" placeholder="Cr" disabled>
                                        </div>
                                        <div class="col-sm-1">
                                            <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="form-group row accounts">
                                        <label for="" class="col-sm-2 col-form-label"></label>
                                        <div class="col-sm-5">
                                            <?php if (isset($journal)) { ?>
                                                <input type="hidden" name="ledger[<?php echo $i?>][ledger_id]" value="<?php echo $journal->ledger[$i]->ledger_id?>">
                                            <?php } ?>
                                            <select name="ledger[<?php echo $i?>][account_number]" class="form-control selectAccounts" required>
                                                <option selected disabled>--Select Accounts--</option>
                                                <?php foreach ($accounts as $row) { ?>
                                                    <option value="<?php echo $row->id?>" <?php echo isset($journal) && $journal->ledger[$i]->account_number == $row->id ? 'selected' : ''?>><?php echo $row->text?></option>
                                                <?php }?>    
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm formDb" name="ledger[<?php echo $i?>][increase_db]" placeholder="Db" disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control form-control-sm formCr" name="ledger[<?php echo $i?>][increase_cr]" placeholder="Cr" value="<?php echo isset($journal) ? (int)$journal->ledger[$i]->ledger_amount : ''?>">
                                        </div>
                                        <div class="col-sm-1">
                                            <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                <?php }
                             }    
                        }?>
                        </div>
                        <div class="form-group">
                            <div class="row justify-content-end">
                                <div class="col-sm-2">
                                    <select class="form-control" id="addRow">
                                        <option value="1">Db</option>
                                        <option value="2">Cr</option>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-success pull-right btnAdd" type="button">Add <i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Memo</label>
                            <div class="col-sm-5">
                                <textarea name="journal_memo" class="form-control"><?php echo isset($journal) ? $journal->journal_memo : ''?></textarea>
                                <?php echo form_error('journal_memo', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                            <div class="col-sm-2">
                                <div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                                    <div class="card-header">Db Total</div>
                                    <div class="card-body">
                                         Rp. <span class="dbTotal"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                                    <div class="card-header">Cr Total</div>
                                    <div class="card-body">
                                         Rp. <span class="crTotal"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row justify-content-end">
                                <div class="col-sm-1">
                                    <button class="btn btn-primary pull-right" id="btnSubmit" type="submit" disabled>Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>