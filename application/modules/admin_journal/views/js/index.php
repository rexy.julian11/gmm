<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function(){
        function numberFormat(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
        function sum(){
            $('.formDb').unbind().keyup(function(){
                var a = 0;
                var b = 0;
                $('.formDb').each(function(){
                    if ($(this).val() === ""){
                        a = a + 0;
                    }
                    else{
                        a = a + parseInt($(this).val());
                    }
                });
                $('.formCr').each(function(){
                    if ($(this).val() === "") {
                        b = b + 0;
                    }
                    else{
                        b = b + parseInt($(this).val());
                    }
                });
                $('.dbTotal').html(numberFormat(a));

                if (a == b) {
                    $('#btnSubmit').attr('disabled', false);
                }
                else{
                    $('#btnSubmit').attr('disabled', true);
                }
            });
            $('.formCr').unbind().keyup(function(){
                var a = 0;
                var b = 0;
                $('.formCr').each(function(){
                    if ($(this).val() === "") {
                        a = a + 0;
                    }
                    else{
                        a = a + parseInt($(this).val());
                    }
                });
                $('.formDb').each(function(){
                    if ($(this).val() === ""){
                        b = b + 0;
                    }
                    else{
                        b = b + parseInt($(this).val());
                    }
                });
                $('.crTotal').html(numberFormat(a));

                if (a == b) {
                    $('#btnSubmit').attr('disabled', false);
                }
                else{
                    $('#btnSubmit').attr('disabled', true);
                }
            });
        }
        function sumRmv(){
            var a = 0;
            var b = 0;

            $('.formCr').each(function(){
                if ($(this).val() === "") {
                    a = a + 0;
                }
                else{
                    a = a + parseInt($(this).val());
                }
            });
            $('.crTotal').html(numberFormat(a));

            $('.formDb').each(function(){
                if ($(this).val() === "") {
                    b = b + 0;
                }
                else{
                    b = b + parseInt($(this).val());
                }
            });
            $('.dbTotal').html(numberFormat(b));

            if (a == b) {
                $('#btnSubmit').attr('disabled', false);
            }
            else{
                $('#btnSubmit').attr('disabled', true);
            }
        }
        
        sumRmv();
        $('.selectAccounts').select2({
            placeholder : 'Select Accounts',
            allowClear : true
        });
        $('.btnRemove').unbind().click(function(){
            $(this).closest('.accounts').remove();

            sumRmv();
        });
        var number = 2;
        $('.btnAdd').unbind().click(function(){
            var type = $('#addRow').val();
            if (type == '1') {
                var html = `
                        <div class="form-group row accounts">
                        <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-5">
                                <select name="ledger[${number}][account_number]" class="form-control selectAccounts" required>
                                    <option selected disabled>--Select Accounts--</option>
                                    <?php foreach ($accounts as $row) { ?>
                                        <option value="<?php echo $row->id?>"><?php echo $row->text?></option>
                                    <?php }?>    
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm formDb" name="ledger[${number}][increase_db]" placeholder="Db">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm formCr" name="ledger[${number}][increase_cr]" placeholder="Cr" disabled>
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                            </div>
                        </div>`;
            }
            else{
                var html = `
                        <div class="form-group row accounts">
                        <label for="" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-5">
                                <select name="ledger[${number}][account_number]" class="form-control selectAccounts" required>
                                    <option selected disabled>--Select Accounts--</option>
                                    <?php foreach ($accounts as $row) { ?>
                                        <option value="<?php echo $row->id?>"><?php echo $row->text?></option>
                                    <?php }?>    
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm formDb" name="ledger[${number}][increase_db]" placeholder="Db" disabled>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control form-control-sm formCr" name="ledger[${number}][increase_cr]" placeholder="Cr">
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-danger pull-right btnRemove" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
                            </div>
                        </div>`;
            }
            
            $('.accounts-group').append(html);
            $('.selectAccounts').select2({
                placeholder : 'Select Accounts',
                allowClear : true
            });
            $('.btnRemove').unbind().click(function(){
                $(this).closest('.accounts').remove();
                sumRmv();
            });

            sum();
            number = number + 1 ;
        });
        sum();

        // $('#btnSubmit').unbind().click(function(){
        //     var data = $('#journal').serialize();
        //     console.log(data)
        // });
    });
</script>