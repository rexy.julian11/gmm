<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
	var datatable = null;
    $(document).ready(function(){
        function numberFormat(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
        datatable = $('#journal').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ajax": {
                "url": '<?php echo site_url('admin/journal/datatables'); ?>',
                "type": "POST"
            },
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                                { "searchable": false, 
                                  "targets"   : [0, 1, 3 ] }
                            ],
            "columns": [
                {"data":'',"sortable":false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {"data": "journal_datetime", render : function(data){
                        return moment(data).format("DD-MM-YYYY");
                    }
                },
                {"data": "journal_description"},
                {"data": "ledger", render : function(data){
                        return 'Rp. '  + numberFormat(data);
                    }
                },
                {"data": "action","ordering":false}
            ],
 
        });


    });
	
</script>