<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>
                    <?php echo $title; ?>
                </div>
                <?php echo form_open(); ?>
                <div class='ibox-content'>
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input name="title" autofocus id="" class="form-control"
                            value="<?php echo isset($menu->title) ? $menu->title : set_value("title"); ?>">
                        <?php echo form_error('title', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="icon">Icon (from <a href="https://fonthttps://fontawesome.com/v4.7.0/icons/"
                                target="_blank"> fontawesome.com <i class="fa fa-external-link"></i></a>) </label>
                        <input name="icon" id="" placeholder="eg: fa-home" class="form-control"
                            value="<?php echo isset($menu->icon) ? $menu->icon : set_value("icon"); ?>">
                        <?php echo form_error('icon', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="url">URL (leave it blank if this menu is main menu and has child menu)</label>
                        <input name="url" placeholder="eg: users, users/add" id="" class="form-control"
                            value="<?php echo isset($menu->url) ? $menu->url : set_value("url"); ?>">
                        <?php echo form_error('url', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="parent">Parent</label>
                        <select name="parent" id="" class="form-control">
                            <option value=""> - Choose Parent - </option>
                            <?php foreach($menus as $m){
                                if($menu->parent == $m->id){
                                    echo "<option value='$m->id' selected><i class='fa $m->icon'></i> $m->title</option>";
                                }
                                else{
                                    echo "<option value='$m->id'><i class='fa $m->icon'></i> $m->title</option>";
                                }
                            } ?>
                        </select>
                        <?php echo form_error('parent', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="menu_order">Menu Order </label>
                        <input type="number" name="menu_order" id="" class="form-control"
                            value="<?php echo isset($menu->menu_order) ? $menu->menu_order : set_value("menu_order"); ?>">
                        <?php echo form_error('menu_order', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="active">Active? </label><br>
                        <input name="active" id="" type='checkbox' class="js-switch" value='1'
                            <?php echo (isset($menu) && $menu->active == 0 ) ? '' : 'checked'; ?>>
                        <?php echo form_error('active', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>


                    <?php if(isset($menu)):?>
                    <?php foreach($groups as $gm):
                        $ada = is_in_array($group_menu,'group_id',$gm->id);
                        $active = '';
                        if(isset($ada)){
                            if($group_menu[$ada]->active == 1){
                                $active = 'checked';
                            };
                        }
                        ?>
                    <input type="checkbox" name="groups[<?php echo $gm->id; ?>]" value='1' <?php echo $active; ?>>
                    <?php echo $gm->description; ?> <br>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <div class="form-group">

                        <label for="groups">For Groups</label><br>
                        <?php foreach($groups as $groups): ?>
                        <input type="checkbox" name="groups[<?php echo $groups->id; ?>]" value='1'>
                        <?php echo $groups->description; ?> <br>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class='ibox-footer'>
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit(isset($menu) ? 'Save Menu' : 'Create Menu'); ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
