<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>
                    <?php echo btn_add('admin/menu/add','Create Menu'); ?>
                </div>
                <div class='ibox-content'>
                    <div class="dd" id="nestable2">
                        <ol class="dd-list">
                            <?php foreach($main_menu as $main):
                            $cek_url = $this->ion_auth_acl->check_existing_permisisons(site_url().$main->url);
                            if($cek_url && $main->url){
                                $url_valid = "<span class='badge badge-primary'>url valid</span>";
                            }
                            else if($main->url == ''){
                                $url_valid = "";
                            }
                            else{
                                $url_valid = "<span class='badge badge-danger'>url invalid</span>";
                            }
                                    $submenu = $this->Admin_menu_model->get_menu($main->id); ?>
                            <li class="dd-item" data-id="<?php echo $main->id; ?>">
                                <div class="dd-handle">
                                    <span class="float-right">
                                        <?php echo btn_edit('admin/menu/edit/', $main->id); ?>
                                        <?php echo btn_delete('admin/menu/delete/', $main->id); ?>
                                    </span>
                                    <span class="float-right mr-5">
                                        <?php echo active($main->active); ?>
                                    </span>
                                    <span class="float-right mr-5">
                                        <?php echo $url_valid; ?>
                                    </span>
                                    <span class="float-right mr-5">
                                        <?php echo $main->url; ?>
                                    </span>


                                    <?php echo $main->menu_order." - "; ?> <span class="label label-info"> <i
                                            class="fa <?php echo $main->icon; ?>"></i></span>
                                    <?php echo $main->title; ?>
                                </div>
                                <?php if(isset($submenu)): ?>
                                <ol class="dd-list">
                                    <?php foreach($submenu as $sub):
                                        $cek_url = $this->ion_auth_acl->check_existing_permisisons(site_url().$sub->url);
                                        if($cek_url){
                                            $url_valid = "<span class='badge badge-primary'>url valid</span>";
                                        }
                                        else{
                                            $url_valid = "<span class='badge badge-danger'>url invalid</span>";
                                        }
                             ?>
                                    <li class="dd-item" data-id="<?php echo $sub->id ?>">
                                        <div class="dd-handle">
                                            <span class="float-right">
                                                <?php echo btn_edit('admin/menu/edit/', $sub->id); ?>
                                                <?php echo btn_delete('admin/menu/delete/', $sub->id); ?>
                                            </span>
                                            <span class="float-right mr-5">
                                                <?php echo active($sub->active); ?>
                                            </span>
                                            <span class="float-right mr-5">
                                                <?php echo $url_valid; ?>
                                            </span>
                                            <span class="float-right mr-5">
                                                <?php echo $sub->url; ?>
                                            </span>

                                            <?php echo $sub->title; ?>
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ol>
                                <?php endif; ?>
                            </li>
                            <?php endforeach; ?>
                        </ol>
                    </div>

                </div>
                <div class='ibox-footer'>
                </div>
            </div>
        </div>
    </div>
</div>
