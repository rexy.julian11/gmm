<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_menu extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_menu_model');
                $this->breadcrumb->add('Menu manager',base_url().'admin/menu');
                $this->data['title'] = 'Menu manager';
            }
            
            final public function index()
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('js/datatables','js/js'); //load aditional js script
                $this->data['main_menu'] = $this->Admin_menu_model->get_menu();

                $this->_render_page('html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
            
            }
            final public function add()
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['title'] = 'Add Menu';
                $this->breadcrumb->add('Add', site_url('admin/menu/add'));

                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->data['groups'] = $this->ion_auth->groups()->result();
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
		
                    
                    $this->data['menus'] = $this->Admin_menu_model->get_menu();
                    $this->_render_page('html/form', $this->data); 
                } else {
                    $data = $this->security->xss_clean($this->input->post());
                    if( ! isset($data['active'])){
                        $data['active'] = 0;
                    }

                    
                    if($this->Admin_menu_model->insert($data)){
                        $this->session->set_flashdata('success', 'Success Adding Data');   
                    }
                    redirect('admin/menu','refresh');
                    # code...
                }
                
            }
            
            final public function edit($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['title'] = 'Update Menu';
                $this->breadcrumb->add('Edit', site_url('admin/menu/edit'));

                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->data['groups'] = $this->ion_auth->groups()->result();
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->data['menus'] = $this->Admin_menu_model->get_menu();
                    $this->data['menu'] = $this->Admin_menu_model->get_menu_by_id($id);
                    $this->data['group_menu'] = $this->Admin_menu_model->get_group_menu($id);
                    
                    $this->_render_page('html/form', $this->data); 
                } else {
                    $data = $this->security->xss_clean($this->input->post());
                    if( ! isset($data['active'])){
                        $data['active'] = 0;
                    }

                    if($this->Admin_menu_model->update($id,$data)){
                        $this->session->set_flashdata('success_edit', 'Success Updating Data');   
                    }
                    redirect('admin/menu','refresh');
                    # code...
                }
            }
            
            final public function delete($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                 if($this->Admin_menu_model->delete($id)){
                        $this->session->set_flashdata('success_delete', 'Success Deleting Data');   
                    }
                    redirect('admin/menu','refresh');
            
            }

            public function get_my_menu(){
                return $this->Admin_menu_model->get_my_menu();
            }
            
            
        }
