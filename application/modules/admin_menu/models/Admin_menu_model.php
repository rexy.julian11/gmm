<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_menu_model extends CI_Model
        {
            private $table = 'menu';
            public function __construct()
            {
                parent::__construct();
            }
            public function index()
            {
                
            }

            public function get_menu($id = 0){
                $this->db->where('parent',$id);
                $this->db->order_by('menu_order', 'ASC');
                return $this->db->get($this->table)->result();
            }
            
            public function get(){
                return $this->db->get($this->table)->result();
            }

            public function update($id, $data){
                $data['updated'] = date("Y-m-d H:i:s");
                $this->db->trans_begin();
                
                $group_data = $data['groups'];
                unset($data['groups']);
                //state 1 : update menu table
                $this->db->update($this->table,$data,array('id'=> $id));

                //step 2 : update group_menu
                $groups = $this->ion_auth->groups()->result();
                foreach($groups as $group){
                    if(isset($group_data[$group->id]) && $group_data[$group->id] == 1){
                        $active = 1;
                    }
                    else{
                        $active = 0;
                    }
                    $this->db->where('group_id', $group->id);
                    $this->db->where('menu_id', $id);
                    $this->db->set('active', $active);
                    $this->db->update('group_menu');
                }
                
                
                

                // step 3 : update has_child menu
                if($data['parent'] != ''){
                    $this->db->where('id', $data['parent']);
                    $this->db->set('has_child',1);
                    $this->db->update($this->table);
                }
                else{
                   $menu = $this->get_menu_by_id($id); 
                //    cek if parent menu still has another child or not
                    if($this->has_child($menu->parent) == 0){

                        $this->db->where('id', $menu->parent);
                        $this->db->set('has_child',0);
                        $this->db->update($this->table);
                    }
                }

                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return TRUE;
                }else{
                    $this->db->trans_rollback();
                    return FALSE;
                }
            }

            public function insert($data){
                $groups = $this->ion_auth->groups()->result();

                $group_data = $data['groups'];
                unset($data['groups']);
                $this->db->trans_begin();
                //stage 1 insert to menu
                $this->db->insert($this->table, $data);
                $menu = $this->db->insert_id();

                //stage 2, assign menu to group
                foreach($groups as $groups){
                    if(isset($group_data[$groups->id]) && $group_data[$groups->id] == 1){
                        $active = 1;
                    }
                    else{
                        $active = 0;
                    }
                    $gm = array(
                        'group_id' => $groups->id,
                        'menu_id' => $menu,
                        'active' => $active,
                    );
                    $this->db->insert('group_menu', $gm);
                }

                //stage 3 : if sub menu and has a parent, update parent for has_child field
                if($data['parent'] != ''){
                    $this->db->where('id', $data['parent']);
                    $this->db->set('has_child',1);
                    $this->db->update($this->table);
                }
                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return TRUE;
                }else{
                    $this->db->trans_rollback();
                    return FALSE;
                }
            }

            public function delete($id){
                $this->db->where('id',$id);
                return $this->db->delete($this->table);
            }

            public function get_menu_by_id($id){
                $this->db->where('id', $id);
                return $this->db->get($this->table)->row();
            }

            public function get_group_menu($id){
                $this->db->select('m.*,g.name, g.description');
                $this->db->from('group_menu as m');
                $this->db->join('groups as g','g.id = m.group_id');
                $this->db->where('m.menu_id', $id);
                return $this->db->get()->result();
            }

            public function get_my_menu(){
                //get users current active menu to render
                $my_group= $this->ion_auth->get_users_groups()->row();
                $this->db->select('');
                $this->db->from($this->table.' as m');
                $this->db->join('group_menu as gm', 'gm.menu_id = m.id');
                $this->db->where('gm.group_id',$my_group->id);
                $this->db->where('gm.active',1);
                $this->db->where('m.active',1);
                $this->db->order_by('m.menu_order', 'asc');
                $menus = $this->db->get()->result();
                
                $parent = array();
                $sub = array();
                foreach($menus as $m){
                    if($m->parent == 0){
                        $parent[] = $m;
                    }
                    if($m->parent != 0){
                        $sub[$m->parent][] = $m;
                    }
                }

                $my_menu = array(
                    'parent' => $parent,
                    'sub'   => $sub
                );

                return $my_menu;
                // echo "<pre>";
                // print_r($menus);
                // print_r($my_menu);
                // echo "</pre>";
                // die;

            }

            public function has_child($id){
                $this->db->where('parent', $id);
                return $this->db->get($this->table)->num_rows();
            }
            public function getTransactionMenu()
            {
                return $this->db->get('transaction_setting')->result();
            }
            
            
        }
