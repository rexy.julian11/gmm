<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pembayaran_penjualan_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getOrderData()
    {
        return $this->db->select('so_id as id, so_id as text')
                        ->where('is_paid', 0)
                        ->get('sales_orders')->result();
    }
    public function getOrderDetail($so_id)
    {
        $order = $this->db->select('so.*, CONCAT(customer.first_name , " ", customer.last_name) as customer_name, customer.phone, customer.address')
                          ->join('users as customer', 'customer.id = so.customer_id')
                          ->where('so.so_id', $so_id)
                          ->get('sales_orders as so')->row();
        $item = $this->db->select('p.*, i.item_name')
                         ->join('items as i', 'i.item_id = p.item_id')
                         ->where('p.so_id', $so_id)
                         ->get('sales_order_item_details as p')->result();
        $service = $this->db->select('sos.*, s.service_name')
                            ->join('services as s', 's.service_id = sos.service_id')
                            ->where('sos.so_id', $so_id)
                            ->get('sales_order_service_details as sos')->result();
        $dibayarkan = $this->db->select_sum('payment_amount')
                               ->where('order_number', $so_id)
                               ->get('payment')->row();
        $order->item = $item; 
        $order->dibayarkan = $dibayarkan;
        $order->service = $service; 
        return $order;
    }
    public function getPaymentAccount()
    {
        return $this->db->select('account_number as id, account_name as text')
                        ->where('account_type', 'Account')
                        ->get('accounts')->result();
    }
    public function doPayment($data)
    {
        return $this->db->insert('payment', $data);
    }
    public function checkPayment($order_number)
    {
        return $this->db->select_sum('payment_amount')
                        ->where('order_number', $order_number)
                        ->group_by('order_number')
                        ->get('payment')->row();
    }
    public function totalOrder($order_number)
    {
        return $this->db->select('so_bill')
                        ->where('so_id', $order_number)
                        ->get('sales_orders')->row();
    }
    public function updateOrder($order_number, $data)
    {
        return $this->db->where('so_id', $order_number)
                        ->update('sales_orders', $data);
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function updateStock($item_id, $data)
    {
        return $this->db->where('item_id', $item_id)
                        ->update('items', $data);
    }
}