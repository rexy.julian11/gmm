<script>
    $(document).ready(function(){
        function countPayed()
        {
            var totalPay = 0;
            var sisaTagihan = 0;
            var dibayarkan = parseFloat($('.inputDibayarkan').val());
            var totalTagihan = parseFloat($('.inputTotalTagihan').val());
            $('.inputPayment').unbind().each(function(){
                var value = parseFloat($(this).val());
                if (isNaN(value)) {
                    value = 0;
                }
                totalPay = totalPay + value;
            });
            $('.totalDibayarkan').html(totalPay);
            sisaTagihan = totalTagihan - totalPay - dibayarkan;
            $('.inputSisaTagihan').val(sisaTagihan).trigger('change');
        }
        function countTotal()
        {
            $('body').on('change', '.inputPayment', function(){
                countPayed();
            });
            $('.btnRemove').unbind().click(function(){
                $(this).closest('tr').remove();
                countPayed();
            });
        }
        $('#fakturPenjualan').select2({
            placeholder : 'Pilih Faktur Penjualan',
            ajax: {
                url: '<?= site_url('admin/pembayaran-penjualan/getOrderData')?>',
                processResults: function (data) {
                return {
                    results: data
                };
                }
            }
        });

        $('.btnAddPayment').click(function(){
            $.notify("Harap Pilih Penjualan Terlebih Dahulu", "warn");
        });

        $('#fakturPenjualan').unbind().change(function(){
            var id = $(this).val();
            $.ajax({
                url : '<?= site_url('admin/pembayaran-penjualan/getorderdetail/')?>' + id,
                type : 'POST'
            }).done(function(r){
                var totalItem = 0;
                var totalService = 0;
                var htmlItem = ``;
                var htmlService = ``;
                var dibayarkan = 0;
                var totalTagihan = 0;
                $('#so_date').val(moment(r.so_date).format('YYYY-MM-DD'));
                $('#so_bill').val(r.so_bill);
                $('#so_date_expired').val(moment(r.so_expired_date).format('YYYY-MM-DD'));
                $('#inputNama').val(r.customer_name);
                $('#inputTelp').val(r.phone);
                $('#inputAlamat').val(r.address);
                $('#order_number').val(r.so_id);
                $('.tblBarang').html('');
                $('.tblPayment').html('');
                $('.totalDibayarkan').html('');
                $.each(r.item, function(i, data){
                    totalItem = totalItem + parseInt(data.item_total);
                    htmlItem += `<tr>
                                <td class="tblItemId" style="display:none;">${data.item_id}</td>
                                <td>${data.item_name}</td>
                                <td>${data.item_price}</td>
                                <td class="tblItemQty">${data.item_qty}</td>
                                <td class="text-right">${data.item_total}</td>
                            </tr>`;
                });
                $.each(r.service, function(i, data){
                    totalService = totalService = parseInt(data.sos_total);
                    htmlService += `
                                    <tr>
                                        <td class="tblServiceId" style="display:none;">${data.service_id}</td>
                                        <td>${data.service_name}</td>
                                        <td>${data.sos_price}</td>
                                        <td class="tblServiceUnit">${data.sos_unit}</td>
                                        <td class="tblServiceQty">${data.sos_qty}</td>
                                        <td class="tblServiceDuration">${data.sos_duration}</td>
                                        <td class="text-right">${data.sos_total}</td>
                                    </tr>
                                    `;
                });
                if (r.dibayarkan.payment_amount != null) {
                    dibayarkan = dibayarkan + parseFloat(r.dibayarkan.payment_amount);
                }
                totalTagihan = parseFloat(totalItem) + parseFloat(totalService); 
                sisaTagihan = parseFloat(totalItem) + parseFloat(totalService) - dibayarkan;
                $('.tblBarang').append(htmlItem);
                $('.tblJasa').append(htmlService);
                $('.grandTotalItem').html(totalItem);
                $('.grandTotalService').html(totalService);
                $('.inputTotalTagihan').val(totalTagihan);
                $('.inputSisaTagihan').val(sisaTagihan);
                $('.inputDibayarkan').val(dibayarkan);
                $('.btnAddPayment').unbind().click(function(){
                    var htmlPayment = `<tr>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button>
                                            </td>
                                            <td>
                                                <select class="form-control selectPaymentType" name="" required>
                                                    <option selected disabled>Pilih Metode Pembayaran</option>
                                                    <option value="Tunai">Tunai</option>
                                                    <option value="Giro">Giro</option>
                                                    <option value="Transfer">Transfer</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control selectPayment" name="" required>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control inputPayment" style="text-align: right; " name="" id="" required>
                                            </td>
                                       </tr>`;
                    $('.tblPayment').append(htmlPayment);
                    $('.selectPayment').select2({
                        placeholder : 'Pilih Akun Pembayaran',
                        ajax: {
                            url: '<?= site_url('admin/pembayaran-penjualan/getPaymentAccount')?>',
                            processResults: function (data) {
                            return {
                                results: data
                            };
                            }
                        }
                    });
                    countTotal()
                });
                $('.btnSubmit').unbind().click(function(){  
                    var order_number    = $('#order_number').val();
                    var tblItem         = $('.tblBarang').find('tr');
                    var tblService      = $('.tblJasa').find('tr');
                    var account_number  = [];
                    var payment_method  = [];
                    var payment_amount  = [];
                    var items           = [];
                    var services        = [];
                    $('.selectPayment').unbind().each(function(){
                        account_number.push($(this).val());
                    });
                    $('.selectPaymentType').unbind().each(function(){
                        payment_method.push($(this).val());
                    });
                    $('.inputPayment').unbind().each(function(){
                        payment_amount.push(parseFloat($(this).val()));
                    });
                    tblItem.unbind().each(function(){
                        items.push({'item_id' : $(this).find('.tblItemId').html(), 'item_qty' : $(this).find('.tblItemQty').html()});
                    });
                    tblService.unbind().each(function(){
                        services.push({'service_id' : $(this).find('.tblServiceId').html() , 
                                       'service_qty' : $(this).find('.tblServiceQty').html() ,
                                       'service_unit' : $(this).find('tblServiceUnit').html() , 
                                       'service_duration' : $(this).find('tblServiceDuration').html()});
                    });
                    if (account_number.length === 0 || payment_method.length === 0 || payment_amount.length === 0) {
                        $.notify("Please fill the form", "warn");
                    }
                    else{
                        $.ajax({
                            url : '<?= site_url('admin/pembayaran-penjualan/dopayment')?>',
                            type : 'POST',
                            data : {order_number : order_number,
                                    account_number : account_number,
                                    payment_method : payment_method,
                                    payment_amount : payment_amount,
                                    items : items,
                                    services : services}
                        }).done(function(r){
                            if (r == 'true') {
                                window.location.href = "<?= site_url('admin/pembayaran-penjualan')?>";
                            }
                            else{
                                $.notify("Check Your Data", "warn");
                            } 
                        });
                    }
                });

            });
        });
    });
</script>