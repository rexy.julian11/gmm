<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_pembayaran_penjualan/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {"data": "id"},
                    {"data": "so_date"},
                    {"data": "so_bill"},
                    {"data": "so_expired_date"},
                    {"data": "customer_name"},
                    {"data": "account_name"},
                    {"data": "payment_method"},
                    {"data": "payment_amount"}
                ], 
            });
    
    
        });
        </script>