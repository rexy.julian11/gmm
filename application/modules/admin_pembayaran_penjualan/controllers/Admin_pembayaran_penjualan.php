<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pembayaran_penjualan extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_pembayaran_penjualan_model');
        $this->load->model('admin_journal/Admin_journal_model');
        $this->breadcrumb->add('Admin pembayaran penjualan',base_url().'admin_pembayaran_penjualan');
        $this->data['title'] = 'Admin pembayaran penjualan';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_pembayaran_penjualan/js/datatables'); //load aditional js script
        $this->_render_page('admin_pembayaran_penjualan/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('so.so_id as id, so.so_date, so.so_bill, so_expired_date, CONCAT(customer.first_name, " ", customer.last_name) as customer_name, 
                                a.account_name, p.payment_method, p.payment_amount');
            $this->datatables->from('sales_orders as so');
            $this->datatables->join('payment as p', 'p.order_number = so.so_id', 'inner');
            $this->datatables->join('users as customer', 'customer.id = so.customer_id');
            $this->datatables->join('accounts as a', 'a.account_number = p.account_number');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="pembayaran-penjualan/edit/$1" title="edit">
                    <i class="fa fa-pencil">
                    </i>
                    <a class="btn btn-icon btn-sm btn-danger mr-1" href="pembayaran-penjualan/delete/$1" title="delete">
                    <i class="fa fa-trash-o">
                    </i>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->data['js_script'] = array('admin_pembayaran_penjualan/js/form'); //load aditional js script
        $this->_render_page('admin_pembayaran_penjualan/html/form', $this->data);

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    public function getOrderData()
    {
        $order = $this->Admin_pembayaran_penjualan_model->getOrderData();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($order));
    }
    public function getOrderDetail($po_id)
    {
        $order = $this->Admin_pembayaran_penjualan_model->getOrderDetail($po_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($order));
    }
    public function getPaymentAccount()
    {
        $accounts = $this->Admin_pembayaran_penjualan_model->getPaymentAccount();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($accounts));
    }
    public function doPayment()
    {
        $data = $this->input->post(null);
        
        $total = 0;
        $insertJournal['journal_description']   = 'Penjualan' . ' ' . $data['order_number'];
        if ($journal = $this->Admin_journal_model->insertJournal($insertJournal)) {
            for ($i=0; $i < count($data['payment_method']); $i++) { 
                $insert['order_number'] = $data['order_number'];
                $insert['account_number'] = $data['account_number'][$i];
                $insert['payment_method'] = $data['payment_method'][$i];
                $insert['payment_amount'] = $data['payment_amount'][$i];
                if ($this->Admin_pembayaran_penjualan_model->doPayment($insert)) {
                    $insertLedger['journal_id']         = $journal;
                    $insertLedger['account_number']     = $data['account_number'][$i];
                    $insertLedger['ledger_increase_to'] = 'Cr';
                    $insertLedger['ledger_amount']      = $data['payment_amount'][$i];
                    $this->Admin_journal_model->insertLedger($insertLedger); 
                    $message = 'true';
                }
                else{
                    $message = 'false';
                }
                $total = $total + $data['payment_amount'][$i];
            }
        }

        $insertLedger['journal_id']         = $journal;
        $insertLedger['account_number']     = '1-3.1';
        $insertLedger['ledger_increase_to'] = 'Db';
        $insertLedger['ledger_amount']      = $total;
        $this->Admin_journal_model->insertLedger($insertLedger); 

        if ($message == 'true') {
            $checkPayment = $this->Admin_pembayaran_penjualan_model->checkPayment($data['order_number']);
            $totalOrder = $this->Admin_pembayaran_penjualan_model->totalOrder($data['order_number']);
            if ((int)$checkPayment->payment_amount == (int)$totalOrder->so_bill) {
                $update['is_paid'] = 1;
                if ($this->Admin_pembayaran_penjualan_model->updateOrder($data['order_number'], $update)) {
                    foreach ($data['items'] as $item) {
                        $itemdata = $this->Admin_pembayaran_penjualan_model->getItemData($item['item_id']);
                        $updateStock['item_stock'] = (int)$itemdata->item_stock - (int)$item['item_qty'];
                        $this->Admin_pembayaran_penjualan_model->updateStock($item['item_id'], $updateStock);
                    }
                    echo 'true';
                }
                else{
                    echo 'false';
                }
            }
            else{
                echo 'true';
            }
        }
        else{
            echo 'false';
        }
    }
}