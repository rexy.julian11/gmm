<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_retur_penjualan_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('retur_penjualan', $data);
    }
    public function insertItemDetail($data)
    {
        return $this->db->insert('sales_order_item_details', $data);
    }
    public function insertServiceDetail($data)
    {
        return $this->db->insert('sales_order_service_details', $data);
    }
    public function update($id, $data)
    {
        $this->db->where('rp_id', $id);
        return $this->db->update('retur_penjualan', $data);
    }
    public function deleteRetur($id)
    {
        return $this->db->where('rp_id', $id)
                        ->delete('retur_penjualan');
    }
    public function deleteItemDetail($id)
    {
        $this->db->where('so_id', $id);
        return $this->db->delete('sales_order_item_details');
    }
    public function deleteServiceDetail($id)
    {
        $this->db->where('so_id', $id);
        return $this->db->delete('sales_order_service_details');
    }
    public function getPurchaseData($id)
    {
        $purchase = $this->db->select('po.*, CONCAT(customer.first_name, " ", customer.last_name) as customer_name, customer.phone, customer.address')
                             ->join('users as customer', 'customer.id = po.customer_id')
                             ->where('po.rp_id', $id)
                             ->get('retur_penjualan as po')->row();
        $items = $this->db->select('poi.*, i.item_name')
                          ->join('items as i', 'i.item_id = poi.item_id')
                          ->where('poi.so_id', $id)
                          ->get('sales_order_item_details as poi')->result();
        $purchase->items = $items;

        return $purchase;
    }
    public function getPurchaseDetail($id)
    {
        return $this->db->select('po.*, items.item_name')
                        ->join('items', 'items.item_id = po.item_id')
                        ->where('po.po_no', $id)
                        ->get('purchase_order_details as po')->result();
    }
    public function getCustomer()
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as text')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 5)
                        ->get('users as u')->result();
    }
    public function getCustomerData($customer_id)
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as name, phone, address')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 5)
                        ->where('u.id', $customer_id)
                        ->get('users as u')->row();
    }
    public function getItems() 
    {
        return $this->db->select('item_id as id, item_name as text')
                        ->get('items')->result();
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function getServices()
    {
        return $this->db->select('service_id as id, service_name as text')
                        ->get('services')->result();
    }
    public function getServiceData($service_id)
    {
        return $this->db->where('service_id', $service_id)
                        ->get('services')->row();
    }
    public function getPoDetail($id)
    {
        $data['items'] = $this->db->select('poi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = poi.item_id')
                                  ->where('poi.so_id', $id)
                                  ->get('sales_order_item_details as poi')->result();
        $data['services'] = $this->db->select('pos.*, s.service_name')
                                     ->join('services as s', 's.service_id = pos.service_id')
                                     ->where('pos.so_id', $id)
                                     ->get('sales_order_service_details as pos')->result();
        return $data;
    }
    public function getInvoice($id)
    {
        $invoice = $this->db->select('po.*, concat(customer.first_name, " ", customer.last_name) as customer_name, customer.address, customer.phone')
                            ->join('users as customer', 'customer.id = po.customer_id')
                            ->where('po.po_no', $id)
                            ->get('retur_penjualan as po')->row();
        $invoice->items = $this->db->select('poi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = poi.item_id')
                                  ->where('poi.so_id', $id)
                                  ->get('sales_order_item_details as poi')->result();
        $invoice->services = $this->db->select('pos.*, s.service_name')
                                     ->join('services as s', 's.service_id = pos.service_id')
                                     ->where('pos.so_id', $id)
                                     ->get('sales_order_service_details as pos')->result();
        return $invoice;
    }
    public function getJournal($journal_ref_pk)
    {
        return $this->db->where('journal_ref_pk', $journal_ref_pk)
                        ->get('journals')->row();
    }
    public function getLedger($journal_id)
    {
        return $this->db->where('journal_id', $journal_id)
                        ->get('ledgers')->result();
    }
    public function deleteJournal($so_id)
    {
        return $this->db->where('journal_ref_pk', $so_id)
                        ->delete('journals');
    }
    public function deleteLedger($journal_id)
    {
        return $this->db->where('journal_id', $journal_id)
                        ->delete('ledgers');
    }
    public function getReturDetail($retur_id)
    {
        return $this->db->select('i.item_name, soi.*')
                        ->join('items as i', 'i.item_id = soi.item_id')
                        ->where('soi.so_id', $retur_id)
                        ->get('sales_order_item_details as soi')->result();
    }
}