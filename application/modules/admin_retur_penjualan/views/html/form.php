<style>
    .center{
        width: 150px;
        margin: 40px auto;   
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Retur Pembelian
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">No Retur *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="rp_number" value="<?= $nr_number?>" readonly>
                                        <?php echo form_error('rp_number', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>               
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Retur *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="rp_date" value="">
                                        <?php echo form_error('rp_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Total Nilai Retur *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="rp_bill" value="" readonly>
                                        <?php echo form_error('rp_bill', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Jatuh Tempo *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="rp_date_expired" value="">
                                        <?php echo form_error('rp_date_expired', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Customer *</label>
                                    <div class="col-sm-8">
                                        <select name="" id="" class="form-control selectCustomer">
                                        </select>
                                    </div>
                                </div>
                                <div class="alert alert-dismissible alert-secondary">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputNama" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Telepon </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputTelp" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Alamat </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputAlamat" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Barang *</label>
                                <div class="col-sm-4">
                                    <select name="" id="" class="form-control selectBarang">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary btnAddBarang"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col" style="display:none;">id</th>
                                        <th scope="col">-</th>
                                        <th scope="col">Barang</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" class="pull-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblBarang">
    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <hr>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Grand Total </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right inputGrandTotal" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Terbilang </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right terbilang" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary pull-right btnSubmit">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>