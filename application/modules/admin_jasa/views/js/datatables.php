<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
	var datatable = null;
    $(document).ready(function(){

        datatable = $('.datatables').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ajax": {
                "url": '<?php echo site_url('admin_jasa/datatables'); ?>',
                "type": "POST"
            },
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                                { "searchable": false, 
                                  "targets"   : 0 }
                            ],
            "columns": [
                {"data":'',"sortable":false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {"data": "service_name"},
                {"data": "service_price"},
                {"data": "service_unit"},
                {"data": "service_duration"},
                {"data": "action","ordering":false}
            ], 
        });


    });
	
</script>