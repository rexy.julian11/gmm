<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Jasa
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service Name *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="service_name" value="<?= isset($service) ? $service->service_name : ''?>">
                                <?php echo form_error('service_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service Price *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="service_price" value="<?= isset($service) ? $service->service_price : ''?>">
                                <?php echo form_error('service_price', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service Unit *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="service_unit" value="<?= isset($service) ? $service->service_unit : ''?>">
                                <?php echo form_error('service_unit', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service Duration *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="service_duration" value="<?= isset($service) ? $service->service_duration : ''?>">
                                <?php echo form_error('service_duration', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>