<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_jasa extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_jasa_model');
                $this->breadcrumb->add('Admin jasa',base_url().'admin_jasa');
                $this->data['title'] = 'Admin jasa';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_jasa/js/datatables'); //load aditional js script
                $this->_render_page('admin_jasa/html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
                if($this->input->server('REQUEST_METHOD')=='POST'){
                    $this->load->library('Datatables');
                    $this->datatables->select('service_id as id, service_name, service_price, service_unit, service_duration');
                    $this->datatables->from('services');
                    $this->datatables->add_column(
                        'action',
                        '<a class="btn btn-icon btn-sm btn-success mr-1" href="jasa/edit/$1" title="edit">
                         <i class="fa fa-pencil">
                         </i>
                         <a class="btn btn-icon btn-sm btn-danger mr-1" href="jasa/delete/$1" title="delete">
                         <i class="fa fa-trash-o">
                         </i>',
                        "id");
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output($this->datatables->generate());
                }else{
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(401)
                        ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
                }
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                $this->load->library('form_validation');
                $this->form_validation->set_rules('service_name', 'Service Name', 'required');
                $this->form_validation->set_rules('service_price', 'Service Price', 'required|numeric');
                $this->form_validation->set_rules('service_unit', 'Service Unit', 'required');
                $this->form_validation->set_rules('service_duration', 'Service Duration', 'required|numeric');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->_render_page('admin_jasa/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($this->Admin_jasa_model->insert($data)) {
                        $this->session->set_flashdata('success', 'Successfull create data');
                        redirect('admin/jasa', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/jasa/form', 'refresh');
                    }
                
                }

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                $this->load->library('form_validation');
                $this->form_validation->set_rules('service_name', 'Service Name', 'required');
                $this->form_validation->set_rules('service_price', 'Service Price', 'required|numeric');
                $this->form_validation->set_rules('service_unit', 'Service Unit', 'required');
                $this->form_validation->set_rules('service_duration', 'Service Duration', 'required|numeric');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->data['service'] = $this->Admin_jasa_model->getService($id);
                    $this->_render_page('admin_jasa/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($this->Admin_jasa_model->update($id, $data)) {
                        $this->session->set_flashdata('success', 'Successfull update data');
                        redirect('admin/jasa', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/jasa/form', 'refresh');
                    }
                
                }
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                if ($this->Admin_jasa_model->delete($id)) {
                    $this->session->set_flashdata('success', 'Successfull delete data');
                    redirect('admin/jasa', 'refresh');
                }
                else{
                    $this->session->set_flashdata('failed', 'Check your data and try again');
                    redirect('admin/jasa', 'refresh');
                }
            }
            
        }