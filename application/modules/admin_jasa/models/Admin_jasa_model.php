<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_jasa_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('services', $data);
    }
    public function getService($service_id)
    {
        $this->db->where('service_id', $service_id);
        return $this->db->get('services')->row();
    }
    public function update($service_id, $data)
    {
        $this->db->where('service_id', $service_id);
        return $this->db->update('services', $data);
    }
    public function delete($service_id)
    {
        $this->db->where('service_id', $service_id);
        return $this->db->delete('services');
    }
}