<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_spk/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {"data": "spk_id"},
                    {"data": "so_id"},
                    {"data": "spk_date"},
                    {"data": "spk_name"},
                    {"data": "action","ordering":false},
                    {"data": "print","ordering":false}
                ], 
            });
    
    
        });
        </script>