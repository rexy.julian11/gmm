<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form SPK
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">SPK No *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="spk_id" value="<?= isset($spk) ? $spk->spk_id : $spk_id?>" readonly>
                                <?php echo form_error('spk_id', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sales Order *</label>
                            <div class="col-sm-6">
                                <select name="so_id" class="form-control select2" id="">
                                    <option selected disalbed>Pilih Sales Order</option>
                                    <?php foreach ($so as $row) { ?>
                                        <option value="<?= $row->so_id?>"><?= $row->so_id?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tanggal *</label>
                            <div class="col-sm-6">
                                <input type="date" class="form-control" name="spk_date" value="<?= isset($spk) ? $spk->spk_date : ''?>">
                                <?php echo form_error('spk_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Supir *</label>
                            <div class="col-sm-6">
                                <select name="user_id" class="form-control select2" id="">
                                    <option selected disalbed>Pilih Supir</option>
                                    <?php foreach ($sopir as $row) { ?>
                                        <option value="<?= $row->id?>"><?= $row->name?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>                    
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>