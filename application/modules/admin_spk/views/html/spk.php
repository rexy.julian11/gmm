<html>
    <style>
        .center{
            text-align: center;
        }
    </style>
    <body>
        <h2 class="center">PT Gemilang Makmur Mandiri</h2>
        <hr>
        <h4 class="center">Surat Perintah Kerja</h4>
        <h4 class="center">Nomor : <?= $spk_id?></h4>

        <p>Yang bertanda tangan dibawah ini : </p>
        <table style="margin-left : 12px;">
            <tr>
                <td>&emsp;Nama</td>
                <td>:</td>
                <td><?= $first_name ." ".$last_name?></td>
            </tr>
            <tr>
                <td>&emsp;Telepon</td>
                <td>:</td>
                <td><?= $phone?></td>
            </tr>
            <tr>
                <td>&emsp;Alamat</td>
                <td>:</td>
                <td><?= $address?></td>
            </tr>
        </table>
        <p>Bertindak atas nama PT Gemilang Makmur Mandiri selaku pelaksana pekerjaan nomor : <?= $so_id?> sesuai ketentuan di dalam 
        gambar kerja, rencana kerja, dan syarat-syarat teknis</p>
    </body>
</html>