<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                SPK
            </header>
            <div class="card-body">
                <div class="pull-right" style="margin-bottom:7px;">
                    <a href="<?php echo site_url('admin/spk/add')?>" class="btn btn-primary">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover datatables">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">NO SPK</th>
                                <th scope="col">Sales Order</th>
                                <th scope="col">Date</th>
                                <th scope="col">User</th>
                                <th scope="col">Action</th>
                                <th scope="col">Print</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
