<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_spk extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_spk_model');
        $this->breadcrumb->add('Admin spk',base_url().'admin_spk');
        $this->data['title'] = 'Admin spk';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_spk/js/datatables'); //load aditional js script
        $this->_render_page('admin_spk/html/index', $this->data);
    }
    
    /**
     *Data Services 
     **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('spk.spk_id, spk.*, CONCAT(u.first_name , " ", u.last_name) as spk_name');
            $this->datatables->from('spk as spk');
            $this->datatables->join('users as u', 'u.id = spk.user_id');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="" title="edit">
                <i class="fa fa-pencil">
                </i>
                <a class="btn btn-icon btn-sm btn-danger mr-1" href="" title="delete">
                <i class="fa fa-trash-o">
                </i>',
                "spk_id");
            $this->datatables->add_column(
                'print', 
                '<a class="btn btn-success" href="spk/print/$1">Print</a>',
                "spk_id"
            );
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
            }else{
                $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }
    
    /**
     * we use final to indicate that this function will be included to ACL table 
     **/
    
    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['js_script'] = array('admin_spk/js/form'); //load aditional js script
        $this->load->library('form_validation');
        $this->form_validation->set_rules('spk_date', 'Tanggal', 'required');
        $this->form_validation->set_rules('user_id', 'Supir', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['spk_id'] = 'SPK-'.time()."-".date("Y");
            $this->data['so']     = $this->Admin_spk_model->getSO();
            $this->data['sopir']  = $this->Admin_spk_model->getSupir();
            $this->_render_page('admin_spk/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if ($user = $this->Admin_spk_model->insert($data)) {
                $this->session->set_flashdata('success', 'Successfull create data');
                redirect('admin/spk', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/spk/form', 'refresh');
            }
        
        }

    }
    public function print($id)
    {
        $this->load->library('pdf');
        $new_id = str_replace("_","-",$id);
        $data = $this->Admin_spk_model->getData($new_id);
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "spk.pdf";
        $this->pdf->load_view('admin_spk/html/spk', $data);
    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    
}