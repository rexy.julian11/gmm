<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_spk_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('spk', $data);
    }
    public function getData($id)
    {
        return $this->db->join('users as u', 'u.id = spk.user_id')
                        ->where('spk_id', $id)
                        ->get('spk')->row();
    }
    public function update($id, $data)
    {
        return $this->db->where('spk_id', $id)
                        ->update('spk', $data);
    }
    public function delete($id)
    {
        return $this->db->where('spk_id', $id)
                        ->delete('spk');
    }
    public function getSO()
    {
        return $this->db->select('so_id')
                        ->where('is_paid', 1)
                        ->get('sales_orders')->result();
    }
    public function getSupir()
    {
        return $this->db->select('u.id as id, CONCAT(u.first_name, " ", u.last_name) as name')
                        ->join('users_groups as ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 7)
                        ->get('users as u')->result();
    }
}