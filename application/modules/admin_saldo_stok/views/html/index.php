<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                Saldo Stok
            </header>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover datatables">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Barang</th>
                                <th scope="col">Stok</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
