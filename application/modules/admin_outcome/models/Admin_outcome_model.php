<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_outcome_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getDataOutcome($id)
    {
        $this->db->where('ts_parent_menu', $id);
        return $this->db->get('transaction_setting')->result();
    }
}