<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_outcome extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_outcome_model');
                $this->breadcrumb->add('Admin outcome',base_url().'admin_outcome');
                $this->data['title'] = 'Admin outcome';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_outcome/js/datatables'); //load aditional js script
                $this->_render_page('admin_outcome/html/index', $this->data);
            }
            public function sub($id)
            {
                $this->data['outcome'] = $this->Admin_outcome_model->getDataOutcome($id);

                
                echo "<pre>";
                print_r ($this->data['outcome']);
                echo "</pre>";
                
            }
            /**
             *Data Services 
            **/
            public function datatables()
            {
                
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                //start your code here

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            
        }