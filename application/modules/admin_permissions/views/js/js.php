<script>
$(document).ready(function() {
    $("#category_id").on('change', function() {
        if ($("#category_id").val() != '') {
            category = $("#category_id option:selected").text();
            perm_key = category.replace(" ", "_");
            $("#perm_key").val(perm_key.toLowerCase() + "/");
        } else {
            $("#perm_key").val("");
        }

    })
});
</script>
