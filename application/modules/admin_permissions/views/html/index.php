<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?php echo btn_add('admin/permissions/add', $this->lang->line('create_permissions_title')); ?>
                    <?php echo isset($rebuild_link) ? '<a href="'.$rebuild_link.'" class="btn btn-warning">Rebuild ACL</a>': ''; ?>
                    <?php echo isset($flush_link) ? '<a href="'.$flush_link.'" class="btn btn-danger">Flush ACL</a>': ''; ?>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped datatable">
                        <thead>
                            <th>Category</th>
                            <th>Permissions</th>
                            <th>Descriptions</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <?php foreach($permissions as $perm): ?>
                            <tr>
                                <td><?php echo $perm['category']; ?></td>
                                <td><?php echo $perm['key']; ?></td>
                                <td><?php echo $perm['name']; ?></td>

                                <td>
                                    <?php echo btn_edit('admin/permissions/edit/', $perm['id']); ?>
                                    <?php echo btn_delete('admin/permissions/delete/', $perm['id']); ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
