<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>
                    <?php echo $title; ?>
                </div>
                <?php echo form_open(); ?>

                <div class='ibox-content'>
                    <div class="alert alert-info">
                        <strong>Info!</strong> Rebuild ACL table means that you'll automatically add new permisisons to
                        permissions table and matriks without losing your existing permissions data.
                        But if you face any errors, please do flush ACL instead.
                    </div>

                    We need to know that it's really you, and to make sure that you don't do this accidentally.
                    <div class="form-group">
                        <label for="password">Password <span class="text-danger">*</span></label>
                        <input type="password" name="password" id="" class="form-control" value="">
                        <?php echo form_error('password', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password <span class="text-danger">*</span></label>
                        <input type="password" name="confirm_password" id="" class="form-control" value="">
                        <?php echo form_error('confirm_password', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                </div>
                <div class='ibox-footer'>
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit('Confirm'); ?>
                </div>
                <?php echo form_close(); ?>

            </div>
        </div>
    </div>
</div>
