<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?php echo $title; ?>
                </div>

                <?php echo form_open(); ?>
                <div class="ibox-content">
                    <?php if($this->session->flashdata('failed_edit')){
                    echo "<span class='alert alert-danger'>".$this->ion_auth->errors()."</span>";
                } ?>
                    <div class="form-group">
                        <label for="category_id"><?php echo $this->lang->line('category_id_calidation_label') ?> <span
                                class="text-danger">*</span></label>

                        <select name="category_id" id="category_id" class="form-control select2">
                            <option value="">- Choose Category (Class Name) -</option>
                            <?php foreach($categories as $cat){
                                if(isset($permission) && $permission->category_id == $cat['id']){
                                    echo "<option value='".$cat['id']."' selected>".$cat['name']."</option>"; 
                                }
                                else{
                                    echo "<option value='".$cat['id']."'>".$cat['name']."</option>"; 
                                }
                            }?>
                        </select>
                        <?php echo form_error('category_id', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="perm_key"><?php echo $this->lang->line('perm_key_validation_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input name="perm_key" id="perm_key" class="form-control" placeholder="Class_name/method"
                            value="<?php echo isset($permission->perm_key) ? $permission->perm_key : set_value("perm_key"); ?>">
                        <?php echo form_error('perm_key', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="perm_name"><?php echo $this->lang->line('perm_name_validation_label'); ?> </label>
                        <input name="perm_name" id="" class="form-control"
                            value="<?php echo isset($permission->perm_name) ? $permission->perm_name : set_value("perm_name"); ?>">
                        <?php echo form_error('perm_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group ">

                        <span><?php echo $this->lang->line('edit_user_groups_heading');?></span><br>
                        <?php foreach ($groups as $group):?>
                        <?php
                            $group_perm = $this->ion_auth_acl->get_group_permissions($group['id']);     
                            $checked = null;
                            if(isset($permission) && isset($group_perm[$permission->perm_key]) && $group_perm[$permission->perm_key]['value']==1){
                                $checked = 'checked';
                            }
                        ?>
                        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"
                            <?php echo set_checkbox('groups[]', $group['id']) ?> <?php echo $checked; ?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?><br>
                        <?php endforeach?>



                    </div>
                </div>
                <div class="ibox-footer">
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit(isset($permission) ? 'Save Permission' : $this->lang->line('create_permissions_btn')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
