<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_permissions extends Backend_Controller
        {
            protected $class;
            protected $tables;
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Permissions_model');
                $this->breadcrumb->add('Permissions', site_url('permissions'));
                $this->class = $this->router->fetch_class()."/";
                $this->data['title'] = 'Permissions';
                $this->tables = $this->config->item('tables', 'ion_auth_acl');
              
            }


            final public function index()
            {
                
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                //get permissions for each method
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('js/datatables'); //load aditional js script
                
                $this->data['groups'] = $this->ion_auth->groups()->result_array();
                $this->data['perm_categories'] = $this->ion_auth_acl->permissions_categories('full');
                $this->data['permissions'] = $this->ion_auth_acl->permissions('full');
                
                
                if( $this->ion_auth_acl->has_permission($this->router->fetch_class()."/rebuild_acl")){
                    $this->data['rebuild_link'] = site_url('admin/permissions/rebuild-acl');
                }
                if( $this->ion_auth_acl->has_permission($this->router->fetch_class()."/flush_acl")){
                    $this->data['flush_link'] = site_url('admin/permissions/flush-acl');
                }
                $this->_render_page('html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
            
            }

            
            final public function rebuild_acl(){
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                $this->data['title'] = "Rebuild ACL";
                
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('confirm_password', 'Password', 'trim|required|matches[password]');
                
                
                if ($this->form_validation->run() == FALSE) {
                    $this->_render_page('html/rebuild_acl', $this->data);
                    # code...
                } else {
                    $data = $this->input->post();
                    $user = $this->ion_auth->user()->row();
                        if(password_verify($data['password'],$user->password)){
                        # code...
                        $exclude_modules = array(
                                'auth',
                                'errors',
                                'widgets',
                                'error_404',
                                'error_403'
                            );
                        if($this->ion_auth_acl->rebuilt_rbac($exclude_modules)){
                                $this->session->set_flashdata('success', 'ACL re-build successfully, please assign new permissions to your groups manualy');                                
                                redirect('admin/permissions','refresh');

                        }
                        else{
                                $this->session->set_flashdata('failed', 'ACL re-build failed');                                
                                redirect('admin/permissions','refresh');
                        }
                    }
                    else{
                        $this->session->set_flashdata('failed', 'You entered the wrong password');                                
                        redirect('admin/permissions/rebuild_acl','refresh');
                        
                    }
                    
                    
                }
                
                
               
            }
            
            final public function flush_acl(){
                //get permissions for each method
                // if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                //     redirect('errors/error_403','refresh');
                // }
                $this->data['title'] = "Flush ACL";
                
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('confirm_password', 'Password', 'trim|required|matches[password]');
                
                
                if ($this->form_validation->run() == FALSE) {
                    $this->_render_page('html/flush_acl', $this->data);
                    # code...
                } else {
                    $data = $this->input->post();
                    $user = $this->ion_auth->user()->row();
                        if(password_verify($data['password'],$user->password)){
                        # code...
                        $exclude_modules = array(
                                'auth',
                                'errors',
                                'widgets',
                                'error_404',
                                'error_403'
                            );
                        if($this->ion_auth_acl->flush_rbac($exclude_modules)){
                                $this->session->set_flashdata('success', 'ACL re-build successfully, please re-assign your group to these permissions');                                
                                redirect('admin/permissions','refresh');

                        }
                        else{
                                $this->session->set_flashdata('failed', 'ACL re-build failed');                                
                                redirect('admin/permissions','refresh');
                        }
                    }
                    else{
                        $this->session->set_flashdata('failed', 'You entered the wrong password');                                
                        redirect('admin/permissions/flush_acl','refresh');
                        
                    }
                    
                    
                }
                
                
               
            }

            final public function add()
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                $this->form_validation->set_rules('category_id', $this->lang->line('category_id_validation_label'), 'trim|required');
                $this->form_validation->set_rules('perm_key', $this->lang->line('perm_key_validation_label'), 'trim|required|is_unique['.$this->tables['permissions'].'.perm_key]');
                $this->form_validation->set_rules('perm_name', $this->lang->line('perm_name_validation_label'), 'trim|required');
                $this->data['groups'] = $this->ion_auth->groups()->result_array();
                $this->data['js_script'] = array('js/js'); //load aditional js script
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->data['categories'] = $this->ion_auth_acl->permissions_categories('full');
                    
                    $this->_render_page('html/form', $this->data);

                } else {
                    # code...
                    $data = $this->security->xss_clean($this->input->post());
                    
                    $this->db->trans_begin();
                    //add permissions, will return last inserted Id
                    $permission = $this->ion_auth_acl->create_permission(strtolower($data['perm_key']),$data['perm_name'], $data['category_id']);
                    
                    //add permissions to all group with selected status
                    foreach($this->data['groups'] as $group){
                        $status = 0;
                        if($group['id'] == in_array($group['id'],$data['groups'])){
                            $status = 1;
                        }
                        $this->ion_auth_acl->add_permission_to_group($group['id'],$permission, $status );
                    }

                    
                    if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    
                }else{
                    $this->session->set_flashdata('success', $this->ion_auth_acl->messages());
                    $this->db->trans_rollback();
                    }
                    redirect('admin/permissions');
                    
                }
                
            
            }
            
            final public function edit($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                if(! $id){
                    redirect($_SERVER['HTTP_REFERER']);
                }

                $this->form_validation->set_rules('category_id', $this->lang->line('category_id_validation_label'), 'trim|required');
                $this->form_validation->set_rules('perm_key', $this->lang->line('perm_key_validation_label'), 'trim|required|callback_permkey_check');
                $this->form_validation->set_rules('perm_name', $this->lang->line('perm_name_validation_label'), 'trim|required');
                $this->data['permission'] = $this->ion_auth_acl->permission($id);
                $this->data['groups'] = $this->ion_auth->groups()->result_array();
                $this->data['js_script'] = array('js/js'); //load aditional js script
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->data['categories'] = $this->ion_auth_acl->permissions_categories('full');
                    
                    $this->_render_page('html/form', $this->data);

                } else {
                    # code...
                    $data = $this->security->xss_clean($this->input->post());
                    $this->db->trans_begin();
                    //add permissions, will return last inserted Id
                    $permission = $this->ion_auth_acl->update_permission($id,$data['perm_key'],array('perm_name' => $data['perm_name']));
                    
                    //add permissions to all group with selected status
                    foreach($this->data['groups'] as $group){
                        $status = 0;
                        if($group['id'] == in_array($group['id'],$data['groups'])){
                            $status = 1;
                        }
                        $this->ion_auth_acl->update_group_permissions($id,$group['id'], $status );
                    }

                    
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                        $this->session->set_flashdata('success_edit', $this->ion_auth_acl->messages());
                        
                    }else{
                        $this->db->trans_rollback();
                        $this->session->set_flashdata('failed', $this->ion_auth_acl->messages());
                    }
                    redirect('admin/permissions');
                    
                }

            }
            
            final public function delete($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                if(! $id){
                    redirect($_SERVER['HTTP_REFERER']);
                }

                if($this->ion_auth_acl->remove_permission($id)){
                    $this->session->set_flashdata('success', 'Permission Removed');
                }
                else{
                    $this->session->set_flashdata('failed', 'Remove Permission Failed');
                }
                redirect('admin/permissions');
            
            }

            public function permkey_check($str){
                if($str != $this->data['permission']->perm_key){

                    $this->db->where('perm_key', $str);
                    $query = $this->db->get($this->tables['permissions']);
                    if($query->num_rows() > 0){
                        $this->form_validation->set_message('permkey_check', 'The {field} is already used');
                        return FALSE;                    
                    }
                    else{
                        return TRUE;
                    }
                }
                else{
                    return TRUE;
                }
            }
        }
