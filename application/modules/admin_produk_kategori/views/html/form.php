<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Produk Kategori
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Kategori *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="category_name" value="<?= isset($category) ? $category->category_name : ''?>">
                                <?php echo form_error('category_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>