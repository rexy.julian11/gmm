<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                Produk Kategori
            </header>
            <div class="card-body">
                <div class="pull-right" style="margin-bottom:7px;">
                    <a href="<?php echo site_url('admin/produk-kategori/add')?>" class="btn btn-primary">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover datatables">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
