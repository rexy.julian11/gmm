<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_produk_kategori extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_produk_kategori_model');
                $this->breadcrumb->add('Admin produk kategori',base_url().'admin_produk_kategori');
                $this->data['title'] = 'Admin produk kategori';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_produk_kategori/js/datatables'); //load aditional js script
                $this->_render_page('admin_produk_kategori/html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
                if($this->input->server('REQUEST_METHOD')=='POST'){
                    $this->load->library('Datatables');
                    $this->datatables->select('category_id as id, category_name');
                    $this->datatables->from('categories');
                    $this->datatables->add_column(
                        'action',
                        '<a class="btn btn-icon btn-sm btn-success mr-1" href="produk-kategori/edit/$1" title="edit">
                         <i class="fa fa-pencil">
                         </i>
                         <a class="btn btn-icon btn-sm btn-danger mr-1" href="produk-kategori/delete/$1" title="delete">
                         <i class="fa fa-trash-o">
                         </i>',
                        "id");
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output($this->datatables->generate());
                }else{
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(401)
                        ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
                }
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                $this->load->library('form_validation');
                $this->form_validation->set_rules('category_name', 'Category Name', 'required');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->_render_page('admin_produk_kategori/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($category = $this->Admin_produk_kategori_model->insert($data)) {
                        $this->session->set_flashdata('success', 'Successfull create data');
                        redirect('admin/produk-kategori', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/produk-kategori/form', 'refresh');
                    }
                
                }

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                $this->load->library('form_validation');
                $this->form_validation->set_rules('category_name', 'Category Name', 'required');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->data['category'] = $this->Admin_produk_kategori_model->getCategoryData($id);
                    $this->_render_page('admin_produk_kategori/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($category = $this->Admin_produk_kategori_model->update($id, $data)) {
                        $this->session->set_flashdata('success', 'Successfull update data');
                        redirect('admin/produk-kategori', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/produk-kategori/form', 'refresh');
                    }
                
                }
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                if ($category = $this->Admin_produk_kategori_model->delete($id)) {
                    $this->session->set_flashdata('success', 'Successfull delete data');
                    redirect('admin/produk-kategori', 'refresh');
                }
                else{
                    $this->session->set_flashdata('failed', 'Check your data and try again');
                    redirect('admin/produk-kategori', 'refresh');
                }
            }
            
        }