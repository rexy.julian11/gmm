<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_produk_kategori_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('categories', $data);
    }
    public function getCategoryData($category_id)
    {
        return $this->db->where('category_id', $category_id)
                        ->get('categories')->row();
    }
    public function update($category_id, $data)
    {
        return $this->db->where('category_id', $category_id)
                        ->update('categories', $data);
    }
    public function delete($category_id)
    {
        return $this->db->where('category_id', $category_id)
                        ->delete('categories');
    }
}