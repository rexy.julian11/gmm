<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_initial_balance_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getDataAccount(){
        $group = $this->db->where('account_type', 'Group')
                          ->get('accounts')->result();
        foreach ($group as $key => $row) {
            $control = $this->db->where('account_type', 'Account')
                                ->where('parent_number', $row->account_number)
                                ->get('accounts')->result();
            $group[$key]->control = $control;
            foreach ($control as $keys => $row_a) {
                $account = $this->db->where('account_type', 'Account')
                                    ->where('parent_number', $row_a->account_number)
                                    ->get('accounts')->result();
                $group[$key]->control[$keys]->account = $account;
            }
        }
        return $group;
    }
    public function setInitialBalance($acc_number, $data)
    {
        $this->db->where('account_number', $acc_number);
        return $this->db->update('accounts', $data);
    }   
}