<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function(){
        $.fn.editable.defaults.mode = 'inline';
        $('#account_initial_balance').editable(); 
    });
</script>