
<div class="page-content">
    <div class="row">
        <div class="col-md-6">
        <?php 
        $i = 0;
        foreach($group AS $row){
            $i++;
            if($i == 4){
                echo '</div><div class="col-md-6">';
            }
        ?>
            <div class="card mb-3">
                <div class="card-header"><?= $row->account_name;?></div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="table-secondary">
                                        <th scope="col" colspan='2'>Name</th>
                                        <th scope="col">Normal Balance</th>
                                        <th scope="col">Initial Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($row->control as $row_a) { ?>
                                    <tr>
                                        <td colspan='2'><?php echo $row_a->account_name?></td>
                                        <td><?php echo $row_a->account_normal_balance?></td>
                                        <td>
                                            <?php if ($row_a->account_initial_balance > 1) { 
                                                echo $row_a->account_initial_balance;
                                            } else {?>
                                                <a href="#" id="account_initial_balance" data-type="text" data-pk="<?php echo $row_a->account_number?>" data-url="<?php echo site_url('admin/initial-balance/setBalance')?>" data-title="Set Initial Balance"><?php echo isset($row_a->account_initial_balance) ? $row_a->account_initial_balance : '0' ?></a>
                                            <?php }?>
                                        </td>
                                    </tr>
                                        <?php foreach ($row_a->account as $row_b) { ?>
                                            <tr>
                                                    <td></td>
                                                    <td><?php echo $row_b->account_name;?></td>  
                                                    <td><?php echo $row_b->account_normal_balance;?></td>  
                                                    <td>
                                                        <?php if ($row_b->account_initial_balance > 1) { 
                                                            echo $row_b->account_initial_balance;
                                                        } else {?>
                                                            <a href="#" id="account_initial_balance" data-type="text" data-pk="<?php echo $row_b->account_number?>" data-url="<?php echo site_url('admin/initial-balance/setBalance')?>" data-title="Set Initial Balance"><?php echo isset($row_b->account_initial_balance) ? $row_b->account_initial_balance : '0' ?></a>
                                                        <?php }?>
                                                    </td>
                                            </tr>
                                        <?php } ?>
                                    <?php }?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>






