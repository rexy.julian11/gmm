<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Widget extends MY_Controller
        {
            protected $wdata = array();
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Widget_model');
                
                $this->wdata = array();
		        $this->load->library(['ion_auth','ion_auth_acl', 'form_validation','user_agent']);

                
                if (!$this->ion_auth->logged_in() || !$this->session->userdata('smadmin')) {
                    // redirect them to the login page
                    redirect(base_url());
                }
            }
            final public function index()
            {
                // $wdata[''] = var; 
                $this->_render_page('html/widget/admin', $this->wdata);
            }
            

           public function admin(){
               $this->load->view('html/widgets/admin', $this->wdata);
            }
           public function member(){
               $this->load->view('html/widgets/member', $this->wdata);
            }
            
           
            
           
           
           
            
        }
