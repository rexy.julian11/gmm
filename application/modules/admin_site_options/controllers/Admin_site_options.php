<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_site_options extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                
                $this->breadcrumb->add('Site options', base_url().'admin/site-options');
                $this->data['title'] = 'Site options';
            }
            final public function index()
            {
                //get permissions for this method
                if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
                    redirect('errors/error_403', 'refresh');
                }
                $this->data['site_info'] = $this->option->get_info('site_info');
                $this->data['about'] = $this->option->get_info('about_us');
                $this->data['img_review'] = $this->option->get_info('img_review');
                $this->data['smtp'] = $this->option->get_info('smtp');
                
                $this->data['header'] = array(theme_assets('inspina') . 'css/plugins/summernote/summernote-bs4.css'); //load aditional stylesheets
                $this->data['js_library'] = array(theme_assets('inspina') . 'js/plugins/summernote/summernote-bs4.js'); //load aditional js library
                $this->data['js_script'] = array('js/summernote', 'js/video'); //load aditional js script
                $this->_render_page('html/index', $this->data);
            }
            
            final public function save_info()
            {
                //get permissions for this method
                if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
                    redirect('errors/error_403', 'refresh');
                }

                $this->form_validation->set_rules('name', 'Site Name', 'trim|required');
                $this->form_validation->set_rules('address', 'Address', 'trim|required');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->data['site_info'] = $this->option->get_info('site_info');

                
                if ($this->form_validation->run() == false) {
                    # code..
                    $this->_render_page('html/index', $this->data);
                } else {
                    $data = $this->input->post();
                    $data['logo'] = $data['current_logo'];
                  

                    $config['upload_path']          = './uploads/';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    // $config['max_size']             = 2000;
                    // $config['max_width']            = 1080;
                    // $config['max_height']           = 1080;
                    $config['encrypt_name']			    = true;
                    $this->load->library('upload', $config);
                        
                    if (! empty($_FILES['logo']['name'])) {
                        if ($data['current_logo'] != '') {
                            unlink('./uploads/'.$data['current_logo']);
                        }
                        if (! $this->upload->do_upload('logo')) {
                            $this->session->set_flashdata('error_logo', $this->upload->display_errors());
                            redirect('admin_site_options');
                        } else {
                            $this->load->library('image_lib');
                            $image_data = $this->upload->data();
                    
                            $config_resize['image_library'] = 'gd2';
                            $config_resize['source_image'] = $image_data['full_path']; //get original image
                            $config_resize['master_dim'] = 'width';
                            $config_resize['quality'] = "100%";
                            $config_resize['maintain_ratio'] = true;
                            $config_resize['width'] = 1080;
                            $config_resize['height'] = 1;
                            $this->image_lib->initialize($config_resize);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('error_logo', $this->image_lib->display_errors());
                            }
                            $data['logo'] = $image_data['file_name'];
                            $this->image_lib->clear();
                        }
                    }

                    unset($data['current_logo']);

                    $array['option_value'] = json_encode($data);

                       
                    $this->db->where('option_key', 'site_info');
                    if ($this->db->update('site_options', $array)) {
                        $this->session->set_flashdata('success', 'Setting saved');
                    } else {
                        $this->session->set_flashdata('failed', 'setting failed to saved');
                    }

                    redirect('admin/site-options');

          
                    
                    # code...
                }
            }

            final public function save_greeting()
            {
                //get permissions for this method
                if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
                    redirect('errors/error_403', 'refresh');
                }
                
                $this->form_validation->set_rules('identify', 'Identifier', 'trim|required');
                $this->form_validation->set_rules('title', 'Title Greetings', 'trim|required');
                $this->form_validation->set_rules('content', 'Description Greetings', 'required');
                $this->data['about'] = $this->option->get_info('about_us');
                $this->data['img_review'] = $this->option->get_info('img_review');
                
                if ($this->form_validation->run() == false) {
                    # code..
                    $this->_render_page('html/index', $this->data);
                } else {
                    $data = $this->input->post();
                    $data['video'] = substr($data['video'], 32);
                    $data['greeting_image'] = $data['current_greeting_image'];
                    $testimonial['image']   = $data['current_testi_image'];

                    $config['upload_path']          = './uploads/';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['encrypt_name']			    = true;
                    $this->load->library('upload', $config);
                    if (! empty($_FILES['greeting_image']['name'])) {
                        if ($data['current_greeting_image'] != '') {
                            unlink('./uploads/'.$data['current_greeting_image']);
                        }
                        if (! $this->upload->do_upload('greeting_image')) {
                            $this->session->set_flashdata('error_greet_img', $this->upload->display_errors());
                            redirect('admin_site_options');
                        } else {
                            $this->load->library('image_lib');
                            $image_data = $this->upload->data();
                    
                            $config_resize['image_library'] = 'gd2';
                            $config_resize['source_image'] = $image_data['full_path']; //get original image
                            $config_resize['master_dim'] = 'width';
                            $config_resize['quality'] = "100%";
                            $config_resize['maintain_ratio'] = true;
                            $config_resize['width'] = 1080;
                            $config_resize['height'] = 1;
                            $this->image_lib->initialize($config_resize);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('error_logo', $this->image_lib->display_errors());
                            }
                            $data['greeting_image'] = $image_data['file_name'];
                            $this->image_lib->clear();
                        }
                    }

                    if (! empty($_FILES['review']['name'])) {
                        if ($data['current_testi_image'] != '') {
                            unlink('./uploads/'.$data['current_testi_image']);
                        }
                        if (! $this->upload->do_upload('review')) {
                            $this->session->set_flashdata('error_foto', $this->upload->display_errors());
                            redirect('admin_site_options');
                        } else {
                            $this->load->library('image_lib');
                            $image_data = $this->upload->data();
                    
                            $config_resize['image_library'] = 'gd2';
                            $config_resize['source_image'] = $image_data['full_path']; //get original image
                            $config_resize['master_dim'] = 'width';
                            $config_resize['quality'] = "100%";
                            $config_resize['maintain_ratio'] = true;
                            $config_resize['width'] = 1080;
                            $config_resize['height'] = 1;
                            $this->image_lib->initialize($config_resize);
                            if (!$this->image_lib->resize()) {
                                $this->session->set_flashdata('error_foto', $this->image_lib->display_errors());
                            }
                            $testimonial['image'] = $image_data['file_name'];
                            $this->image_lib->clear();
                        }
                    }

                    unset($data['current_greeting_image']);
                    unset($data['current_testi_image']);

                    $array['option_value'] = json_encode($data);
                    $testi['option_value'] = json_encode($testimonial);

                    $this->db->trans_begin();
                    $this->db->where('option_key', 'about_us')->update('site_options', $array);
                    $this->db->where('option_key', 'img_review')->update('site_options', $testi);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() == TRUE) {
                        $this->session->set_flashdata('success', 'Setting saved');
                    } else {
                        $this->session->set_flashdata('failed', 'setting failed to saved');
                    }

                    redirect('admin/site-options');

          
                    
                    # code...
                }
            }

            final public function save_smtp()
            {
                //get permissions for this method
                if (! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())) {
                    redirect('errors/error_403', 'refresh');
                }

                $this->form_validation->set_rules('host', 'SMTP Host', 'trim|required');
                $this->form_validation->set_rules('username', 'Host Username', 'trim|required');
                $this->form_validation->set_rules('password', 'Host Secret Key / Password ', 'trim|required');
                $this->form_validation->set_rules('port', 'SMTP Port', 'trim|required');
                $this->form_validation->set_rules('from_email', 'Email From', 'trim|required|valid_email');
                $this->form_validation->set_rules('from_name', 'Email From name', 'trim|required');
                $this->form_validation->set_rules('reply_to', 'Email Reply-to', 'trim|required|valid_email');
                $this->form_validation->set_rules('reply_to_name', 'Name for Reply-to', 'trim|required');
                $this->data['site_info'] = $this->option->get_info('site_info');

                
                if ($this->form_validation->run() == false) {
                    # code..
                    $this->_render_page('html/index', $this->data);
                } else {
                    $data = $this->input->post();
                  

                    $array['option_value'] = json_encode($data);

                       
                    $this->db->where('option_key', 'smtp');
                    if ($this->db->update('site_options', $array)) {
                        $this->session->set_flashdata('success', 'Setting saved');
                    } else {
                        $this->session->set_flashdata('failed', 'setting failed to saved');
                    }

                    redirect('admin/site-options');
                }
            }
        }
