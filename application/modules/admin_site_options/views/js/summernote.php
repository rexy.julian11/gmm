<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script>
$(document).ready(function() {
    var summer = <?php echo isset($about->content) ? json_encode($about->content) : "'Description Greetings Goes Here'";?>;
    $('#summernote').summernote({
        tabSize: 2,
        height: 300
    });
    $('#summernote').summernote('code', summer);
});
</script>