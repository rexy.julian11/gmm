 <?php echo form_open_multipart(site_url('admin/site-options/save-info')); ?>

 <div class="form-group">
     <label for="logo" class="control-label">Logo <span class='text-danger'>*</span></label>
     <div class="custom-file">
         <input id="logo" type="file" name="logo" class="custom-file-input">
         <label for="logo" class="custom-file-label">Pilih</label>
     </div>
     <?php echo ($this->session->flashdata('error_logo')!='') ? $this->session->flashdata('error_logo') : ''; ?>
     <?php echo form_error('logo', '<div class="error" style="color:red">','</div>'); ?>
     <?php echo  isset($site_info->logo) ? "<input type='hidden' name='current_logo' value='$site_info->logo'>" : ''; ?>
     <img class="img img-responsive" style="width:20%"
         src="<?php echo base_url('/uploads/'); echo isset($site_info) ? $site_info->logo: null;?>" alt="">
 </div>
 <div class="form-group">
     <label for="name">Name <span class="text-danger">*</span></label>
     <input name="name" id="" class="form-control"
         value="<?php echo isset($site_info) ? $site_info->name : set_value("name"); ?>">
     <?php echo form_error('name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
 </div>

 <div class="form-group">
     <label for="address">Address<span class="text-danger">*</span></label>
     <input name="address" id="" class="form-control"
         value="<?php echo isset($site_info) ? $site_info->address : set_value("address"); ?>">
     <?php echo form_error('address', '<span class="text-danger" style="form-size: 10px">','</span>');?>
 </div>

 <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="phone">Phone <span class="text-danger">*</span></label>
            <input name="phone" id="" class="form-control"
                value="<?php echo isset($site_info) ? $site_info->phone : set_value("phone"); ?>">
            <?php echo form_error('phone', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="email">Email <span class="text-danger">*</span></label>
            <input name="email" id="" class="form-control"
                value="<?php echo isset($site_info->email) ? $site_info->email : set_value("email"); ?>">
            <?php echo form_error('email', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
    </div>
 </div>
 <div class="form-group">
    <label for="email">Location <span class="text-danger">*</span></label>
    <input name="map" id="" class="form-control"
        value="<?php echo isset($site_info->map) ? htmlentities($site_info->map) : set_value("map"); ?>">
    <?php echo form_error('map', '<span class="text-danger" style="form-size: 10px">','</span>');?>
 </div>

 <div class="mt-5">
     <?php echo btn_submit('Save'); ?>
 </div>
 <?php echo form_close();?>
