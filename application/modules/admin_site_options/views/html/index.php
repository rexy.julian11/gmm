<div class='page-content mb-5'>
    <div class="tabs-container">
        <ul class="nav nav-tabs" role="tablist">
            <li><a class="btn btn-outline mr-2 mb-2 btn-primary nav-link active" data-toggle="tab" href="#tab-1"> Info
                    Situs</a>
            </li>

            <li><a class="btn btn-outline mr-2 mb-2 btn-primary nav-link" data-toggle="tab" href="#tab-3">SMTP</a>
            </li>
        </ul>
        <div class="tab-content">
            <!-- Info Perusahaan -->
            <div role="tabpanel" id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <?php include('form_site_options.php'); ?>
                </div>
            </div>
            <!-- End Info Perusahaan -->

            <div role="tabpanel" id="tab-3" class="tab-pane">
                <div class="panel-body">
                    <?php include('form_smtp.php'); ?>
                </div>
            </div>
        </div>


    </div>
</div>
