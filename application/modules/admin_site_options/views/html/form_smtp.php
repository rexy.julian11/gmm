<?php echo form_open('admin/site-options/save-smtp'); ?>
<div class="row">
    <div class="col-6">
        <h3>SMTP Configuration</h3>
        <div class="form-group">
            <label for="host">Host <span class="text-danger">*</span></label>
            <input name="host" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->host : set_value("host"); ?>">
            <?php echo form_error('host', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="username">Username <span class="text-danger">*</span></label>
            <input name="username" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->username : set_value("username"); ?>">
            <?php echo form_error('username', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="password">Password <span class="text-danger">*</span></label>
            <input name="password" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->password : set_value("password"); ?>">
            <?php echo form_error('password', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="security">Security <span class="text-danger">*</span></label>
            <select name="security" id="" class="form-control">
                <option value="ssl">SSL </option>
                <option value="tls"
                    <?php echo isset($smtp) && $smtp->security == 'tls' ? "selected='selected'" : ''; ?>>TLS
                </option>
            </select>
            <?php echo form_error('security', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="port">Port <span class="text-danger">*</span></label>
            <input name="port" id="" class="form-control col-3"
                value="<?php echo isset($smtp) ? $smtp->port : set_value("port"); ?>">
            <?php echo form_error('port', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
        <div class="form-group">
            <label for="format">Format <span class="text-danger">*</span></label>
            <select name="format" id="" class="form-control">
                <option value="html">HTML</option>
                <option value="text"
                    <?php echo isset($smtp) && $smtp->format == 'text' ? "selected='selected'" : ''; ?>>Text</option>
            </select>
            <?php echo form_error('format', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
    </div>
    <div class="col-6">
        <h3>Sender Data</h3>
        <div class="form-group">
            <label for="from_email">From Email <span class="text-danger">*</span></label>
            <input name="from_email" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->from_email : set_value("from_email"); ?>">
            <?php echo form_error('from_email', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="from_name">From name <span class="text-danger">*</span></label>
            <input name="from_name" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->from_name : set_value("from_name"); ?>">
            <?php echo form_error('from_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>

        <div class="form-group">
            <label for="reply_to">Reply-to Email <span class="text-danger">*</span></label>
            <input name="reply_to" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->reply_to : set_value("reply_to"); ?>">
            <?php echo form_error('reply_to', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
        <div class="form-group">
            <label for="reply_to_name">Reply-to Name <span class="text-danger">*</span></label>
            <input name="reply_to_name" id="" class="form-control"
                value="<?php echo isset($smtp) ? $smtp->reply_to_name : set_value("reply_to_name"); ?>">
            <?php echo form_error('reply_to_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
        </div>
    </div>
</div>
<div class="row container">
    <?php echo btn_submit('Save'); ?>
</div>
<?php echo form_close() ?>
