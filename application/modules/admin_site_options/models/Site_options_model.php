<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Site_options_model extends CI_Model
        {
            private $table = '';
            public function __construct()
            {
                parent::__construct();
            }
            public function get_info($key){
                $conf = $this->db->get_where('site_options', array('option_key'=> $key))->row();
                $conf = isset($conf->option_value) ? json_decode($conf->option_value) : '';
                return $conf;
            }
            
        }
