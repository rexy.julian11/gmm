<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_barang_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getSupplier()
    {
        $supplier = $this->db->select('u.*')
                             ->join('users_groups as ug', 'ug.user_id = u.id')
                             ->where('ug.group_id', 6)
                             ->get('users as u')->result();
        return $supplier;
    }
    public function insert($data)
    {
        return $this->db->insert('items', $data);
    }
    public function getItem($item_id)
    {
        $this->db->where('item_id', $item_id);
        return $this->db->get('items')->row();
    }
    public function update($item_id, $data)
    {
        $this->db->where('item_id', $item_id);
        return $this->db->update('items', $data);
    }
    public function delete($item_id)
    {
        $this->db->where('item_id', $item_id);
        return $this->db->delete('items');
    }
}