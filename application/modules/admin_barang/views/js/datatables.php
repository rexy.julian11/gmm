<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
	var datatable = null;
    $(document).ready(function(){
        $('.selectSupplier').select2({
            placeholder : 'Select Supplier'
        });
        datatable = $('.datatables').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ajax": {
                "url": '<?php echo site_url('admin_barang/datatables'); ?>',
                "type": "POST"
            },
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                                { "searchable": false, 
                                  "targets"   : 0 }
                            ],
            "columns": [
                {"data":'',"sortable":false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {"data": "item_name"},
                {"data": "item_buy_price"},
                {"data": "item_price"},
                {"data": "item_stock"},
                {"data": "supplier"},
                {"data": "action","ordering":false}
            ], 
        });


    });
	
</script>