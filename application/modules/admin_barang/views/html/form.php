<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Barang
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Item Name *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="item_name" value="<?= isset($item) ? $item->item_name : ''?>">
                                <?php echo form_error('item_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>               
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Item Buy Price *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="item_buy_price" value="<?= isset($item) ? $item->item_buy_price : ''?>">
                                <?php echo form_error('item_buy_price', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>               
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Item Sell Price *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="item_price" value="<?= isset($item) ? $item->item_price : ''?>">
                                <?php echo form_error('item_price', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>               
                        <div class="form-group row">
                            <?php if (!isset($item)) { ?>
                                <label class="col-sm-2 col-form-label">Item Stock *</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="item_stock" value="<?= isset($item) ? $item->item_stock : ''?>">
                                    <?php echo form_error('item_stock', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                </div>
                            <?php }?>
                        </div>               
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Suppier *</label>
                            <div class="col-sm-6">
                                <select name="supplier_id" class="form-control selectSupplier" id="">
                                    <?php foreach ($supplier as $row) { ?>
                                        <option value="<?= $row->id?>"><?= $row->first_name . ' ' . $row->last_name?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>               
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>