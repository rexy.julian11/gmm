<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_barang extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_barang_model');
        $this->breadcrumb->add('Admin barang',base_url().'admin_barang');
        $this->data['title'] = 'Admin barang';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_barang/js/datatables'); //load aditional js script
        $this->_render_page('admin_barang/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('i.item_id as id, i.item_name as item_name,i.item_buy_price as item_buy_price, i.item_price as item_price, i.item_stock as item_stock, 
                                        concat(u.first_name, " ",u.last_name) as supplier');
            $this->datatables->from('items as i');
            $this->datatables->join('users as u', 'u.id = i.supplier_id');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="barang/edit/$1" title="edit">
                    <i class="fa fa-pencil">
                    </i>
                    <a class="btn btn-icon btn-sm btn-danger mr-1" href="barang/delete/$1" title="delete">
                    <i class="fa fa-trash-o">
                    </i>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        $this->data['js_script'] = array('admin_barang/js/datatables'); //load aditional js script
        $this->load->library('form_validation');
        $this->form_validation->set_rules('item_name', 'Item Name', 'required');
        $this->form_validation->set_rules('item_price', 'Item Price', 'required|numeric');
        $this->form_validation->set_rules('item_stock', 'Item Stock', 'required|numeric');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['supplier'] = $this->Admin_barang_model->getSupplier();
            $this->_render_page('admin_barang/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if ($this->Admin_barang_model->insert($data)) {
                $this->session->set_flashdata('success', 'Successfull create data');
                redirect('admin/barang', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/barang/form', 'refresh');
            }
        
        }

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->data['js_script'] = array('admin_barang/js/datatables'); //load aditional js script
        $this->load->library('form_validation');
        $this->form_validation->set_rules('item_name', 'Item Name', 'required');
        $this->form_validation->set_rules('item_price', 'Item Price', 'required|numeric');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['supplier'] = $this->Admin_barang_model->getSupplier();
            $this->data['item']     = $this->Admin_barang_model->getItem($id);
            $this->_render_page('admin_barang/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if ($this->Admin_barang_model->update($id, $data)) {
                $this->session->set_flashdata('success', 'Successfull create data');
                redirect('admin/barang', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/barang/form', 'refresh');
            }
        
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        if ($this->Admin_barang_model->delete($id)) {
            $this->session->set_flashdata('success', 'Successfull delete data');
            redirect('admin/barang', 'refresh');
        }
        else{
            $this->session->set_flashdata('failed', 'Check your data and try again');
            redirect('admin/barang', 'refresh');
        }
    }
    
}