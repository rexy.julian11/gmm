<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_faktur_penjualan extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_faktur_penjualan_model');
        $this->load->model('admin_journal/Admin_journal_model');
        $this->breadcrumb->add('Admin faktur penjualan',base_url().'admin_faktur_penjualan');
        $this->data['title'] = 'Admin faktur penjualan';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_faktur_penjualan/js/datatables'); //load aditional js script
        $this->_render_page('admin_faktur_penjualan/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('so.so_id as id,CONCAT(customer.first_name, " ",customer.last_name ) as customer_name, so_date, so_bill, so_expired_date');
            $this->datatables->from('sales_orders as so');
            $this->datatables->join('users as customer', 'customer.id = so.customer_id', 'left');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="faktur-penjualan/edit/$1" title="edit">
                    <i class="fa fa-pencil">
                    </i>
                    <a class="btn btn-icon btn-sm btn-danger mr-1" href="faktur-penjualan/delete/$1" title="delete">
                    <i class="fa fa-trash-o">
                    </i>',
                "id");
            $this->datatables->add_column(
                'invoice',
                '<span class="badge badge-primary">Detail</span>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->data['js_script'] = array('admin_faktur_penjualan/js/form');
        $this->data['so_number'] = 'SLS' . time();
        $this->_render_page('admin_faktur_penjualan/html/form', $this->data);
        
    }
    public function insert()
    {
        $data = $this->input->post(NULL);
        $insert['so_id']            = $data['so_number'];
        $insert['so_date']          = $data['so_date'];
        $insert['so_bill']          = $data['so_bill'];
        $insert['so_expired_date']  = $data['so_date_expired'];
        $insert['customer_id']      = $data['customer_id'];
        if ($po = $this->Admin_faktur_penjualan_model->insert($insert)) {
            foreach ($data['data_barang'] as $row) {
                $insertItem['so_id']        = $data['so_number'];
                $insertItem['item_id']      = $row['item_id'];
                $insertItem['item_price']   = $row['item_price'];
                $insertItem['item_qty']     = $row['item_qty'];
                $insertItem['item_total']   = $row['item_total'];
                $this->Admin_faktur_penjualan_model->insertItemDetail($insertItem);
            }
            foreach ($data['data_jasa'] as $row) {
                $insertService['so_id']           = $data['so_number'];
                $insertService['service_id']      = $row['service_id'];
                $insertService['sos_price']       = $row['sos_price'];
                $insertService['sos_duration']    = $row['sos_duration'];
                $insertService['sos_unit']        = $row['sos_unit'];
                $insertService['sos_qty']         = $row['sos_qty'];
                $insertService['sos_total']       = $row['sos_total'];
                $this->Admin_faktur_penjualan_model->insertServiceDetail($insertService);
            }
            $insertJournal['journal_description']   = 'Penjualan' . ' ' . $data['so_number'];
            $insertJournal['journal_ref_pk']              = $data['so_number'];
            if ($journal = $this->Admin_journal_model->insertJournal($insertJournal)) {
                
                $insertLedger[0]['journal_id']         = $journal;
                $insertLedger[0]['account_number']     = '1-3.1';
                $insertLedger[0]['ledger_increase_to'] = 'Db';
                $insertLedger[0]['ledger_amount']      = (float)$data['so_bill'];
                
                $insertLedger[1]['journal_id']         = $journal;
                $insertLedger[1]['account_number']     = '1-4';
                $insertLedger[1]['ledger_increase_to'] = 'Cr';
                $insertLedger[1]['ledger_amount']      = (float)$data['so_bill'];
                
                for ($i=0; $i < 2 ; $i++) { 
                    $this->Admin_journal_model->insertLedger($insertLedger[$i]); 
                }
                echo 'true';
            }
            else{
                echo 'false';
            }
        }else{
            echo 'false';
        }
        
    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->data['js_script'] = array('admin_faktur_penjualan/js/edit');
        $this->data['so'] = $this->Admin_faktur_penjualan_model->getPurchaseData($id);
        $this->data['id'] = $id;
        $this->_render_page('admin_faktur_penjualan/html/edit', $this->data);
    }
    public function update($id)
    {
        $data = $this->input->post(NULL);
        $update['so_id']            = $data['so_number'];
        $update['so_date']          = $data['so_date'];
        $update['so_bill']          = $data['so_bill'];
        $update['so_expired_date']  = $data['so_date_expired'];
        $update['customer_id']      = $data['customer_id'];
        if ($po = $this->Admin_faktur_penjualan_model->update($id, $update)) {
            $this->Admin_faktur_penjualan_model->deleteItemDetail($id);
            $this->Admin_faktur_penjualan_model->deleteServiceDetail($id);
            foreach ($data['data_barang'] as $row) {
                $updateItem['so_id']        = $id;
                $updateItem['item_id']      = $row['item_id'];
                $updateItem['item_price']   = $row['item_price'];
                $updateItem['item_qty']     = $row['item_qty'];
                $updateItem['item_total']   = $row['item_total'];
                $this->Admin_faktur_penjualan_model->insertItemDetail($updateItem);
            }
            foreach ($data['data_jasa'] as $row) {
                $updateService['so_id']          = $id;
                $updateService['service_id']     = $row['service_id'];
                $updateService['sos_price']      = $row['sos_price'];
                $updateService['sos_unit']       = $row['sos_unit'];
                $updateService['sos_duration']   = $row['sos_duration'];
                $updateService['sos_qty']        = $row['sos_qty'];
                $updateService['sos_total']      = $row['sos_total'];
                $this->Admin_faktur_penjualan_model->insertServiceDetail($updateService);
            }
            $journal = $this->Admin_faktur_penjualan_model->getJournal($data['so_number']);
            $ledger = $this->Admin_faktur_penjualan_model->deleteLedger($journal->journal_id);
            
            $insertLedger[0]['journal_id']         = $journal->journal_id;
            $insertLedger[0]['account_number']     = '1-3.1';
            $insertLedger[0]['ledger_increase_to'] = 'Db';
            $insertLedger[0]['ledger_amount']      = (float)$data['so_bill'];
            
            $insertLedger[1]['journal_id']         = $journal->journal_id;
            $insertLedger[1]['account_number']     = '1-4';
            $insertLedger[1]['ledger_increase_to'] = 'Cr';
            $insertLedger[1]['ledger_amount']      = (float)$data['so_bill'];
                
            for ($i=0; $i < 2 ; $i++) { 
                $this->Admin_journal_model->insertLedger($insertLedger[$i]); 
            }
            echo 'true';
        }else{
            echo 'false';
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        if ($this->Admin_faktur_penjualan_model->deleteSO($id)) {
            $this->Admin_faktur_penjualan_model->deleteItemDetail($id);
            $this->Admin_faktur_penjualan_model->deleteServiceDetail($id);
            $journal = $this->Admin_faktur_penjualan_model->getJournal($id);
            $this->Admin_faktur_penjualan_model->deleteLedger($journal->journal_id);
            $this->Admin_faktur_penjualan_model->deleteJournal($id);

            
            $this->session->set_flashdata('success', 'Successfull delete account');
            redirect('admin/faktur-penjualan', 'refresh');
        }
        else{
            $this->session->set_flashdata('failed', 'Check your data and try again');
            redirect('admin/faktur-penjualan', 'refresh');
        }
    }
    public function getPurchaseDetail()
    {   
        $so_id = $this->input->post('so_id');
        $detail = $this->Admin_faktur_penjualan_model->getPurchaseDetail($so_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($detail));
    }

    public function getCustomer()
    {
        $customer = $this->Admin_faktur_penjualan_model->getCustomer();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($customer));
    }
    public function getCustomerData($customer_id)
    {
        $customer = $this->Admin_faktur_penjualan_model->getCustomerData($customer_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($customer));
    }
    public function getItems()
    {
        $items = $this->Admin_faktur_penjualan_model->getItems();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($items));
    }
    public function getItemData($item_id)
    {
        $item = $this->Admin_faktur_penjualan_model->getItemData($item_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($item));
    }
    public function getServices()
    {
        $services = $this->Admin_faktur_penjualan_model->getServices();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($services));
    }
    public function getServiceData($service_id)
    {
        $service = $this->Admin_faktur_penjualan_model->getServiceData($service_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($service));
    }
    public function getPoDetail($so_id)
    {
        $detail = $this->Admin_faktur_penjualan_model->getPoDetail($so_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($detail));
    }
    public function getInvoice($so_id)
    {
        $invoice = $this->Admin_faktur_penjualan_model->getInvoice($so_id);    
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($invoice));
    }
    public function getAccounts()
    {
        $accounts = $this->Admin_faktur_penjualan_model->getAccounts();    
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($accounts));
    }
}