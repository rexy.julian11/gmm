<style>
    .center{
        width: 150px;
        margin: 40px auto;   
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Faktur Penjualan
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">No Sales *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="so_number" value="<?= $id?>" readonly>
                                        <?php echo form_error('so_number', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>               
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="so_date" value="<?= isset($so) ? date("Y-m-d", strtotime($so->so_date)) : ''?>">
                                        <?php echo form_error('so_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Total Tagihan *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="so_bill" value="<?= isset($so) ? $so->so_bill : ''?>" readonly>
                                        <?php echo form_error('so_bill', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Jatuh Tempo *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="so_date_expired" value="<?= isset($so) ? date("Y-m-d", strtotime($so->so_expired_date)) : ''?>">
                                        <?php echo form_error('so_date_expired', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Customer *</label>
                                    <div class="col-sm-8">
                                        <select name="" id="" class="form-control selectCustomer">
                                        </select>
                                    </div>
                                </div>
                                <div class="alert alert-dismissible alert-secondary">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($so) ? $so->customer_name : ''?>" id="inputNama" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Telepon </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($so) ? $so->phone : ''?>" id="inputTelp" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Alamat </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($so) ? $so->address : ''?>" id="inputAlamat" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Barang *</label>
                                <div class="col-sm-4">
                                    <select name="" id="" class="form-control selectBarang">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary btnAddBarang"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col" style="display:none;">id</th>
                                        <th scope="col">-</th>
                                        <th scope="col">Barang</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" class="pull-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblBarang">
                                    <?php if (isset($so->items)) {
                                        $total_item = 0;
                                        foreach ($so->items as $row) {
                                        $total_item = $total_item + $row->item_total;
                                        ?>
                                        <tr>
                                            <td style="display:none"><?=$row->item_id?></td>
                                            <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                            <td><?=$row->item_name?></td>
                                            <td class="tblPrice"><?=$row->item_price?></td>
                                            <td><div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btnMinus" <?= isset($row->item_qty) && $row->item_qty <= 1 ? 'disabled="disabled"' : ''?>>
                                                            <span class="glyphicon glyphicon-minus"></span>
                                                        </button>
                                                    </span>
                                                    <input type="text" name="" class="form-control input-number qty" value="<?= $row->item_qty?>" min="1" data-qty="<?= $row->item_stock?>">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btnPlus">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                    </span>
                                                </div></td>
                                            <td class="tblTotal pull-right"><?=$row->item_total?></td>
                                        </tr>
                                    <?php    
                                        }
                                     }?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jasa *</label>
                                <div class="col-sm-4">
                                    <select name="" id="" class="form-control selectJasa">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary btnAddJasa"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col" style="display:none;">id</th>
                                        <th scope="col">-</th>
                                        <th scope="col">Service</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Satuan</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" style="width:150px">Durasi</th>
                                        <th scope="col" class="pull-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblJasa">
                                <?php if (isset($so->services)) {
                                        $total_service = 0;
                                        foreach ($so->services as $row) {
                                        $total_service = $total_service + $row->sos_total;
                                    ?>
                                    <tr>
                                        <td style="display:none"><?=$row->service_id?></td>
                                        <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                        <td><?=$row->service_name?></td>
                                        <td class="tblPrice"><?=$row->sos_price?></td>
                                        <td><?=$row->sos_unit?></td>
                                        <td><div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnMinus" <?= isset($row->sos_qty) && $row->sos_qty <= 1 ? 'disabled="disabled"' : ''?>>
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="" class="form-control input-number qty" value="<?= $row->sos_qty?>" min="1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnPlus">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnMinus" <?= isset($row->sos_duration) && $row->sos_duration <= 1 ? 'disabled="disabled"' : ''?>>
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="" class="form-control input-number durasi" value="<?= $row->sos_duration?>" min="1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnPlus">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="tblTotal pull-right"><?=$row->sos_total?></td>
                                    </tr>
                                    <?php    
                                        }
                                     }?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"><?=$total_service?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Grand Total </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right inputGrandTotal" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Terbilang </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right terbilang" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary pull-right btnSubmit">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>