<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_faktur_penjualan/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {"data": "id"},
                    {"data": "so_date"},
                    {"data": "so_bill"},
                    {"data": "so_expired_date"},
                    {"data": "customer_name"},
                    {
                        "className":      'so_id',
                        "orderable":      false,
                        "data":           'invoice',
                        "defaultContent": ''
                    },
                ], 
            });
    
            $('.datatables tbody').on('click', 'td.so_id', function () {
                var tr = $(this).closest('tr');
                var row = datatable.row( tr );
                var id = row.data().id;
                $.ajax({
                    url : '<?= site_url('admin/faktur-penjualan/getinvoice/')?>' + id,
                    type : 'GET'
                }).done(function(r){
                    console.log(r);
                    $('#modalInvoice').html(r.so_id);
                    $('#modalcustomer').html(r.customer_name);
                    $('#modalPhone').html(r.phone);
                    $('#modalAddress').html(r.address);
                    $('#modalDate').html('<strong>Tanggal : </strong>' + r.so_date);
                    $('#modalDateExpired').html('<strong>Tanggal Kadaluarsa : </strong>' + r.so_expired_date);
                    var htmlItem = ``;
                    var htmlService = ``;
                    var subTotalItem = 0;
                    var subTotalService = 0;
                    $.each(r.items, function(i, data){
                        htmlItem += `<tr>
                                        <td>${data.item_name}</td>
                                        <td>${data.item_price}</td>
                                        <td>${data.item_qty}</td>
                                        <td>${data.item_total}</td>
                                    </tr>`;
                        subTotalItem = subTotalItem + parseInt(data.item_total);
                    });
                    $.each(r.services, function(i, data){
                        htmlService += `<tr>
                                        <td>${data.service_name}</td>
                                        <td>${data.sos_price}</td>
                                        <td>${data.sos_unit}</td>
                                        <td>${data.sos_qty}</td>
                                        <td>${data.sos_duration}</td>
                                        <td>${data.sos_total}</td>
                                    </tr>`;
                        subTotalService = subTotalService + parseInt(data.sos_total);
                    });
                    let grandTotal = subTotalItem + subTotalService;
                    $('#modalSubItem').html(subTotalItem);
                    $('#modalSubService').html(subTotalService);
                    $('#modalTotal').html(grandTotal);
                    // $('#modalTerbilang').html(terbilang(grandTotal.toString(), 'Rupiah'));
                    $('#modalTblItem').html(htmlItem);
                    $('#modalTblService').html(htmlService);
                    $('#modalDetail').modal('show');
        
                });
            } );
        });
        </script>