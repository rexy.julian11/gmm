<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_faktur_penjualan_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('sales_orders', $data);
    }
    public function insertItemDetail($data)
    {
        return $this->db->insert('sales_order_item_details', $data);
    }
    public function insertServiceDetail($data)
    {
        return $this->db->insert('sales_order_service_details', $data);
    }
    public function update($so_id, $data)
    {
        $this->db->where('so_id', $so_id);
        return $this->db->update('sales_orders', $data);
    }
    public function deleteSO($so_id)
    {
        return $this->db->where('so_id', $so_id)
                        ->delete('sales_orders');
    }
    public function deleteItemDetail($so_id)
    {
        $this->db->where('so_id', $so_id);
        return $this->db->delete('sales_order_item_details');
    }
    public function deleteServiceDetail($so_id)
    {
        $this->db->where('so_id', $so_id);
        return $this->db->delete('sales_order_service_details');
    }
    public function getPurchaseData($so_id)
    {
        $purchase = $this->db->select('so.*, CONCAT(customer.first_name, " ", customer.last_name) as customer_name, customer.phone, customer.address')
                             ->join('users as customer', 'customer.id = so.customer_id')
                             ->where('so.so_id', $so_id)
                             ->get('sales_orders as so')->row();
        $items = $this->db->select('soi.*, i.item_name, i.item_stock')
                          ->join('items as i', 'i.item_id = soi.item_id')
                          ->where('soi.so_id', $so_id)
                          ->get('sales_order_item_details as soi')->result();
        $services = $this->db->select('sos.*, s.service_name')
                             ->join('services as s', 's.service_id = sos.service_id')
                             ->where('sos.so_id', $so_id)
                             ->get('sales_order_service_details as sos')->result();
        $purchase->items = $items;
        $purchase->services = $services;

        return $purchase;
    }
    public function getPurchaseDetail($so_id)
    {
        return $this->db->select('so.*, items.item_name')
                        ->join('items', 'items.item_id = so.item_id')
                        ->where('so.so_id', $so_id)
                        ->get('purchase_order_details as so')->result();
    }
    public function getCustomer()
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as text')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 5)
                        ->get('users as u')->result();
    }
    public function getCustomerData($customer_id)
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as name, phone, address')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 5)
                        ->where('u.id', $customer_id)
                        ->get('users as u')->row();
    }
    public function getItems() 
    {
        return $this->db->select('item_id as id, item_name as text')
                        ->get('items')->result();
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function getServices()
    {
        return $this->db->select('service_id as id, service_name as text')
                        ->get('services')->result();
    }
    public function getServiceData($service_id)
    {
        return $this->db->where('service_id', $service_id)
                        ->get('services')->row();
    }
    public function getPoDetail($so_id)
    {
        $data['items'] = $this->db->select('soi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = soi.item_id')
                                  ->where('soi.so_id', $so_id)
                                  ->get('sales_order_item_details as soi')->result();
        $data['services'] = $this->db->select('sos.*, s.service_name')
                                     ->join('services as s', 's.service_id = sos.service_id')
                                     ->where('sos.so_id', $so_id)
                                     ->get('sales_order_service_details as sos')->result();
        return $data;
    }
    public function getInvoice($so_id)
    {
        $invoice = $this->db->select('so.*, concat(customer.first_name, " ", customer.last_name) as customer_name, customer.address, customer.phone')
                            ->join('users as customer', 'customer.id = so.customer_id')
                            ->where('so.so_id', $so_id)
                            ->get('sales_orders as so')->row();
        $invoice->items = $this->db->select('soi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = soi.item_id')
                                  ->where('soi.so_id', $so_id)
                                  ->get('sales_order_item_details as soi')->result();
        $invoice->services = $this->db->select('sos.*, s.service_name')
                                     ->join('services as s', 's.service_id = sos.service_id')
                                     ->where('sos.so_id', $so_id)
                                     ->get('sales_order_service_details as sos')->result();
        return $invoice;
    }
    public function getAccounts()
    {
        return $this->db->select('account_number as id, account_name as text')
                        ->where('account_type', 'Account')
                        ->get('accounts')->result();
    }
    public function doPayment($data)
    {
        return $this->db->insert('payment', $data);
    }
    public function getJournal($journal_ref_pk)
    {
        return $this->db->where('journal_ref_pk', $journal_ref_pk)
                        ->get('journals')->row();
    }
    public function deleteJournal($po_id)
    {
        return $this->db->where('journal_ref_pk', $po_id)
                        ->delete('journals');
    }
    public function getLedger($journal_id)
    {
        return $this->db->where('journal_id', $journal_id)
                        ->get('ledgers')->result();
    }
    public function deleteLedger($journal_id)
    {
        return $this->db->where('journal_id', $journal_id)
                        ->delete('ledgers');
    }
}