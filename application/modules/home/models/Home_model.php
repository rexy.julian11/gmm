<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Home_model extends CI_Model
        {
            private $table = '';
            public function __construct()
            {
                parent::__construct();
            }
            public function index()
            {
                
            }

            public function get_settings($opt) {
                $result = $this->db->select('option_value')->from('site_options')
                               ->where('option_key', $opt)
                               ->get()->row();
                $result = json_decode($result->option_value, TRUE);
                return $result;
            }

            public function get_products($limit) {
                $result = $this->db->select('*')->from('sm_products')
                                    ->where('bookable', TRUE)
                                    ->order_by('id', 'desc')
                                    ->limit($limit)
                                    ->get()->result();
                return $result;
            }

            public function get_portofolio($limit) {
                $result = $this->db->select('*')->from('sm_portofolio')
                                    ->limit($limit, 0)
                                    ->order_by('created', 'desc')
                                    ->get()->result();
                if (!empty($result)) {
                    foreach ($result as $res) {
                        $res->image = is_file('./uploads/portofolio/' . $res->image) &&
                                        file_exists('./uploads/portofolio/' . $res->image)
                                        ? base_url('uploads/portofolio/' . $res->image)
                                        : base_url('uploads/no-image.jpg');
                    }
                }
                return $result;
            }

            public function get_review() {
                $hasil = $this->db->select('*')->from('sm_review')
                                                ->where('status', 'publish')
                                                ->get()->result();
                return $hasil;
            }

            public function get_posts($limit) {
                $array = array('status' => 'publish', 'publish_date <=' => date('Y-m-d'));
                return $this->db->select('*')->from('sm_blog')
                                ->where($array)
                                ->order_by('publish_date', 'DESC')
                                ->limit($limit, 0)
                                ->get()->result();
            }
            
        }