<?php defined('BASEPATH') or exit('No direct script access allowed');

        class Home extends Frontend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Home_model', 'home');
                // $this->breadcrumb->add('Home',base_url().'home');
                // $this->data['title'] = 'Home';
            }
            public function index()
            {
                $this->data['header'] = array(base_url().'assets/css/style.css','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css'); //load aditional stylesheets
                $this->data['js_library'] = array('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js','https://rawgit.com/moment/moment/2.2.1/min/moment.min.js'); //load aditional js library
                $this->data['js_script'] = array('home/js/js'); //load aditional js script
                $product = $this->home->get_products(6);
                if(!empty($product)) {
                    foreach ($product as $p) {
                        $p->image = is_file('uploads/product/'.$p->image) && file_exists('uploads/product/'.$p->image)
                        ? base_url('uploads/product/'.$p->image) : base_url('uploads/no-image.jpg');
                    }
                }
                $this->data['products'] = $product;

                $this->data['settings'] = $this->home->get_settings('site_info');
                $this->_render_frontend_page('home/html/index', $this->data);
            }

            
            

            
            
                        
                        
    }
