<!--::banner part start::-->
<?php 
    $dir = dirname(__DIR__, 5);
    $banner = is_file($dir . '/uploads/' . $greetings['greeting_image']) && 
                file_exists($dir . '/uploads/' . $greetings['greeting_image'])
                ? base_url('uploads/'.$greetings['greeting_image'])
                : base_url('uploads/no-image.jpg')
?>
<style>
.banner_part {
    background-image: url('<?=$banner?>');
}

</style>
<section class="banner_part">
    <div class="container">
        <div class="row align-items-center justify-content-end">
            <div class="col-lg-5">
                <div class="banner_text text-center">
                    <div class="banner_text_iner">
                        <h5><?php echo isset($settings) ? $settings['name'] : NULL;?></h5>
                        <h1><?php echo isset($greetings['identify']) ? $greetings['identify'] : NULL;?></h1>
                        <p>Capturing moments from today</p>
                        <a href="#services" class="btn_1">Jadwalkan ke Studio</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--::banner part start::-->

<!--::about_us part start::-->
<section class="about_us padding_top">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-8">
                <div class="about_us_text text-center">
                    <h5>Hi, Kami <?php echo isset($settings['name']) ? $settings['name'] : NULL;?></h5>
                    <h2><?php echo isset($greetings['title']) ? $greetings['title'] : NULL;?></h2>
                    <p><?php echo isset($greetings['content']) ? $greetings['content'] : NULL;?></p>
                    <!-- <a href="#" class="btn_2">read more</a> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--::about_us part end::-->

<!-- gallery_part part start-->
<section class="gallery_part section_padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 offset-lg-2">
                <div class="section_tittle">
                    <p>Lihat Karya-karya terbaru kami</p>
                    <h2>Karya Terbaru</h2>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="portfolio-filter filters">
                    <ul>
                        <li class="active" data-filter="all">All photos</li>
                        <li data-filter="1"> weeding</li>
                        <li data-filter="2">fashion</li>
                        <li data-filter="3">portrait</li>
                        <li data-filter="4">magazine</li>
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-xl-12">
                <div class="gallery_part_item filtr-container">
                    <?php if(isset($porto) && !empty($porto)) :
                        foreach($porto as $p) : ?>
                    <a href="<?=site_url('portofolio/single/'.$p->slug)?>" class="img-gal filtr-item" data-category="1"
                        style="background-image: url('<?php echo $p->image?>')">
                        <div class="single_gallery_item">
                            <div class="single_gallery_item_iner">
                                <div class="gallery_item_text">
                                    <p><?php echo isset($p->photo_category) ? $p->photo_category : NULL;?></p>
                                    <h4><?php echo isset($p->title) ? $p->title : NULL;?></h4>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; endif;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- gallery_part part end-->

<!--::our_service part start::-->
<section class="our_service padding_bottom" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section_tittle">
                    <p>Pilih Layanan sebelum menjadwalkan kunjungan Anda</p>
                    <h2>Layanan Kami</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center filtr-container">
            <?php if (isset($products) && !empty($products)) :
                foreach ($products as $pro) :?>
            <div class="col-lg-4 col-md-6">
                <div class="single_offer_text text-center wedding"
                    style="background-image:url('<?=$pro->image?>'); background-color: rgba(255,255,255,0.7); background-blend-mode:lighten;">
                    <h4 style="color:black"><?php echo isset($pro->title) ? $pro->title : NULL;?></h4>
                    <input type="hidden" value="<?php echo isset($pro->slug) ? $pro->slug : NULL;?>">
                    <p style="color:black"><?php echo isset($pro->description) ? $pro->description : NULL;?></p>
                    <a href="#jadwal" class="btn_1 bokin">book now</a>
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
    <div class="row" id="jadwal" style="background: #000;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center p-3">
                        <h3 class="text-white">Pilih Waktu Kedatangan</h3>
                        <br>
                        <?php echo $this->session->flashdata('snap_error'); ?>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="container ">
                    <div class="row shadow" id="pilihtanggal">
                        <div class="col-lg-6 datepicker">

                        </div>
                        <div class="col-lg-6 ">
                            <div class="card-body p-3 p-sm-5">
                                <h4 class="text-center text-white">Pilih waktu yang tersedia</h4>
                                <div class="timeslots slideInLeft">
                                    <div class="row text-center mx-0 cell-wrap">
                                        <!-- Time slots will appear here by ajax call -->
                                    </div>
                                    <form action="<?php echo site_url('checkout/snap-order'); ?>" class="getSnap"
                                        method="post">
                                        <input type="hidden" name="date">
                                        <input type="hidden" name="slot">
                                        <input type="hidden" name="title">
                                        <input type="hidden" name="price">
                                        <input type="hidden" name="product_id">
                                        <button type="submit" disabled
                                            class="btn btn-default text-uppercase float-right">
                                            Selesaikan <i class="ti-angle-right"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="container d-flex justify-content-between">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--::our_service part end::-->



<!--::blog part start::-->
<section class="catagory_post padding_bottom">
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="section_tittle">
                    <p>Artikel</p>
                    <h2>Cerita kami</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if (isset($blog) && !empty($blog)) :
                foreach($blog as $b) : ?>
            <div class="col-sm-6 col-lg-4 mt-5">
                <div class="single_catagory_post post_2">
                    <div class="category_post_img">
                        <?php
                        $dir = dirname(__DIR__, 5);
                        $img = is_file($dir . "/uploads/blog/" . $b->image) &&
                                file_exists($dir . "/uploads/blog/" . $b->image)
                                ? base_url('uploads/blog/'. $b->image)
                                : base_url('uploads/no-image.jpg');
                    ?>
                        <img src="<?php echo $img?>" alt="<?php echo isset($b->image_alt) ? $b->image_alt : NULL;?>">
                    </div>
                    <div class="post_text_1 pr_30">
                        <h5><span> By <?php echo isset($b->author) ? $b->author : NULL;?>
                            </span> / <?php echo date('F d , Y')?></h5>
                        <a href="<?php echo site_url('blog/single/'.$b->slug)?>">
                            <h3><?php echo isset($b->title) ? $b->title : NULL;?></h3>
                        </a>
                        <p><?php echo isset($b->content) ? character_limiter($b->content, 100) : NULL;?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; endif;?>
        </div>
    </div>
</section>
<!--::blog part end::-->
