<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
//write your custom javascript here!
$(document).ready(function() {
    var base_url = "<?php echo site_url(); ?>";
    $('.timeslots').hide();
    $('#jadwal').hide();
    // $("button[type='submit']").removeAttr('disabled');
    $(".bokin").click(function(e) {
        $('#jadwal').show();
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500);
        e.preventDefault(); //this is the important line.
        var slug = $(e.target).closest(".single_offer_text").find("input[type='hidden']").val();
        $.ajax({
            url: base_url + "services/single",
            method: "POST",
            data: {
                slug: slug
            },
            success: function(response) {
                var serv = JSON.parse(response)
                var title = $('.getSnap').find("input[name='title']");
                var price = $('.getSnap').find("input[name='price']");
                var prodId = $('.getSnap').find("input[name='product_id']");
                title.val(serv.title);
                price.val(serv.price);
                prodId.val(serv.id);
            }
        });
    })
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d',
        setDate: new Date(),
    }).trigger('change').on('changeDate', function(e) {
        let tanggal = moment(e.date).format("YYYY-MM-DD");
        $("input[name='date']").val(tanggal);
        $('.timeslots').show();

        let day = moment(e.date).day() + 1;
        $.ajax({
            url: base_url + "booking/timeslots/",
            data: {
                day: day,
                tanggal: tanggal
            },
            success: function(e) {
                var html = "";
                var booked = "";
                e.forEach(el => {
                    let active = '';
                    let status = '';
                    if (el.active == 0) {
                        active = 'disabled';
                        status = 'disable';
                    }

                    if (el.booked == true) {
                        booked = 'booked';
                        active = 'disabled';
                    }
                    html +=
                        ` <button ${active} class="cell btn col-3 my-1 px-2 shadow ${booked} ${status}">${el.hour}</button>`;
                });
                $(".cell-wrap").html(html);
            }
        });
        $('body').on('click', '.cell', function() {
            $('.cell').removeClass('select');
            $(this).addClass('select');
            $("button[type='submit']").removeAttr('disabled');
            let slot = $(this).text()
            $("input[name='slot']").val(slot.trim());
        });

        $("button[type='submit']").on('click', function() {
            localStorage.setItem("time", $("input[name='slot']").val());
            localStorage.setItem("date", $("input[name='date']").val());
            localStorage.setItem("title", $("input[name='title']").val());
            localStorage.setItem("price", $("input[name='price']").val());
            window.location.href = base_url + 'checkout';
        })
    });






});
</script>
