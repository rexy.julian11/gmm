<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_supir_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }
    public function insertUserGroup($data)
    {
        return $this->db->insert('users_groups', $data);
    }
    public function getSupir($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('users')->row();
    }
    public function update($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('users', $data);
    }
    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('users');
    }
}