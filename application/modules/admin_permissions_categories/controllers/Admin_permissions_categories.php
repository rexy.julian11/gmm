<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_permissions_categories extends Backend_Controller
        {
            protected $tables;
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Permissions_categories_model');
                $this->breadcrumb->add('Permissions categories',base_url().'permissions_categories');
                $this->data['title'] = $this->lang->line('permissions_categories');
                $this->tables = $this->config->item('tables', 'ion_auth_acl');

            }
            
            final public function index()
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('js/datatables'); //load aditional js script
                
                $this->data['perm_categories'] = $this->ion_auth_acl->permissions_categories('full');
                $this->_render_page('html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
            
            }
            final public function add()
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                $this->breadcrumb->add('Add',site_url('permissions_categories/add'));
                $this->data['title'] = $this->lang->line('permissions_categories');

                $this->form_validation->set_rules('cat_name', $this->lang->line('cat_name_validation_label'), 'trim|required|is_unique['.$this->tables['permissions_categories'].'.cat_name]');
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->_render_page('html/form', $this->data);
                } else {
                    # code...
                    $data = $this->security->xss_clean($this->input->post());
                   if($this->ion_auth_acl->create_permission_category($data['cat_name']))
                   {
                       $this->session->set_flashdata('success', $this->ion_auth_acl->messages());
                       redirect('admin/permissions_categories');
                   }
                }
                
            
            }
            final public function edit($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id){
                    redirect($_SERVER['HTTP_REFERER']);
                }

                $this->breadcrumb->add('Edit',site_url('permissions_categories/edit'));
                $this->data['title'] = $this->lang->line('permissions_categories');

                $this->form_validation->set_rules('cat_name', $this->lang->line('cat_name_validation_label'), 'trim|required');
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->data['category'] = $this->ion_auth_acl->permissions_category_by_id($id);
                    $this->_render_page('html/form', $this->data);
                } else {
                    # code...
                    $data = $this->security->xss_clean($this->input->post());
                    if($this->ion_auth_acl->update_permission_category($data['id'], $data['cat_name'])){
                        
                        $this->session->set_flashdata('success_edit', $this->ion_auth_acl->messages());
                        redirect(site_url().'permissions_categories','refresh');
                        
                    }
                }
                
            }
            final public function delete($id)
            {
                //get permissions for each method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if(! $id or empty($id)){
                    redirect($_SERVER['HTTP_REFERER']);
                }

                if($this->ion_auth_acl->remove_permission_category($id)){
                    
                    $this->session->set_flashdata('success_delete', $this->ion_auth_acl->messages());
                    redirect('admin/permissions_categories');
                }
            
            }
        }
