<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?php echo btn_add('admin/permissions-categories/add',$this->lang->line('create_permissions_categories_title')); ?>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped datatable">
                            <thead>
                                <th>#</th>
                                <th>Permissions Categories</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($perm_categories as $cat): ?>
                                <tr>
                                    <td><?php echo $no++;  ?></td>
                                    <td><?php echo $cat['name'];  ?></td>
                                    <td>
                                        <?php echo btn_edit('admin/permissions-categories/edit/',$cat['id']); ?>
                                        <?php echo btn_delete('admin/permissions-categories/delete/',$cat['id']); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="ibox-footer"></div>
            </div>
        </div>
    </div>
</div>
