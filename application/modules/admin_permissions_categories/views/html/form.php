<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?php echo $title; ?>
                </div>
                <?php echo form_open(); ?>
                <?php if(isset($category)): ?>
                <input type="hidden" name="id" value="<?php echo $category->id; ?>">
                <?php endif; ?>
                <div class="ibox-content">
                    <div class="form-group">
                        <label for="cat_name"><?php echo $this->lang->line('cat_name_validation_label'); ?> <span
                                class="text-danger">*</span> <span class='text-muted'>( Class name without
                                underscore(_),
                                eg. "admin users" ) </span></label>
                        <input name="cat_name" id="" class="form-control" autofocus
                            value="<?php echo isset($category->cat_name) ? $category->cat_name : set_value("cat_name"); ?>">
                        <?php echo form_error('cat_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                </div>
                <div class="ibox-footer">
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit(isset($category) ? $this->lang->line('edit_categories_submit') : $this->lang->line('create_categories_submit')) ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
