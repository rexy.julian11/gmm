<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_stok_opname_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insertOpname($data)
    {
        return $this->db->insert('stock_opname', $data);
    }
    public function insertOpnameDetail($data)
    {
        return $this->db->insert('stock_opname_detail', $data);
    }
    public function getItems() 
    {
        return $this->db->select('item_id as id, item_name as text')
                        ->get('items')->result();
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function updateItem($item_id, $data)
    {
        return $this->db->where('item_id', $item_id)
                        ->update('items', $data);
    }
    public function getOpnameDetail($opname_id)
    {
        return $this->db->select('i.item_name, sod.*')
                        ->join('items as i', 'i.item_id = sod.item_id')
                        ->where('sod.so_id', $opname_id)
                        ->get('stock_opname_detail as sod')->result();
    }
}