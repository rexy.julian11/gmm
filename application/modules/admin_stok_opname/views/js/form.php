<script>
    $(document).ready(function(){
        
        function countSelisih(){
            $('.input-number').unbind().change(function(){
                var qty = parseInt($(this).closest('tr').find('.qty').val());
                var stock = parseInt($(this).closest('tr').find('.tblStock').html());
                var selisih = qty - stock;
                if (qty > 1) 
                {
                    $(this).closest('td').find('.btnMinus').attr('disabled', false);
                }else
                {
                    $(this).closest('td').find('.btnMinus').attr('disabled', true);
                }
                $(this).closest('tr').find('.tblSelisih').html(selisih);
            });
            $('.btnPlus').unbind().click(function(){
                var current = $(this).closest('td').find('.input-number');
                if (current > 1) {
                    current = 1;
                }
                var new_number = parseInt(current.val()) + 1;
                current.val(new_number).trigger('change');
            }); 
            $('.btnMinus').unbind().click(function(){
                var current = $(this).closest('td').find('.input-number');
                if (current > 1) {
                    current = 1;
                }
                var new_number = parseInt(current.val()) - 1;
                current.val(new_number).trigger('change');
            });

            $('.btnRemove').unbind().click(function(){
                if ($(this).closest('tr').is('tr:only-child')) {
                    $(this).closest('table').find('.grandTotal').html('');
                    $(this).closest('tr').remove();
                }
                else{
                    $(this).closest('tr').remove();
                }
            });
        }
        $('.selectBarang').select2({
            placeholder : 'Pilih Barang',
            ajax: {
                url: '<?= site_url('admin/stok-opname/getitems')?>',
                processResults: function (data) {
                return {
                    results: data
                };
                }
            }
        });
        var selected = [];
        $('.btnAddBarang').unbind().click(function(){
            var id = $('.selectBarang').val();
            if (selected.includes(id)) {
                $.notify("This item already selected", "warn");
            }
            else{
                selected.push(id);
                $.ajax({
                    url : '<?= site_url('admin/stok-opname/getitemdata/')?>' + id,
                    type : 'GET'
                }).done(function(response){
                    var html = `
                        <tr>
                            <td style="display:none">${response.item_id}</td>
                            <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                            <td>${response.item_name}</td>
                            <td class="tblStock">${response.item_stock}</td>
                            <td><div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btnMinus" disabled="disabled">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" name="" class="form-control input-number qty" value="1" min="1" max="10">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btnPlus">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div></td>
                            <td class="tblSelisih pull-right"></td>
                        </tr>
                    `;
                    $('.tblBarang').append(html);
                    countSelisih();

                    $('.btnSubmit').unbind().click(function(){
                        var opname_id           = $('input[name="opname_id"]').val();
                        var opname_date         = $('input[name="opname_date"]').val();
                        var opname_description  = $('textarea[name="opname_description"]').val();
                        var dataItems           = $('.tblBarang').find('tr');
                        var items               = [];
                        $.each(dataItems, function(i, data){
                            var td          = $(this).find('td');
                            var item_id     = td.eq(0).html();
                            var stock_s     = td.eq(3).html();
                            var stock_f     = td.eq(4).find('.qty').val();
                            var difference  = td.eq(5).html();
                            items.push({'item_id' : item_id, 'stock_s' : stock_s, 'stock_f' : stock_f, 'difference' : difference});
                        });
                        $.ajax({
                            url : '<?= site_url('admin/stok-opname/insert')?>',
                            type : 'POST',
                            data : {opname_id : opname_id,
                                    opname_date : opname_date,
                                    opname_description : opname_description,
                                    items : items}
                        }).done(function(r){
                            if (r == 'true') {
                                window.location.href = "<?= site_url('admin/stok-opname')?>";
                            }
                            else{
                                $.notify("Check Your Data", "warn");
                            }
                        })
                    });
                });
            }
        });
        
    }); 
</script>