<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_stok_opname/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columnDefs: [
                                { "searchable": false, 
                                  "targets"   : [0,4] }
                            ],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {"data": "id"},
                    {"data": "so_desc"},
                    {"data": "so_date"},
                    {
                        "className":      'so_detail',
                        "orderable":      false,
                        "data":           'detail',
                        "defaultContent": ''
                    }
                ], 
            });
            
            $('.datatables tbody').on('click', 'td.so_detail', function () {
                var tr = $(this).closest('tr');
                var row = datatable.row( tr );
                var id = row.data().id;
                $.ajax({
                    url : '<?= site_url('admin/stok-opname/getopnamedetail/')?>' + id,
                    type : 'GET'
                }).done(function(r){
                    $('#modalTblOpname').html('');
                    var  html = ``;
                    $.each(r, function(i, data){
                        html += `
                                    <tr>
                                        <td>${data.item_name}</td>
                                        <td>${data.sod_system_stock}</td>
                                        <td>${data.sod_physical_stock}</td>
                                        <td>${data.sod_difference}</td>
                                    </tr>
                                `;
                    });
                    $('#modalTblOpname').append(html);
                    $('#modalDetail').modal('show');        
                });
            });
    
        });
        </script>