<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Stok Opname
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">No Opname *</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="opname_id" value="<?= $opname_no?>" readonly>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="opname_date" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Judul Opname *</label>
                                <div class="col-sm-8">
                                    <textarea name="opname_description" id="" cols="45" rows="5" class="form-control"></textarea>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <hr>
                    <div class="divSection">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Barang *</label>
                            <div class="col-sm-4">
                                <select name="" id="" class="form-control selectBarang">
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-primary btnAddBarang"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <table class="table table-hover">
                            <thead class="table-active">
                                <tr>
                                    <th scope="col" style="display:none;">id</th>
                                    <th scope="col">-</th>
                                    <th scope="col">Barang</th>
                                    <th scope="col">Stok Sistem</th>
                                    <th scope="col" style="width:150px">Stok Fisik</th>
                                    <th scope="col" class="pull-right">Selisih</th>
                                </tr>
                            </thead>
                            <tbody class="tblBarang">

                            </tbody>
                        </table>
                    </div>
                    <div class="form-group row justify-content-end">
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary pull-right btnSubmit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>