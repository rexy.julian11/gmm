<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_stok_opname extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_stok_opname_model');
        $this->breadcrumb->add('Admin stok opname',base_url().'admin_stok_opname');
        $this->data['title'] = 'Admin stok opname';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_stok_opname/js/datatables'); //load aditional js script
        $this->_render_page('admin_stok_opname/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('so_id as id, so_date, so_desc');
            $this->datatables->from('stock_opname');
            $this->datatables->add_column(
                'detail',
                '<span class="badge badge-pill badge-primary">Detail</span>');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="stok-opname/edit/$1" title="edit">
                    <i class="fa fa-pencil">
                    </i>
                    <a class="btn btn-icon btn-sm btn-danger mr-1" href="stok-opname/delete/$1" title="delete">
                    <i class="fa fa-trash-o">
                    </i>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->data['js_script'] = array('admin_stok_opname/js/form'); //load aditional js script
        $this->data['opname_no'] = 'OPN' . time();
        $this->_render_page('admin_stok_opname/html/form', $this->data);

    }
    public function insert()
    {
        $data = $this->input->post(NULL);    
        $insert['so_id']     = $data['opname_id'];
        $insert['so_date']   = $data['opname_date'];
        $insert['so_desc']   = $data['opname_description'];
        if ($this->Admin_stok_opname_model->insertOpname($insert)) {
            foreach ($data['items'] as $row) {
                $insertDetail['so_id']              = $data['opname_id'];
                $insertDetail['item_id']            = $row['item_id'];
                $insertDetail['sod_system_stock']   = $row['stock_s'];
                $insertDetail['sod_physical_stock'] = $row['stock_f'];
                $insertDetail['sod_difference']     = $row['difference'];
                $this->Admin_stok_opname_model->insertOpnameDetail($insertDetail);

                $item = $this->Admin_stok_opname_model->getItemData($row['item_id']);
                $updateStock['item_stock'] = (int)$item->item_stock + (int)$row['difference'];
                $this->Admin_stok_opname_model->updateItem($row['item_id'], $updateStock);
            }
            echo 'true';
        }
        else{
            echo 'false';
        }
    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    public function getItems()
    {
        $items = $this->Admin_stok_opname_model->getItems();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($items));
    }
    public function getItemData($item_id)
    {
        $item = $this->Admin_stok_opname_model->getItemData($item_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($item));
    }
    public function getOpnameDetail($opname_id)
    {
        $opname = $this->Admin_stok_opname_model->getOpnameDetail($opname_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($opname));
    }
}