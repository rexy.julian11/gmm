<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>
                    <?php echo $title; ?>
                </div>
                <?php echo form_open(); ?>
                <div class='ibox-content'>
                    <div class="form-group">
                        <label for="name">Widget's Name <span class="text-danger">*</span></label>
                        <input name="name" id="" class="form-control"
                            value="<?php echo isset($var) ? $var : set_value("name"); ?>" required>
                        <?php echo form_error('name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <div class="form-group ">

                        <span>Group Pengguna</span><br>
                        <?php foreach ($groups as $group):?>
                        <?php
                                                    
                                                        $gID=$group['id'];
                                                        $checked = null;
                                                        $item = null;
                                                        if(isset($currentGroups)){
                                                        foreach($currentGroups as $grp) {
                                                            if ($gID == $grp->id) {
                                                                $checked= ' checked="checked"';
                                                            break;
                                                            }
                                                        }
                                                        }

                                                        ?>

                        <input type="checkbox" name="group_id[]" value="<?php echo $group['id'];?>"
                            <?php echo set_checkbox('group_id[]', $group['id']) ?> <?php echo $checked; ?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?><br>
                        <?php endforeach?>



                    </div>
                    <div class="form-group">
                        <label for="active">Active? </label><br>
                        <input name="active" id="" type='checkbox' class="js-switch" value='1'
                            <?php echo (isset($menu) && $menu->active == 0 ) ? '' : 'checked'; ?>>
                        <?php echo form_error('active', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                </div>
                <div class='ibox-footer'>
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit('Save') ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
