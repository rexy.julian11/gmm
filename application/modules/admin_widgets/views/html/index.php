<div class='page-content'>
    <div class='row'>
        <div class='col-12'>
            <div class='ibox'>
                <div class='ibox-title'>
                    <?php echo btn_add('admin/widgets/add', 'Tambah Widget'); ?>
                </div>
                <div class='ibox-content'>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Widget</th>
                            <th>Group</th>
                            <th>Active</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <?php $no=1;foreach($widget as $w): ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <td><?php echo $w->widget ?></td>
                                <td><?php echo $w->group ?></td>
                                <td><?php echo active($w->active); ?></td>
                                <td><?php echo btn_edit('admin/widgets/edit/',$w->id).btn_delete('admin/widgets/delete/',$w->id); ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class='ibox-footer'>
                </div>
            </div>
        </div>
    </div>
</div>
