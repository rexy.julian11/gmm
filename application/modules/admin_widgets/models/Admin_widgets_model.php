<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_widgets_model extends CI_Model
        {
            private $table = '';
            public function __construct()
            {
                parent::__construct();
            }
            public function get()
            {
             return $this->db->select('gw.id,w.name as widget,g.name as group,g.description,gw.active')
                             ->from('group_widget as gw')
                             ->join('widgets as w', 'w.id = gw.widget_id')
                             ->join('groups as g','g.id=gw.group_id')
                             ->get()
                             ->result();   
            }

            public function insert(){
                $group_id = $this->input->post('group_id');

                $this->db->trans_begin();

                $w['name'] = $this->input->post('name');
                $this->db->insert('widgets', $w);
                $widget_id = $this->db->insert_id();
                

                if($this->input->post('active') !== null && $this->input->post('active') == 1){
                    $active = 1;
                }
                else{
                    $active = 0;
                }
                foreach($group_id as $g){
                    $wg = array(
                        'widget_id' => $widget_id,
                        'active'    => $active,
                        'group_id'  =>$g
                    );
                    $this->db->insert('group_widget', $wg);
                }
                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return TRUE;
                }else{
                    $this->db->trans_rollback();
                    return FALSE;
                }
               
                
            }
            
        }
