<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_widgets extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_widgets_model');
                $this->breadcrumb->add('Admin widgets',base_url().'admin_widgets');
                $this->data['title'] = 'Admin widgets';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_widgets/js/datatables'); //load aditional js script
                $this->data['widget'] = $this->Admin_widgets_model->get();
                          
                $this->_render_page('admin_widgets/html/index', $this->data);
            }
            
            
            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                $this->breadcrumb->add('Add',base_url());
                
                //start your code here
                $this->form_validation->set_rules('name', 'group', 'trim|required');
                $this->form_validation->set_rules('group_id[]', 'Group', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->data['groups'] = $this->ion_auth->groups()->result_array();
                    $this->_render_page('html/form', $this->data);
                } else {
                    # code...
                    if($this->Admin_widgets_model->insert()){
                        $this->session->set_flashdata('success', 'Sukses menambahkan data');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Gagal menambahkan data');
                    }
                    redirect('admin/widgets');
                }
                

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            
        }
