<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pesanan_pembelian_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        $this->db->insert('purchase_orders', $data);
        return $this->db->insert_id();
    }
    public function insertItemDetail($data)
    {
        return $this->db->insert('purchase_order_item_details', $data);
    }
    public function insertServiceDetail($data)
    {
        return $this->db->insert('purchase_order_service_details', $data);
    }
    public function update($po_id, $data)
    {
        $this->db->where('po_id', $po_id);
        return $this->db->update('purchase_orders', $data);
    }
    public function deleteItemDetail($po_id)
    {
        $this->db->where('po_id', $po_id);
        return $this->db->delete('purchase_order_item_details');
    }
    public function deleteServiceDetail($po_id)
    {
        $this->db->where('po_id', $po_id);
        return $this->db->delete('purchase_order_service_details');
    }
    public function getPurchaseData($po_id)
    {
        $purchase = $this->db->select('po.*, CONCAT(supplier.first_name, " ", supplier.last_name) as supplier_name, supplier.phone, supplier.address')
                             ->join('users as supplier', 'supplier.id = po.supplier_id')
                             ->where('po.po_id', $po_id)
                             ->get('purchase_orders as po')->row();
        $items = $this->db->select('poi.*, i.item_name')
                          ->join('items as i', 'i.item_id = poi.item_id')
                          ->where('poi.po_id', $po_id)
                          ->get('purchase_order_item_details as poi')->result();
        $services = $this->db->select('pos.*, s.service_name')
                             ->join('services as s', 's.service_id = pos.service_id')
                             ->where('pos.po_id', $po_id)
                             ->get('purchase_order_service_details as pos')->result();
        $purchase->items = $items;
        $purchase->services = $services;

        return $purchase;
    }
    public function getPurchaseDetail($po_id)
    {
        return $this->db->select('po.*, items.item_name')
                        ->join('items', 'items.item_id = po.item_id')
                        ->where('po.po_id', $po_id)
                        ->get('purchase_order_details as po')->result();
    }
    public function getSupplier()
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as text')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 6)
                        ->get('users as u')->result();
    }
    public function getSupplierData($supplier_id)
    {
        return $this->db->select('u.id as id, concat(first_name, " ", last_name) as name, phone, address')
                        ->join('users_groups AS ug', 'ug.user_id = u.id')
                        ->where('ug.group_id', 6)
                        ->where('u.id', $supplier_id)
                        ->get('users as u')->row();
    }
    public function getItems() 
    {
        return $this->db->select('item_id as id, item_name as text')
                        ->get('items')->result();
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function getServices()
    {
        return $this->db->select('service_id as id, service_name as text')
                        ->get('services')->result();
    }
    public function getServiceData($service_id)
    {
        return $this->db->where('service_id', $service_id)
                        ->get('services')->row();
    }
    public function getPoDetail($po_id)
    {
        $data['items'] = $this->db->select('poi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = poi.item_id')
                                  ->where('poi.po_id', $po_id)
                                  ->get('purchase_order_item_details as poi')->result();
        $data['services'] = $this->db->select('pos.*, s.service_name')
                                     ->join('services as s', 's.service_id = pos.service_id')
                                     ->where('pos.po_id', $po_id)
                                     ->get('purchase_order_service_details as pos')->result();
        return $data;
    }
    public function getInvoice($po_id)
    {
        $invoice = $this->db->select('po.*, concat(supplier.first_name, " ", supplier.last_name) as supplier_name, supplier.address, supplier.phone')
                            ->join('users as supplier', 'supplier.id = po.supplier_id')
                            ->where('po.po_id', $po_id)
                            ->get('purchase_orders as po')->row();
        $invoice->items = $this->db->select('poi.*, i.item_name')
                                  ->join('items as i', 'i.item_id = poi.item_id')
                                  ->where('poi.po_id', $po_id)
                                  ->get('purchase_order_item_details as poi')->result();
        $invoice->services = $this->db->select('pos.*, s.service_name')
                                     ->join('services as s', 's.service_id = pos.service_id')
                                     ->where('pos.po_id', $po_id)
                                     ->get('purchase_order_service_details as pos')->result();
        return $invoice;
    }
    public function getAccounts()
    {
        return $this->db->select('account_number as id, account_name as text')
                        ->get('accounts')->result();
    }
    public function doPayment($data)
    {
        return $this->db->insert('payment', $data);
    }
}