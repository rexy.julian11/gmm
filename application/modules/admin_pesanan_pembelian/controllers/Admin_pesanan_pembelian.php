<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pesanan_pembelian extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_pesanan_pembelian_model');
        $this->breadcrumb->add('Admin pesanan pembelian',base_url().'admin_pesanan_pembelian');
        $this->data['title'] = 'Admin pesanan pembelian';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_pesanan_pembelian/js/datatables'); //load aditional js script
        $this->_render_page('admin_pesanan_pembelian/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('po.po_id as id,CONCAT(supplier.first_name, " ",supplier.last_name ) as supplier_name, po_no, po_date, po_bill, po_expired_date');
            $this->datatables->from('purchase_orders as po');
            $this->datatables->join('users as supplier', 'supplier.id = po.supplier_id');
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="pesanan_pembelian/edit/$1" title="edit">
                    <i class="fa fa-pencil">
                    </i>
                    <a class="btn btn-icon btn-sm btn-danger mr-1" href="pesanan_pembelian/delete/$1" title="delete">
                    <i class="fa fa-trash-o">
                    </i>',
                "id");
            $this->datatables->add_column(
                'invoice',
                '<span class="badge badge-primary">Detail</span>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->data['js_script'] = array('admin_pesanan_pembelian/js/form');
        $this->data['po_number'] = 'PRC' . time();
        $this->_render_page('admin_pesanan_pembelian/html/form', $this->data);
        

    }
    public function insert()
    {
        $data = $this->input->post(NULL);
        $insert['po_no']            = $data['po_number'];
        $insert['po_date']          = $data['po_date'];
        $insert['po_bill']          = $data['po_bill'];
        $insert['po_expired_date']  = $data['po_date_expired'];
        $insert['supplier_id']   = $data['supplier_id'];
        if ($po = $this->Admin_pesanan_pembelian_model->insert($insert)) {
            foreach ($data['data_barang'] as $row) {
                $insertItem['po_id']        = $po;
                $insertItem['item_id']      = $row['item_id'];
                $insertItem['item_price']   = $row['item_price'];
                $insertItem['item_qty']     = $row['item_qty'];
                $insertItem['item_total']   = $row['item_total'];
                $this->Admin_pesanan_pembelian_model->insertItemDetail($insertItem);
            }
            foreach ($data['data_jasa'] as $row) {
                $insertService['po_id']        = $po;
                $insertService['service_id']   = $row['service_id'];
                $insertService['pos_price']    = $row['pos_price'];
                $insertService['pos_unit']     = $row['pos_unit'];
                $insertService['pos_qty']      = $row['pos_qty'];
                $insertService['pos_duration'] = $row['pos_duration'];
                $insertService['pos_total']    = $row['pos_total'];
                $this->Admin_pesanan_pembelian_model->insertServiceDetail($insertService);
            }
            echo 'true';
        }else{
            echo 'false';
        }
        
    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->data['js_script'] = array('admin_pesanan_pembelian/js/edit');
        $this->data['po'] = $this->Admin_pesanan_pembelian_model->getPurchaseData($id);
        $this->data['id'] = $id;
        $this->_render_page('admin_pesanan_pembelian/html/edit', $this->data);

        //start your code here
    }
    public function update($id)
    {
        $data = $this->input->post(NULL);
        $update['po_no']            = $data['po_number'];
        $update['po_date']          = $data['po_date'];
        $update['po_bill']          = $data['po_bill'];
        $update['po_expired_date']  = $data['po_date_expired'];
        $update['supplier_id']      = $data['supplier_id'];
        if ($po = $this->Admin_pesanan_pembelian_model->update($id, $update)) {
            $this->Admin_pesanan_pembelian_model->deleteItemDetail($id);
            $this->Admin_pesanan_pembelian_model->deleteServiceDetail($id);
            foreach ($data['data_barang'] as $row) {
                $updateItem['po_id']        = $id;
                $updateItem['item_id']      = $row['item_id'];
                $updateItem['item_price']   = $row['item_price'];
                $updateItem['item_qty']     = $row['item_qty'];
                $updateItem['item_total']   = $row['item_total'];
                $this->Admin_pesanan_pembelian_model->insertItemDetail($updateItem);
            }
            foreach ($data['data_jasa'] as $row) {
                $updateService['po_id']        = $id;
                $updateService['service_id']   = $row['service_id'];
                $updateService['pos_price']    = $row['pos_price'];
                $updateService['pos_unit']     = $row['pos_unit'];
                $updateService['pos_qty']      = $row['pos_qty'];
                $updateService['pos_duration'] = $row['pos_duration'];
                $updateService['pos_total']    = $row['pos_total'];
                $this->Admin_pesanan_pembelian_model->insertServiceDetail($updateService);
            }
            echo 'true';
        }else{
            echo 'false';
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    public function doPayment()
    {
        $data = $this->input->post(null);
        $this->Admin_pesanan_pembelian_model->doPayment($data);
    }
    public function getPurchaseDetail()
    {   
        $po_id = $this->input->post('po_id');
        $detail = $this->Admin_pesanan_pembelian_model->getPurchaseDetail($po_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($detail));
    }

    public function getSupplier()
    {
        $supplier = $this->Admin_pesanan_pembelian_model->getSupplier();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($supplier));
    }
    public function getSupplierData($supplier_id)
    {
        $supplier = $this->Admin_pesanan_pembelian_model->getSupplierData($supplier_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($supplier));
    }
    public function getItems()
    {
        $items = $this->Admin_pesanan_pembelian_model->getItems();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($items));
    }
    public function getItemData($item_id)
    {
        $item = $this->Admin_pesanan_pembelian_model->getItemData($item_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($item));
    }
    public function getServices()
    {
        $services = $this->Admin_pesanan_pembelian_model->getServices();
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($services));
    }
    public function getServiceData($service_id)
    {
        $service = $this->Admin_pesanan_pembelian_model->getServiceData($service_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($service));
    }
    public function getPoDetail($po_id)
    {
        $detail = $this->Admin_pesanan_pembelian_model->getPoDetail($po_id);
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($detail));
    }
    public function getInvoice($po_id)
    {
        $invoice = $this->Admin_pesanan_pembelian_model->getInvoice($po_id);    
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($invoice));
    }
    public function getAccounts()
    {
        $accounts = $this->Admin_pesanan_pembelian_model->getAccounts();    
        $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($accounts));
    }
}