<style>
    .center{
        width: 150px;
        margin: 40px auto;   
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Pesanan Pembelian
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">No PO *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="po_number" value="<?= isset($po) ? $po->po_no : ''?>" readonly>
                                        <?php echo form_error('po_number', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>               
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal PO *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="po_date" value="<?= isset($po) ? date("Y-m-d", strtotime($po->po_date)) : ''?>">
                                        <?php echo form_error('po_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Total Tagihan *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="po_bill" value="<?= isset($po) ? $po->po_bill : ''?>">
                                        <?php echo form_error('po_bill', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Jatuh Tempo *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="po_date_expired" value="<?= isset($po) ? date("Y-m-d", strtotime($po->po_expired_date)) : ''?>">
                                        <?php echo form_error('po_date_expired', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Supplier *</label>
                                    <div class="col-sm-8">
                                        <select name="" id="" class="form-control selectSupplier">
                                        </select>
                                    </div>
                                </div>
                                <div class="alert alert-dismissible alert-secondary">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($po) ? $po->supplier_name : ''?>" id="inputNama" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Telepon </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($po) ? $po->phone : ''?>" id="inputTelp" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Alamat </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?= isset($po) ? $po->address : ''?>" id="inputAlamat" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Barang *</label>
                                <div class="col-sm-4">
                                    <select name="" id="" class="form-control selectBarang">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary btnAddBarang"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col" style="display:none;">id</th>
                                        <th scope="col">-</th>
                                        <th scope="col">Barang</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" class="pull-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblBarang">
                                    <?php if (isset($po->items)) {
                                        $total_item = 0;
                                        foreach ($po->items as $row) {
                                        $total_item = $total_item + $row->item_total;
                                        ?>
                                        <tr>
                                            <td style="display:none"><?=$row->item_id?></td>
                                            <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                            <td><?=$row->item_name?></td>
                                            <td class="tblPrice"><?=$row->item_price?></td>
                                            <td><div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btnMinus" <?= isset($row->item_qty) && $row->item_qty <= 1 ? 'disabled="disabled"' : ''?>>
                                                            <span class="glyphicon glyphicon-minus"></span>
                                                        </button>
                                                    </span>
                                                    <input type="text" name="" class="form-control input-number qty" value="<?= $row->item_qty?>" min="1">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default btnPlus">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </button>
                                                    </span>
                                                </div></td>
                                            <td class="tblTotal pull-right"><?=$row->item_total?></td>
                                        </tr>
                                    <?php    
                                        }
                                     }?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"><?= $total_item?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jasa *</label>
                                <div class="col-sm-4">
                                    <select name="" id="" class="form-control selectJasa">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary btnAddJasa"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col" style="display:none;">id</th>
                                        <th scope="col">-</th>
                                        <th scope="col">Service</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Satuan</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" style="width:150px">Durasi</th>
                                        <th scope="col" class="pull-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblJasa">
                                    <?php if (isset($po->services)) {
                                        $total_service = 0;
                                        foreach ($po->services as $row) {
                                        $total_service = $total_service + $row->pos_total;
                                    ?>
                                    <tr>
                                        <td style="display:none"><?=$row->service_id?></td>
                                        <td><button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                        <td><?=$row->service_name?></td>
                                        <td class="tblPrice"><?=$row->pos_price?></td>
                                        <td><?=$row->pos_unit?></td>
                                        <td><div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnMinus" <?= isset($row->pos_qty) && $row->pos_qty <= 1 ? 'disabled="disabled"' : ''?>>
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="" class="form-control input-number qty" value="<?= $row->pos_qty?>" min="1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnPlus">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnMinus" <?= isset($row->pos_duration) && $row->pos_duration <= 1 ? 'disabled="disabled"' : ''?>>
                                                        <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="" class="form-control input-number durasi" value="<?= $row->pos_duration?>" min="1">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default btnPlus">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="tblTotal pull-right"><?=$row->pos_total?></td>
                                    </tr>
                                    <?php    
                                        }
                                     }?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"><?=$total_service?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Grand Total </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right inputGrandTotal" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Terbilang </label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control text-right terbilang" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary pull-right btnSubmit">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>