<style>
    td.details-control {
        background: url('<?= base_url('assets/themes/backend/inspina/img/details_open.png')?>') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?= base_url('assets/themes/backend/inspina/img/details_close.png')?>') no-repeat center center;
    }
    td.po_no {
        cursor: pointer;
    }
    .select-in-modal+.select2-container {
        width: 100% !important;
        padding: 0;
        z-index:10000;
    }

    .select2-container--open {
        z-index:10000;
    }
    span.select2-container {
        z-index:10050;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                Pesanan Pembelian
            </header>
            <div class="card-body">
                <div class="pull-right" style="margin-bottom:7px;">
                    <a href="<?php echo site_url('admin/pesanan-pembelian/add')?>" class="btn btn-primary">Add New <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover datatables">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nomor</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Tagihan</th>
                                <th scope="col">Tanggal Jatuh Tempo</th>
                                <th scope="col">Nama Pemasok</th>
                                <th scope="col">Action</th>
                                <th scope="col">Invoice</th>
                            </tr> 
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="table-responsive table-hidden">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Item Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody class="tblDetail">

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <div class="modal fade" id="modalDetail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Invoice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="ibox-content p-xl">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h5>From:</h5>
                                                <address>
                                                    <strong>PT Gemilang Makmur Mandiri</strong><br>
                                                    Nama Jalan<br>
                                                    Detail Jalan<br>
                                                    <abbr title="Phone">P:</abbr> (123) 601-4590
                                                </address>
                                            </div>

                                            <div class="col-sm-6 text-right">
                                                <h4>Invoice No.</h4>
                                                <h4 class="text-navy" id="modalInvoice"></h4>
                                                <span>To:</span>
                                                <address>
                                                    <strong id="modalSupplier"></strong><br>
                                                    <span id="modalAddress"></span><br>
                                                    <abbr title="Phone" id="modalPhone">P:</abbr>
                                                </address>
                                                <p>
                                                    <span id="modalDate"></span><br/>
                                                    <span id="modalDateExpired"><strong>Tanggal Jatuh Tempo:</strong> March 24, 2014</span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="table-responsive m-t">
                                            <table class="table invoice-table">
                                                <thead>
                                                    <tr>
                                                        <th>Barang</th>
                                                        <th>Harga</th>
                                                        <th>Kuantitas</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="modalTblItem">
                                                </tbody>
                                            </table>
                                        </div>
                                        <table class="table invoice-total">
                                            <tbody>
                                                <tr>
                                                    <td><strong>Sub Total :</strong></td>
                                                    <td id="modalSubItem"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="table-responsive m-t">
                                            <table class="table invoice-table">
                                                <thead>
                                                    <tr>
                                                        <th>Jasa</th>
                                                        <th>Harga</th>
                                                        <th>Satuan</th>
                                                        <th>Kuantitas</th>
                                                        <th>Durasi</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="modalTblService">
                                                </tbody>
                                            </table>
                                        </div>

                                        <table class="table invoice-total">
                                            <tbody>
                                                
                                                <tr>
                                                    <td>
                                                        <select name="" class="form-control" id="selectMethod">
                                                            <option selected disabled>Pilih Metode Pembayaran</option>
                                                            <option value="Tunai">Tunai</option>
                                                            <option value="Debit">Debit</option>
                                                            <option value="Giro">Giro</option>
                                                        </select>
                                                    </td>
                                                    <td><strong class="pull-right">Sub Total :</strong></td>
                                                    <td id="modalSubService"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select name="" class="form-control" id="selectAccount">
                                                        </select>
                                                    </td> 
                                                    <td><strong class="pull-right">Grand Total :</strong></td>
                                                    <td id="modalTotal"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="text-right">
                                            <button class="btn btn-primary" id="btnBayar" type="button"><i class="fa fa-dollar"></i> Bayar</button>
                                        </div>

                                        <!-- <div class="well m-t"><strong>Comments</strong>
                                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less
                                        </div> -->
                                    </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                </div>
                </div>
            </div>
            </div>
    </div>
</div>
