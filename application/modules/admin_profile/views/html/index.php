<?php 
// $avatar = ($mydata->avatar != '') ? $mydata->avatar : site_url('uploads/avatar.png');

?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Profil Anda</h5>
                </div>
                <div>
                    <div class="ibox-content no-padding border-left-right text-center">
                        <img alt="image" class="rounded-circle img img-responsive mt-3" style="max-width: 70%"
                            src="<?php echo $avatar; ?>">
                    </div>
                    <div class="ibox-content profile-content">
                        <h4><strong><?php echo  $mydata->first_name." ".$mydata->last_name; ?></strong></h4>
                        <p>

                            <?php echo  isset($mygroup->name) ? '<i class="fa fa-users"></i> '.$mygroup->name .'<br>': ""; ?>
                            <?php echo  isset($mydata->company) ? '<i class="fa fa-building-o"></i> '.$mydata->company .'<br>': ""; ?>
                            <?php echo  isset($mydata->phone) ? '<i class="fa fa-phone"></i> '.$mydata->phone .'<br>': ""; ?>
                            <?php echo  isset($mydata->email) ? '<i class="fa fa-envelope"></i> '.$mydata->email.'<br>' : ""; ?>
                            <br>
                        </p>

                        <div class="user-button">
                            <div class="row">
                                <?php if(isset($mydata->userPhone)):?>
                                <div class="col-md-6">
                                    <a href="tel:<?php echo  $mydata->userPhone; ?>"
                                        class="btn btn-default btn-sm btn-block"><i class="fa fa-phone"></i>
                                        Telepon</a>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Update your Profile</h5>
                </div>
                <div>
                    <?php echo  
                    $this->session->flashdata('upload_error');
                    ?>
                    <?php echo  form_open_multipart(); ?>
                    <div class="ibox-content">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-2">
                                    <img alt="image" class="rounded-circle mt-3" style="max-width: 60px"
                                        src="<?php echo  $avatar; ?>">
                                </div>
                                <div class="col-10">
                                    <input type="hidden" name="iduser" value="<?php echo  $mydata->id; ?>">
                                    <label for="avatar">Foto</label>
                                    <div class="custom-file">
                                        <input id="avatar" name="avatar" type="file" class="custom-file-input">
                                        <label for="avatar" class="custom-file-label">Choose file...</label>
                                    </div>
                                    <?php if($this->session->flashdata('avatar_error')){
                                        echo $this->session->flashdata('avatar_error');
                                    } ?>
                                    <?php echo  form_error('avatar', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="first_name">First Name <span class="text-danger">*</span></label>
                                    <input name="first_name" id="" class="form-control"
                                        value="<?php echo  isset($mydata->first_name) ? $mydata->first_name : set_value("first_name"); ?>"
                                        autofocus>
                                    <?php echo  form_error('first_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="last_name">last Name</label>
                                    <input name="last_name" id="" class="form-control"
                                        value="<?php echo  isset($mydata->last_name) ? $mydata->last_name : set_value("last_name"); ?>"
                                        autofocus>
                                    <?php echo  form_error('last_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="text-danger">*</span></label>
                            <input name="email" id="" class="form-control"
                                value="<?php echo  isset($mydata->email) ? $mydata->email : set_value("email"); ?>">
                            <?php echo  form_error('email', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input name="phone" id="" class="form-control"
                                value="<?php echo  isset($mydata->phone) ? $mydata->phone : set_value("phone"); ?>">
                            <?php echo  form_error('phone', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                        </div>

                        <hr>

                        <div class="gray-bg wrapper p-2">
                            <div class="form-group">
                                <label for="userPasswordExisting">Password<span class="text-danger">*</span></label>
                                <input type="password" name="userPasswordExisting" id="" class="form-control"
                                    placeholder="Password">
                                <span class="text-info"> * Make sure this is you by typing your current password</span>
                                <br>
                                <?php echo  form_error('userPasswordExisting', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-footer">
                        <?php echo btn_back(); ?>
                        <?php echo btn_submit('Save'); ?>
                    </div>
                    <?php echo  form_close(); ?>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Update Password</h5>
                </div>
                <div>
                    <?php echo  
                    $this->session->flashdata('upload_error');
                    ?>
                    <?php echo  form_open('admin_profile/update_password'); ?>
                    <div class="ibox-content">


                        <div class="form-group">
                            <label for="new_password">New Password</label>
                            <input type="password" name="new_password" id="" class="form-control"
                                placeholder="Password">
                            <?php echo $this->session->flashdata('error_new_password') ; ?>
                        </div>
                        <div class="form-group">
                            <label for="new_password_confirm">Confirm New Password</label>
                            <input type="password" name="new_password_confirm" id="" class="form-control"
                                placeholder="Password">
                            <?php echo $this->session->flashdata('error_new_confirm') ; ?>

                        </div>
                        <hr>
                        <div class="gray-bg wrapper p-2">
                            <div class="form-group">
                                <label for="current_password">Current Password<span class="text-danger">*</span></label>
                                <input type="password" name="current_password" id="" class="form-control"
                                    placeholder="Password">
                                <?php echo $this->session->flashdata('error_current_password') ; ?>

                            </div>
                        </div>
                    </div>
                    <div class="ibox-footer">
                        <?php echo btn_back(); ?>
                        <?php echo btn_submit('Change Password'); ?>
                    </div>
                    <?php echo  form_close(); ?>
                </div>
            </div>

        </div>
    </div>

</div>
