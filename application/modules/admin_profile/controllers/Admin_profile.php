<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_profile extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Profile_model');
                $this->breadcrumb->add('Profile',base_url().'profile');
                $this->data['title'] = 'Profile';
            }
            public function index()
            {
            

                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                
                $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('userPasswordExisting', 'Password', 'trim|required|callback_check_password');
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->_render_page('html/index', $this->data);

                } else {
                    $data = $this->security->xss_clean($this->input->post());


                    if(password_verify($data['userPasswordExisting'], $this->data['mydata']->password)){
                        if($_FILES['avatar']['name'] !== ''){
                            $config['upload_path']          = 'uploads/employees/';
                            $config['allowed_types']        = 'gif|jpg|png';
                            $config['max_size']             = 100;
                            $config['max_width']            = 1024;
                            $config['max_height']           = 1024;

                            $this->load->library('upload', $config);
                            $this->upload->display_errors('<span class="text-danger">', '</span>');

                            if ( ! $this->upload->do_upload('avatar'))
                            {
                                   $this->session->set_flashdata('avatar_error', $this->upload->display_errors());
                                   redirect('admin/profile');
                            }
                            else
                            {
                                $data_upload = $this->upload->data();
                                    $data['avatar'] = $data_upload['file_name'];
                            }
                        }
                        if($this->ion_auth->update_user($data['id'], $data)){
                            $messages = $messages = $this->ion_auth->messages();
                            $this->session->set_flashdata('success_edit', $messages);
                            
                        }
                        else{
                            $messages = $messages = $this->ion_auth->messages();
                            $this->session->set_flashdata('failed', $messages);
                            
                        }
                    }
                    else{
                            $this->session->set_flashdata('failed', 'You Enter Wrong Password');
                        
                    }
                    redirect('admin/profile');
                    # code...
                }

            }
                    
            public function update_password()
            {
                if ($this->input->server('REQUEST_METHOD') != 'POST'){
                    redirect('admin/profile');
                }
                
                $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
                $this->form_validation->set_rules('new_password_confirm', 'New Password Confirm', 'trim|required|matches[new_password]');
                $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|callback_check_password');
                
                
                if ($this->form_validation->run() == FALSE) {
                    # code...
                    $this->session->set_flashdata('error_new_password', form_error('new_password','<span class="text-danger">','<span>'));
                    $this->session->set_flashdata('error_new_confirm', form_error('new_password_confirm','<span class="text-danger">','<span>'));
                    $this->session->set_flashdata('error_current_password', form_error('current_password','<span class="text-danger">','<span>'));
                   
                    redirect('admin/profile');
                } else {
                    $post = $this->security->xss_clean($this->input->post());

                    if(password_verify($post['current_password'], $this->data['mydata']->password)){

                        $data['password'] = $post['new_password'];
                        if($this->ion_auth->update_user($this->data['mydata']->id, $data)){
                            $messages = $messages = $this->ion_auth->messages();
                            $this->session->set_flashdata('success_edit', $messages);
                            
                        }
                        else{
                            $messages = $messages = $this->ion_auth->messages();
                            $this->session->set_flashdata('failed', $messages);
                            
                        }
                    }
                    else{
                            $this->session->set_flashdata('failed', 'You Enter Wrong Password');
                        
                    }
                    redirect('admin/profile');
                    # code...
                }
                
                
            }

             public function check_password($str){
                if(! password_verify($str, $this->data['mydata']->password)){
                        $this->form_validation->set_message('check_password', 'Your Current Password is incorrect');
                        return FALSE;                    
                }
                else{
                    return TRUE;
                }
            }
            
        }
