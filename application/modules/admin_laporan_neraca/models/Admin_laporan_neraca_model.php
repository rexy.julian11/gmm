<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_laporan_neraca_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    
    public function neraca($start_date, $end_date)
    {
        $accounts =  $this->db->select('a.*, p.account_name as parent_name')
                              ->join('accounts as p', 'p.account_number = a.parent_number')
                              ->order_by('a.account_number', 'ASC')
                              ->where_in('a.group_number', [1,2,3])
                              ->get('accounts as a')->result();
                              
        foreach ($accounts as $key => $value) {
            $balance = $value->account_initial_balance;
            $normal_balance = $value->account_normal_balance;
            $addbalance = $this->db->select('l.ledger_amount, l.ledger_increase_to')
                                   ->join('journals as j', 'j.journal_id = l.journal_id')
                                   ->where('l.account_number', $value->account_number)
                                   ->where('j.journal_datetime >=', $start_date)
                                   ->where('j.journal_datetime <=', $end_date)
                                   ->get('ledgers as l')->result();
            if (!empty($addbalance)) {
                foreach ($addbalance as $keyy => $row) {
                    if ($normal_balance == 'Db') {
                        if ($row->ledger_increase_to == 'Db') {
                            $balance = (float)$balance + (float)$row->ledger_amount;
                        }
                        else{
                            $balance = (float)$balance - (float)$row->ledger_amount;
                        }
                    }
                    else{
                        if ($row->ledger_increase_to == 'Cr') {
                            $balance = (float)$balance + (float)$row->ledger_amount;
                        }
                        else{
                            $balance = (float)$balance - (float)$row->ledger_amount;
                        }
                    }
                }
            }
            $accounts[$key]->balance = $balance;
        }

        return $accounts;
    }
}