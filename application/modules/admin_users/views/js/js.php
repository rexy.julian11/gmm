<script>
$(document).ready(function() {
    $("#passwordGenerator").on('click', function() {
        let password = generatePassword()
        $("#password").val(password);
        $("#password_confirm").val(password);
        toggleViewPassword('password');
        toggleViewPassword('password_confirm');
    });

    $("#viewPassword").on('click', function() {
        toggleViewPassword('password');
        toggleViewPassword('password_confirm');
    });

    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
});

function toggleViewPassword(selector) {
    var x = document.getElementById(selector);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
</script>
