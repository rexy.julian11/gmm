<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><?php echo lang('deactivate_heading');?></h5><br>
                    <span><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></span>
                </div>
                <?php echo form_open(); ?>
                <div class="ibox-content">
                    <?php echo form_hidden(['id' => $user->id]); ?>


                    <div class="form-group">
                        <input type="radio" name="confirm" value="yes" checked="checked" />
                        <?php echo lang('deactivate_confirm_y_label', 'confirm');?> <br>
                        <input type="radio" name="confirm" value="no" />
                        <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
                    </div>



                </div>
                <div class="ibox-footer">
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit($this->lang->line('deactivate_submit_btn')); ?>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
