<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><?php echo $title; ?></h5><br>
                    <span><?php echo $this->lang->line('create_user_subheading');?></span>
                </div>
                <?php echo form_open(); ?>
                <div class="ibox-content">

                    <div class="form-group">
                        <label for="first_name"><?php echo $this->lang->line('create_user_fname_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input placeholder="<?php echo $this->lang->line('create_user_fname_label'); ?>"
                            name="first_name" id="" class="form-control" autofocus
                            value="<?php echo isset($user)? $user->first_name : set_value("first_name"); ?>">
                        <?php echo form_error('first_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <div class="form-group">
                        <label for="last_name"><?php echo $this->lang->line('create_user_lname_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input placeholder="<?php echo $this->lang->line('create_user_lname_label'); ?>"
                            name="last_name" id="" class="form-control"
                            value="<?php echo isset($user)? $user->last_name : set_value("last_name"); ?>">
                        <?php echo form_error('last_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <?php if($identity_column!=='email'): ?>
                    <div class="form-group">
                        <label for="identity"><?php echo $this->lang->line('create_user_identity_label'); ?> </label>
                        <input placeholder="<?php echo $this->lang->line('create_user_identity_label'); ?>"
                            name="identity" id="" class="form-control"
                            value="<?php echo isset($user)? $user->username : set_value("identity"); ?>">
                        <?php echo form_error('identity', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <?php endif; ?>

                    <div class="form-group">
                        <label for="company"><?php echo $this->lang->line('create_user_company_label');?> </label>
                        <input name="company" placeholder="<?php echo $this->lang->line('create_user_company_label');?>"
                            id="" class="form-control"
                            value="<?php echo isset($user)? $user->company : set_value("company"); ?>">
                        <?php echo form_error('company', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <div class="form-group">
                        <label for="email"><?php echo $this->lang->line('create_user_email_label'); ?> <span
                                class="text-danger">*</span></label>
                        <input placeholder="<?php echo $this->lang->line('create_user_email_label');?>" name="email"
                            id="" class="form-control"
                            value="<?php echo isset($user)? $user->email :set_value("email"); ?>">
                        <?php echo form_error('email', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>

                    <div class="form-group">
                        <label for="phone"> <?php echo $this->lang->line('create_user_phone_label'); ?> </label>
                        <input placeholder="<?php echo $this->lang->line('create_user_phone_label');?>" name="phone"
                            id="" class="form-control"
                            value="<?php echo isset($user)? $user->phone : set_value("phone"); ?>">
                        <?php echo form_error('phone', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group">
                        <label for="password">
                            <?php echo isset($user) ? $this->lang->line('edit_user_password_label'): $this->lang->line('create_user_password_label').' <span class="text-danger">*</span>' ;?></label>
                        <div class="input-group">
                            <input placeholder="<?php echo $this->lang->line('create_user_password_label');?>"
                                type="password" name="password" id="password" class="form-control" value="">
                            <div class="input-group-append">
                                <span class="input-group-text btn" title="show" id="viewPassword"><i
                                        class="fa fa-eye"></i></span>
                                <span class="input-group-text btn btn-primary" title='generate'
                                    id="passwordGenerator"><i class="fa fa-cog"></i></span>
                            </div>
                            <?= form_error('password', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password_confirm">
                            <?php echo isset($user) ? $this->lang->line('edit_user_password_confirm_label'): $this->lang->line('create_user_password_confirm_label').' <span class="text-danger">*</span>' ;?></label>
                        <input placeholder="<?php echo $this->lang->line('create_user_password_confirm_label');?>"
                            type="password" name="password_confirm" id="password_confirm" class="form-control" value="">
                        <?php echo form_error('password_confirm', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                    </div>
                    <div class="form-group ">

                        <span><?php echo $this->lang->line('edit_user_groups_heading');?></span><br>
                        <?php foreach ($groups as $group):?>
                        <?php
                                                    
                                                        $gID=$group['id'];
                                                        $checked = null;
                                                        $item = null;
                                                        if(isset($currentGroups)){
                                                        foreach($currentGroups as $grp) {
                                                            if ($gID == $grp->id) {
                                                                $checked= ' checked="checked"';
                                                            break;
                                                            }
                                                        }
                                                        }

                                                        ?>

                        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"
                            <?php echo set_checkbox('groups[]', $group['id']) ?> <?php echo $checked; ?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?><br>
                        <?php endforeach?>



                    </div>
                </div>
                <div class="ibox-footer">
                    <?php echo btn_back(); ?>
                    <?php echo btn_submit(isset($user) ? $this->lang->line('edit_user_submit_btn') : $this->lang->line('create_user_submit_btn')); ?>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
