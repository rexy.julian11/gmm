<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <?= btn_add('admin/users/create_user', lang('index_create_user_link'),lang('index_create_user_link'));?>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped datatable">
                            <thead>
                                <tr>
                                    <th><?php echo lang('index_fname_th');?></th>
                                    <th><?php echo lang('index_lname_th');?></th>
                                    <th><?php echo lang('index_email_th');?></th>
                                    <th><?php echo lang('index_groups_th');?></th>
                                    <th><?php echo lang('index_status_th');?></th>
                                    <th><?php echo lang('index_action_th');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user):?>
                                <tr>
                                    <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                                    <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                    <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                                    <td>
                                        <?php foreach ($user->groups as $group):?>
                                        <?php echo anchor("admin_users/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                                        <?php endforeach?>
                                    </td>
                                    <td><?php echo ($user->active) ? anchor("admin_users/deactivate/".$user->id, lang('index_active_link')) : anchor("admin_users/activate/". $user->id, lang('index_inactive_link'));?>
                                    </td>
                                    <td>
                                        <?= btn_edit('admin/users/edit_user/', $user->id); ?>
                                        <?= btn_delete('admin/users/delete_user/', $user->id); ?>
                                    </td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>

<script>
$(document).ready(function() {
    $("#datatable").DataTable(), $("#datatable-buttons").DataTable({
        lengthChange: !1,
        buttons: ["copy", "excel", "pdf", "colvis"]
    }).buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)")
});
</script>
