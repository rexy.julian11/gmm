<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_transaction extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_transaction_model');
        $this->breadcrumb->add('Admin transaction',base_url().'admin_transaction');
        $this->data['title'] = 'Admin transaction';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_transaction/js/datatables'); //load aditional js script
        $this->_render_page('admin_transaction/html/index', $this->data);
    }
    public function sub($id)
    {
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_transaction/js/datatables'); //load aditional js script
        $this->data['setting'] = $this->Admin_transaction_model->getSetting($id);
        $this->data['transactions'] = $this->Admin_transaction_model->getTransactions($id);
        $this->_render_page('admin_transaction/html/index', $this->data);
    }
    
    /**
     *Data Services 
    **/
    public function datatables()
    {
        
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        $this->data['js_script'] = array('admin_transaction/js/form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('transaction_amount', 'Amount', 'required|numeric');
        $this->form_validation->set_rules('transaction_date', 'Date', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['setting'] = $this->Admin_transaction_model->getSetting($id);
            $first  = [];
            $second = [];
            foreach ($this->data['setting']->from as $key => $row) {
                array_push($first, $row->tsd_acc_id);
            }
            foreach ($this->data['setting']->to as $key => $row) {
                array_push($second, $row->tsd_acc_id);
            }
            $this->data['first'] = $this->Admin_transaction_model->getFirstAccount($first);
            $this->data['second'] = $this->Admin_transaction_model->getSecondAccount($second);
            $this->_render_page('admin_transaction/html/form', $this->data);
        }
        else
        {

            $data = $this->input->post();
            $data['transaction_id']   = 'TRF'.time().$id;
            $data['transaction_type'] = $id;
            $data['user_id']          = $this->ion_auth->get_user_id();
            if ($this->Admin_transaction_model->insert($data)) {
                $dataJournal['journal_description'] = 'Transfer '. $data['transaction_id'];
                $dataJournal['journal_ref_pk']      = $data['transaction_id'];
                if ($journal = $this->Admin_transaction_model->insertJournal($dataJournal)) {
                    for ($i=0; $i < 2; $i++) { 
                        $dataLedger['journal_id']     = $journal;
                        if ($i == 0 ) {
                            $account = $this->Admin_transaction_model->getAccount($data['transaction_cr']);
                            if ($account->account_normal_balance == 'Db') {
                                $dataLedger['account_number']     =  $data['transaction_cr'];
                                $dataLedger['ledger_increase_to'] =  'Cr';
                                $dataLedger['ledger_amount']      =  $data['transaction_amount'];
                            }
                            else{
                                $dataLedger['account_number']     =  $data['transaction_cr'];
                                $dataLedger['ledger_increase_to'] =  'Db';
                                $dataLedger['ledger_amount']      =  $data['transaction_amount'];
                            }
                        }
                        else{
                            $account = $this->Admin_transaction_model->getAccount($data['transaction_cr']);
                            if ($account->account_normal_balance == 'Db') {
                                $dataLedger['account_number']     =  $data['transaction_db'];
                                $dataLedger['ledger_increase_to'] =  'Db';
                                $dataLedger['ledger_amount']      =  $data['transaction_amount'];
                            }
                            else{
                                $dataLedger['account_number']     =  $data['transaction_db'];
                                $dataLedger['ledger_increase_to'] =  'Cr';
                                $dataLedger['ledger_amount']      =  $data['transaction_amount'];
                            }
                        }
                        $this->Admin_transaction_model->insertLedger($dataLedger);
                    }
                    $this->session->set_flashdata('success', 'Successfull create transaction');
                    redirect('admin/transaction/sub/'.$id, 'refresh');
                }
                else{
                    $this->session->set_flashdata('failed', 'Check your data and try again');
                    redirect('admin/transaction/form', 'refresh');
                }
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/transaction/form', 'refresh');
            }
        
        }

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        //start your code here
    }
    
}