<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <?php echo $setting->ts_name?>    
                </div>
                <div class="card-body">
                    <?php echo form_open()?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Amount *</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="transaction_amount" value="">
                                <?php echo form_error('transaction_amount', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date *</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" name="transaction_date" value="">
                                <?php echo form_error('transaction_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Cash/Bank</label>
                            <div class="col-sm-10">
                                <select name="transaction_cr" class="form-control select2">
                                    <option value="" selected disabled> Select Account</option>
                                    <?php foreach ($first as $row) { ?>
                                        <option value="<?php echo $row->account_number?>"><?php echo $row->account_name?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Revenues</label>
                            <div class="col-sm-10">
                                <select name="transaction_db" class="form-control select2" id="">
                                    <option value="" selected disabled> Select Account</option>
                                    <?php foreach ($second as $row) { ?>
                                        <option value="<?php echo $row->account_number?>"><?php echo $row->account_name?></option>
                                    <?php }?>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button class="btn btn-primary pull-right" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>

