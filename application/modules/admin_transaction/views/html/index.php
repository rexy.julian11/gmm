<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Transaction <?php echo $setting->ts_name?>    
                </div>
            </div>
            <div class="card-header">
                <div class="pull-right" style="margin-bottom : 5px">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/transaction/add/').$setting->ts_id?>">Add Transaction <i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr class="table-secondary">
                                <td>Date</td>
                                <td>Amount</td>
                                <td>By</td>
                                <td>Transaction From</td>
                                <td>Transaction To</td>
                                <td>Type</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transactions as $row) { ?>
                                <tr>
                                    <td><?php echo $row->transaction_date?></td>
                                    <td><?php echo $row->transaction_amount?></td>
                                    <td><?php echo $row->name?></td>
                                    <td><?php echo $row->account_from?></td>
                                    <td><?php echo $row->account_to?></td>
                                    <td><?php echo $row->ts_name?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


