<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_transaction_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getFirstAccount($first)
    {
        $this->db->where('account_type', 'Account')
                 ->where_in('account_number', $first);
        return $this->db->get('accounts')->result();
    }
    public function getSecondAccount($second)
    {
        $this->db->where('account_type', 'Account')
                 ->where_in('account_number', $second);
        return $this->db->get('accounts')->result();
    }
    public function getTransactions($id)
    {
        return $this->db->select('transactions.*, from.account_name as account_from, to.account_name as account_to, transaction_setting.ts_name, concat(users.first_name, " ", users.last_name) as name')
                        ->join('users', 'users.id = transactions.user_id')
                        ->join('transaction_setting', 'transaction_setting.ts_id = transactions.transaction_type')
                        ->join('accounts as from', 'from.account_number = transactions.transaction_db')
                        ->join('accounts as to', 'to.account_number = transactions.transaction_cr')
                        ->where('transactions.transaction_type', $id)
                        ->get('transactions')->result();
    }
    public function getSetting($id)
    {
        $ts = $this->db->where('ts_id', $id)
                       ->get('transaction_setting')->row();
        $tsd_from = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                    ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                    ->where('tsd_parent_id', $ts->ts_from)
                    ->get('transaction_setting_detail')->result();
        $tsd_to   = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                    ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                    ->where('tsd_parent_id', $ts->ts_to)
                    ->get('transaction_setting_detail')->result();
        $ts->from = $tsd_from;
        $ts->to   = $tsd_to;

        return $ts;
    }
    public function insert($data)
    {
        return $this->db->insert('transactions', $data);
    }
    public function getAccount($account_number)
    {
        return $this->db->where('account_number', $account_number)
                        ->get('accounts')->row();
    }
    public function insertJournal($data)
    {
        $this->db->insert('journals', $data);
        return $this->db->insert_id();
    }
    public function insertLedger($data)
    {
        return $this->db->insert('ledgers', $data);
    }
}