<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $title; ?></title>

    <link href="<?php echo theme_assets('inspina') ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo theme_assets('inspina') ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo theme_assets('inspina') ?>css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div class="">
                <h2 class="">GMM</h2>
            </div>
            <h3><?= $welcome; ?> to Gemilang Makmur Mandiri</h3>
            <span><?php echo $this->lang->line('login_subheading'); ?></span>

            <?php echo $message;?>
            <?php echo form_open('',array('role'=>'form', 'class'=>'m-t')); ?>
            <div class="form-group">
                <input class="form-control" id="username" name="identity"
                    placeholder="<?php echo $this->lang->line('login_identity_placeholder');?>" required="" autofocus
                    autocomplete="off">
                <?= form_error('identity', '<span class=\'text-danger\' style=\'form-size: 10px\'>','</span>'); ?>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="userpassword" name="password"
                    placeholder="<?php echo $this->lang->line('login_password_placeholder');?>" required="">
                <?= form_error('password', '<span class=\'text-danger\' style=\'form-size: 10px\'>','</span>'); ?>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <!-- <a
                href="<?php echo site_url('auth/forgot_password') ?>"><small><?php echo $this->lang->line('login_forgot_password'); ?></small></a>
            <p class="text-muted text-center"><small><?php echo $this->lang->line('do_not_have_account'); ?></small></p>
            <a class="btn btn-sm btn-white btn-block"
                href="<?php echo site_url('auth/create_user') ?>"><?php echo $this->lang->line('create_an_account'); ?></a> -->
            <?php echo form_close(); ?>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo theme_assets('inspina') ?>js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo theme_assets('inspina') ?>js/popper.min.js"></script>
    <script src="<?php echo theme_assets('inspina') ?>js/bootstrap.js"></script>

</body>

</html>
