<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pemasok extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_pemasok_model');
        $this->breadcrumb->add('Admin pemasok',base_url().'admin_pemasok');
        $this->data['title'] = 'Admin pemasok';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_pemasok/js/datatables'); //load aditional js script
        $this->_render_page('admin_pemasok/html/index', $this->data);
    }

    /**
     *Data Services 
    **/
    public function datatables()
    {
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $this->load->library('Datatables');
            $this->datatables->select('users.id, concat(first_name , " ", last_name) as name, username, phone, address, email');
            $this->datatables->from('users');
            $this->datatables->join('users_groups as ug', 'ug.user_id = users.id');
            $this->datatables->where('ug.group_id', 6);
            $this->datatables->add_column(
                'action',
                '<a class="btn btn-icon btn-sm btn-success mr-1" href="pelanggan/edit/$1" title="edit">
                 <i class="fa fa-pencil">
                 </i>
                 <a class="btn btn-icon btn-sm btn-danger mr-1" href="pelanggan/delete/$1" title="delete">
                 <i class="fa fa-trash-o">
                 </i>',
                "id");
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output($this->datatables->generate());
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_status_header(401)
                ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
        }
    }

    /**
     * we use final to indicate that this function will be included to ACL table 
     **/

    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->_render_page('admin_pemasok/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if ($user = $this->Admin_pemasok_model->insert($data)) {
                $insertGroup['user_id']   = $user;
                $insertGroup['group_id'] = 6;
                $this->Admin_pemasok_model->insertUserGroup($insertGroup);
                $this->session->set_flashdata('success', 'Successfull create data');
                redirect('admin/pemasok', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/pemasok/form', 'refresh');
            }
        
        }

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['pemasok'] = $this->Admin_pemasok_model->getPemasok($id);
            $this->_render_page('admin_pemasok/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            if ($user = $this->Admin_pemasok_model->update($id, $data)) {
                $this->session->set_flashdata('success', 'Successfull update data');
                redirect('admin/pemasok', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/pemasok/form', 'refresh');
            }
        
        }
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }
        
        if ($user = $this->Admin_pemasok_model->delete($id)) {
            $this->session->set_flashdata('success', 'Successfull delete data');
            redirect('admin/pemasok', 'refresh');
        }
        else{
            $this->session->set_flashdata('failed', 'Check your data and try again');
            redirect('admin/pemasok', 'refresh');
        }
    }
    
}