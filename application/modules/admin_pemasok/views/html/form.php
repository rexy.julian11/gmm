<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Pemasok
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">First Name *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="first_name" value="<?= isset($pemasok) ? $pemasok->first_name : ''?>">
                                <?php echo form_error('first_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Last Name *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="last_name" value="<?= isset($pemasok) ? $pemasok->last_name : ''?>">
                                <?php echo form_error('last_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Email *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="email" value="<?= isset($pemasok) ? $pemasok->email : ''?>">
                                <?php echo form_error('email', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Address *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="address" value="<?= isset($pemasok) ? $pemasok->address : ''?>">
                                <?php echo form_error('address', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Phone Number *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="phone" value="<?= isset($pemasok) ? $pemasok->phone : ''?>">
                                <?php echo form_error('phone', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>