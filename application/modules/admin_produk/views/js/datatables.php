<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('admin_produk/datatables'); ?>',
                    type: "POST"
                },
                order: [[ 1, 'asc' ]],
                columnDefs: [
                                { "searchable": false, 
                                  "targets"   : 0 }
                            ],
                columns: [
                    {"data":'',"sortable":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {"data": "category_name"},
                    {"data": "product_name"},
                    {"data": "product_buy_price"},
                    {"data": "product_sell_price"},
                    {"data": "product_stock"},
                    {"data": "action","ordering":false}
                ], 
            });
            
            $('#selectCategory').select2({
                placeholder : 'Pilih Kategori'
            });
    
        });
        </script>