<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Produk
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kategori *</label>
                            <div class="col-sm-6">
                                <select name="category_id" class="form-control" id="selectCategory">
                                    <?php foreach ($categories as $row) { ?>
                                        <option value="<?= $row->category_id?>" <?= isset($product) && $product->category_id == $row->category_id ? 'selected' : '' ;?>><?= $row->category_name?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Produk *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="product_name" value="<?= isset($product) ? $product->product_name : ''?>">
                                <?php echo form_error('product_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Beli *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="product_buy_price" value="<?= isset($product) ? $product->product_buy_price : ''?>">
                                <?php echo form_error('product_buy_price', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Harga Jual *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="product_sell_price" value="<?= isset($product) ? $product->product_sell_price : ''?>">
                                <?php echo form_error('product_sell_price', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Stok *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="product_stock" value="<?= isset($product) ? $product->product_stock : ''?>">
                                <?php echo form_error('product_stock', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>                   
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>