<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_produk_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function insert($data)
    {
        return $this->db->insert('products', $data);
    }
    public function getProductData($product_id)
    {
        return $this->db->where('product_id', $product_id)
                        ->get('products')->row();
    }
    public function update($product_id, $data)
    {
        return $this->db->where('product_id', $product_id)
                        ->update('products', $data);
    }
    public function delete($product_id)
    {
        return $this->db->where('product_id', $product_id)
                        ->delete('products');
    }
    public function getCategory()
    {
        return $this->db->get('categories')->result();
    }
}