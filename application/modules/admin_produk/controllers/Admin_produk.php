<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_produk extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_produk_model');
                $this->breadcrumb->add('Admin produk',base_url().'admin_produk');
                $this->data['title'] = 'Admin produk';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_produk/js/datatables'); //load aditional js script
                $this->_render_page('admin_produk/html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
                if($this->input->server('REQUEST_METHOD')=='POST'){
                    $this->load->library('Datatables');
                    $this->datatables->select('p.product_id as id, product_name, product_buy_price, product_sell_price, product_stock, c.category_name');
                    $this->datatables->from('products as p');
                    $this->datatables->join('categories as c', 'c.category_id = p.category_id');
                    $this->datatables->add_column(
                        'action',
                        '<a class="btn btn-icon btn-sm btn-success mr-1" href="admin-produk/edit/$1" title="edit">
                         <i class="fa fa-pencil">
                         </i>
                         <a class="btn btn-icon btn-sm btn-danger mr-1" href="admin-produk/delete/$1" title="delete">
                         <i class="fa fa-trash-o">
                         </i>',
                        "id");
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output($this->datatables->generate());
                }else{
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(401)
                        ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
                }
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                $this->data['js_script'] = array('admin_produk/js/datatables'); //load aditional js script
                $this->load->library('form_validation');
                $this->form_validation->set_rules('product_name', 'Product Name', 'required');
                $this->form_validation->set_rules('product_buy_price', 'Buy Price', 'required|numeric');
                $this->form_validation->set_rules('product_sell_price', 'Sell Price', 'required|numeric');
                $this->form_validation->set_rules('product_stock', 'Stock', 'required|numeric');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->data['categories'] = $this->Admin_produk_model->getCategory();
                    $this->_render_page('admin_produk/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($category = $this->Admin_produk_model->insert($data)) {
                        $this->session->set_flashdata('success', 'Successfull create data');
                        redirect('admin/produk', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/produk/form', 'refresh');
                    }
                
                }

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }
                $this->data['js_script'] = array('admin_produk/js/datatables'); //load aditional js script
                $this->load->library('form_validation');
                $this->form_validation->set_rules('product_name', 'Product Name', 'required');
                $this->form_validation->set_rules('product_buy_price', 'Buy Price', 'required|numeric');
                $this->form_validation->set_rules('product_sell_price', 'Sell Price', 'required|numeric');
                $this->form_validation->set_rules('product_stock', 'Stock', 'required|numeric');
                if ($this->form_validation->run() == FALSE)
                {
                    $this->data['categories'] = $this->Admin_produk_model->getCategory();
                    $this->data['product'] = $this->Admin_produk_model->getProductData($id);
                    $this->_render_page('admin_produk/html/form', $this->data);
                }
                else
                {
                    $data = $this->input->post();
                    if ($category = $this->Admin_produk_model->update($id, $data)) {
                        $this->session->set_flashdata('success', 'Successfull create data');
                        redirect('admin/produk', 'refresh');
                    }
                    else{
                        $this->session->set_flashdata('failed', 'Check your data and try again');
                        redirect('admin/produk/form', 'refresh');
                    }
                
                }
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                if ($category = $this->Admin_produk_model->delete($id)) {
                    $this->session->set_flashdata('success', 'Successfull delete data');
                    redirect('admin/produk', 'refresh');
                }
                else{
                    $this->session->set_flashdata('failed', 'Check your data and try again');
                    redirect('admin/produk', 'refresh');
                }
            }
            
        }