<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Frontend_errors extends Frontend_Controller
        {
            public function __construct()
            {
                parent::__construct();
            }
            public function index()
            {
                redirect('error_404','refresh');
                
            }

            public function error_403(){
                $this->output->set_status_header(403);
                $this->data['title'] = 'Error 403';
                $this->_render_frontend_page('frontend/403', $this->data);
            }

            public function page_missing(){
                $this->output->set_status_header(404);
                 $this->data['title'] = 'Error 404';
                $this->_render_frontend_page('frontend/err_404', $this->data);
            }
            
           
        }
