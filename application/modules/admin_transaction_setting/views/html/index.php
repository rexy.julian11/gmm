<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Transaction Setting    
                </div>
            </div>
            <div class="card-header">
                <div class="pull-right" style="margin-bottom : 5px">
                    <a class="btn btn-primary" href="<?php echo site_url('admin/transaction-setting/add')?>">Add<i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr class="table-secondary">
                                <td>Name</td>
                                <td>From</td>
                                <td>To</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($ts as $key => $row) { ?>
                                <tr>
                                    <td><?php echo $row->ts_name?></td>
                                    <td>
                                        <?php foreach ($row->from as $row_from) { ?> 
                                            <?php echo $row_from->account_name?> <br>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?php foreach ($row->to as $row_to) { ?> 
                                            <?php echo $row_to->account_name?> <br>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <a href="<?php echo site_url('admin/transaction_setting/edit/'). $row->ts_id?>" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="<?php echo site_url('admin/transaction_setting/delete/'). $row->ts_id?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


