<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Income    
                </div>
                <div class="card-body">
                    <?php echo form_open()?>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name *</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="ts_name" value="<?php echo isset($setting) ? $setting->ts_name : ''?>">
                                <?php echo form_error('ts_name', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">From *</label>
                            <div class="col-sm-10">
                                <select name="ts_from[]" class="form-control selectAccounts first" multiple="multiple">
                                    <?php foreach ($accounts as $row) { ?>
                                        <option value="<?php echo $row->account_number?>"><?php echo $row->account_name?></option>
                                    <?php }?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">To *</label>
                            <div class="col-sm-10">
                                <select name="ts_to[]" class="form-control selectAccounts second" multiple="multiple" id="">
                                    <?php foreach ($accounts as $row) { ?>
                                        <option value="<?php echo $row->account_number?>"><?php echo $row->account_name?></option>
                                    <?php }?>         
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <button class="btn btn-primary pull-right" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>

