<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function(){
        $('.table').DataTable();

        var first = $('.first').select2();
        var second = $('.second').select2();
        var first_value = <?php echo json_encode($first) ?>;
        var second_value = <?php echo json_encode($second) ?>;
        first.val(first_value).trigger('change');
        second.val(second_value).trigger('change');
    });
</script>