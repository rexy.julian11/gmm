<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_transaction_setting extends Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_transaction_setting_model');
        $this->breadcrumb->add('Admin transaction setting',base_url().'admin_transaction_setting');
        $this->data['title'] = 'Admin transaction setting';
    }
    final public function index()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $this->data['header'] = array(); //load aditional stylesheets
        $this->data['js_library'] = array(); //load aditional js library
        $this->data['js_script'] = array('admin_transaction_setting/js/script'); //load aditional js script
        $this->data['ts'] = $this->Admin_transaction_setting_model->getTransactionSetting(); 
        $this->_render_page('admin_transaction_setting/html/index', $this->data);
    }
    
    /**
     *Data Services 
        **/
    public function datatables()
    {
        
    }
    
    /**
     * we use final to indicate that this function will be included to ACL table 
     **/
    
    final public function add()
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        $this->data['js_script'] = array('admin_transaction_setting/js/script'); //load aditional js script
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ts_name', 'Name', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['accounts'] = $this->Admin_transaction_setting_model->getAccounts();
            $this->_render_page('admin_transaction_setting/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            $insertSetting['ts_name']           = $data['ts_name'];
            $insertSetting['ts_from']           = 'F' . time() . rand(1,9);
            $insertSetting['ts_to']             = 'T' . time() . rand(1,9);
            if ($setting = $this->Admin_transaction_setting_model->insert($insertSetting)) {
                foreach ($data['ts_from'] as $key => $row) {
                    $insertDetail['tsd_acc_id']     = $row;
                    $insertDetail['tsd_parent_id']  = $insertSetting['ts_from'];
                    $this->Admin_transaction_setting_model->insertDetail($insertDetail);
                }
                foreach ($data['ts_to'] as $key => $row) {
                    $insertDetail['tsd_acc_id']     = $row;
                    $insertDetail['tsd_parent_id']  = $insertSetting['ts_to'];
                    $this->Admin_transaction_setting_model->insertDetail($insertDetail);
                }
                $this->session->set_flashdata('success', 'Successfull create data');
                redirect('admin/transaction_setting', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/transaction-setting/form', 'refresh');
            }
        }

    }
    final public function edit($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }
        $this->data['js_script'] = array('admin_transaction_setting/js/edit'); //load aditional js script
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ts_name', 'Name', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['accounts'] = $this->Admin_transaction_setting_model->getAccounts();
            $this->data['setting'] = $this->Admin_transaction_setting_model->getSetting($id);
            $this->data['first'] = [];
            $this->data['second'] = [];

            foreach ($this->data['setting']->from as $key => $row) {
                array_push($this->data['first'], $row->tsd_acc_id);
            }
            foreach ($this->data['setting']->to as $key => $row) {
                array_push($this->data['second'], $row->tsd_acc_id);
            }
            $this->_render_page('admin_transaction_setting/html/form', $this->data);
        }
        else
        {
            $data = $this->input->post();
            $insertSetting['ts_name']           = $data['ts_name'];
            if ($update = $this->Admin_transaction_setting_model->updateSetting($id, $insertSetting)) {
                $setting = $this->Admin_transaction_setting_model->getSetting($id);
                foreach ($setting->from as $key => $value) {
                    $this->Admin_transaction_setting_model->deleteSettingDetail($value->tsd_id);
                }
                foreach ($setting->to as $key => $value) {
                    $this->Admin_transaction_setting_model->deleteSettingDetail($value->tsd_id);
                }


                foreach ($data['ts_from'] as $key => $row) {
                    $insertDetail['tsd_acc_id']     = $row;
                    $insertDetail['tsd_parent_id']  = $setting->ts_from;
                    $this->Admin_transaction_setting_model->insertDetail($insertDetail);
                }
                foreach ($data['ts_to'] as $key => $row) {
                    $insertDetail['tsd_acc_id']     = $row;
                    $insertDetail['tsd_parent_id']  = $setting->ts_to;
                    $this->Admin_transaction_setting_model->insertDetail($insertDetail);
                }
                $this->session->set_flashdata('success', 'Successfull update data');
                redirect('admin/transaction_setting', 'refresh');
            }
            else{
                $this->session->set_flashdata('failed', 'Check your data and try again');
                redirect('admin/transaction-setting/form', 'refresh');
            }
        }
        
    }
    final public function delete($id)
    {
        //get permissions for this method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        if( ! $id ){
            if ($this->agent->is_referral())
            {
                redirect($this->agent->referrer());
            }
            else{
                redirect('admin_dashboard');
            }
        }
        $setting = $this->Admin_transaction_setting_model->getSetting($id);
        foreach ($setting->from as $key => $value) {
            $this->Admin_transaction_setting_model->deleteSettingDetail($value->tsd_id);
        }
        foreach ($setting->to as $key => $value) {
            $this->Admin_transaction_setting_model->deleteSettingDetail($value->tsd_id);
        }
        if ($delete = $this->Admin_transaction_setting_model->deleteSetting($id)) {   
            $this->session->set_flashdata('success', 'Successfull delete data');
            redirect('admin/transaction_setting', 'refresh');
        }
        else{
            $this->session->set_flashdata('failed', 'Check your data and try again');
            redirect('admin/transaction-setting/form', 'refresh');
        }
    }
    
}