<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_transaction_setting_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getTransactionSetting()
    {
        $ts = $this->db->get('transaction_setting')->result();

        foreach ($ts as $key => $row) {
            $tsd_from = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                                 ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                                 ->where('tsd_parent_id', $row->ts_from)
                                 ->get('transaction_setting_detail')->result();
            $tsd_to   = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                                 ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                                 ->where('tsd_parent_id', $row->ts_to)
                                 ->get('transaction_setting_detail')->result();
            $ts[$key]->from = $tsd_from;
            $ts[$key]->to   = $tsd_to;
        }
        return $ts;
    }
    public function getAccounts()
    {
        $this->db->where('a_parent.account_type', 'Account');
        return $this->db->get('accounts as a_parent')->result();
    }
    public function insert($data)
    {
        return $this->db->insert('transaction_setting', $data);
    }
    public function insertDetail($data)
    {
        return $this->db->insert('transaction_setting_detail', $data);
    }

    public function getSetting($id)
    {
        $ts = $this->db->where('ts_id', $id)
                       ->get('transaction_setting')->row();
        $tsd_from = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                            ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                            ->where('tsd_parent_id', $ts->ts_from)
                            ->get('transaction_setting_detail')->result();
        $tsd_to   = $this->db->select('transaction_setting_detail.*, accounts.account_name')
                            ->join('accounts', 'accounts.account_number = transaction_setting_detail.tsd_acc_id')
                            ->where('tsd_parent_id', $ts->ts_to)
                            ->get('transaction_setting_detail')->result();
        $ts->from = $tsd_from;
        $ts->to   = $tsd_to;

        return $ts;
    }
    public function updateSetting($id, $data)
    {
        $this->db->where('ts_id', $id);
        return $this->db->update('transaction_setting', $data);
    }
    public function deleteSetting($id)
    {
        $this->db->where('ts_id', $id);
        return $this->db->delete('transaction_setting');
    }
    public function deleteSettingDetail($id)
    {
        $this->db->where('tsd_id', $id);
        return $this->db->delete('transaction_setting_detail');
    }
}