<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_laporan_bukubesar_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function bukubesar($start_date, $end_date, $account_number)
    {
        $group = $this->db->where('account_type', 'Account')
                          ->where('account_number', $account_number)
                          ->get('accounts')->result();

        
        foreach ($group as $keyy => $valuee) {
            $starting_balance =  $this->db->select('j.journal_description, j.journal_datetime, j.journal_ref_pk, l.*, a.account_normal_balance, a.account_name, a.account_initial_balance')
                                        ->join('ledgers as l', 'l.journal_id = j.journal_id')
                                        ->join('accounts as a', 'a.account_number = l.account_number', 'left')
                                        ->where('a.account_type', 'Account')
                                        ->where('a.account_number', $valuee->account_number)
                                        ->where('j.journal_datetime <=', $start_date)
                                        ->order_by('a.account_number')
                                        ->get('journals as j')->result();
            $start_balance = 0;
            foreach ($starting_balance as $key => $value) {
                if ($key != 0) {
                    if ($value->account_number == $starting_balance[$key-1]->account_number) {
                        if ($value->account_normal_balance == 'Db') {
                            if ($value->ledger_increase_to == 'Db') {
                                $start_balance = (float)$starting_balance[$key-1]->new_balance + (float)$value->ledger_amount;
                            }
                            else{
                                $start_balance = (float)$starting_balance[$key-1]->new_balance - (float)$value->ledger_amount;
                            }
                        }
                        else{
                            if ($value->ledger_increase_to == 'Db') {
                                $start_balance = (float)$starting_balance[$key-1]->new_balance - (float)$value->ledger_amount;
                            }
                            else{
                                $start_balance = (float)$starting_balance[$key-1]->new_balance + (float)$value->ledger_amount;
                            }
                        }
                    }
                    else{
                        $balance = (float)$value->account_initial_balance;
                        if ($value->account_normal_balance == 'Db') {
                            if ($value->ledger_increase_to == 'Db') {
                                $start_balance = $start_balance + (float)$value->ledger_amount;
                            }
                            else{
                                $start_balance = $start_balance - (float)$value->ledger_amount;
                            }
                        }
                        else{
                            if ($value->ledger_increase_to == 'Db') {
                                $start_balance = $start_balance - (float)$value->ledger_amount;
                            }
                            else{
                                $start_balance = $start_balance + (float)$value->ledger_amount;
                            }
                        }
                    }
                }
                else{
                    $start_balance = (float)$value->account_initial_balance;
                    if ($value->account_normal_balance == 'Db') {
                        if ($value->ledger_increase_to == 'Db') {
                            $start_balance = $start_balance + (float)$value->ledger_amount;
                        }
                        else{
                            $start_balance = $start_balance - (float)$value->ledger_amount;
                        }
                    }
                    else{
                        if ($value->ledger_increase_to == 'Db') {
                            $start_balance = $start_balance - (float)$value->ledger_amount;
                        }
                        else{
                            $start_balance = $start_balance + (float)$value->ledger_amount;
                        }
                    }
                }
                $starting_balance[$key]->new_balance = $start_balance;
            }

            $ledger =  $this->db->select('j.journal_description, j.journal_datetime, j.journal_ref_pk, l.*, a.account_normal_balance, a.account_name, a.account_initial_balance')
                                ->join('ledgers as l', 'l.journal_id = j.journal_id')
                                ->join('accounts as a', 'a.account_number = l.account_number', 'left')
                                ->where('a.account_type', 'Account')
                                ->where('a.account_number', $valuee->account_number)
                                ->where('j.journal_datetime >=', $start_date)
                                ->where('j.journal_datetime <=', $end_date)
                                ->order_by('a.account_number')
                                ->get('journals as j')->result();
            foreach ($ledger as $key => $value) {
                if ($key != 0) {
                    if ($value->account_number == $ledger[$key-1]->account_number) {
                        if ($value->account_normal_balance == 'Db') {
                            if ($value->ledger_increase_to == 'Db') {
                                $balance = (float)$ledger[$key-1]->new_balance + (float)$value->ledger_amount;
                            }
                            else{
                                $balance = (float)$ledger[$key-1]->new_balance - (float)$value->ledger_amount;
                            }
                        }
                        else{
                            if ($value->ledger_increase_to == 'Db') {
                                $balance = (float)$ledger[$key-1]->new_balance - (float)$value->ledger_amount;
                            }
                            else{
                                $balance = (float)$ledger[$key-1]->new_balance + (float)$value->ledger_amount;
                            }
                        }
                    }
                    else{
                        $balance = (float)$value->account_initial_balance;
                        if ($value->account_normal_balance == 'Db') {
                            if ($value->ledger_increase_to == 'Db') {
                                $balance = $balance + (float)$value->ledger_amount;
                            }
                            else{
                                $balance = $balance - (float)$value->ledger_amount;
                            }
                        }
                        else{
                            if ($value->ledger_increase_to == 'Db') {
                                $balance = $balance - (float)$value->ledger_amount;
                            }
                            else{
                                $balance = $balance + (float)$value->ledger_amount;
                            }
                        }
                    }
                }
                else{
                    $balance = $start_balance;
                    if ($value->account_normal_balance == 'Db') {
                        if ($value->ledger_increase_to == 'Db') {
                            $balance = $balance + (float)$value->ledger_amount;
                        }
                        else{
                            $balance = $balance - (float)$value->ledger_amount;
                        }
                    }
                    else{
                        if ($value->ledger_increase_to == 'Db') {
                            $balance = $balance - (float)$value->ledger_amount;
                        }
                        else{
                            $balance = $balance + (float)$value->ledger_amount;
                        }
                    }
                }
                $ledger[$key]->new_balance = $balance;
                $ledger[$key]->account_initial_balance = $start_balance;
            }
            $group[$keyy]->ledgers = $ledger;
        }
        
        return $group;
    }
    public function getAccount()
    {
        return $this->db->select('account_number as id, account_name as text')
                        ->where('account_type', 'Account')
                        ->order_by('account_number', 'ASC')
                        ->get('accounts')->result();
    }
}