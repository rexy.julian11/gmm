<script>
    $(document).ready(function(){
        function number(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? ',' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
        var datatable  = $('.datatables').DataTable({
            "bPaginate": false,
            "columnDefs": [
                { "visible": false, "targets": [0, 7] },
                {
                    "render": $.fn.dataTable.render.number( ',', '.', 2, 'Rp.' ) ,
                    "targets": [4,5,6]
                },
                { "targets": [4, 5, 6], "className": 'text-right' }
            ],
            "drawCallback": function ( settings ) {
                var api  = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last = null;
                var total = 0;
                api.column(0, {page:'current'} ).data().each( function ( group, i) {
                    var data = datatable.row(i).data();
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr><td colspan="6"><hr></td></tr><tr class="group"><td colspan="5">'+group+'</td><td class="text-right">'+data[7]+'</td></tr>'
                        );
                        $(rows).eq(i-1).after(
                            '<tr class="group"><td colspan="5">Total :</td><td class="text-right">'+total+'</td></tr>'
                        );
                        last = group;
                        total = 0;
                    }
                    else{
                        total = total + data[6];
                        console.log(total)
                    }
                });

            }
        });
        $('#selectAccount').select2();
        $('.btnSubmit').click(function(){
            var account_number = $('#selectAccount').val();
            var start_date = $('input[name="start_date"]').val();
            var end_date   = $('input[name="end_date"]').val();
            if (start_date.length === 0 || end_date.length === 0 || account_number == null) {
                $.notify("Please fill the form", "warn");
            }
            else{
                $.ajax({
                    url : '<?= site_url('admin/laporan-bukubesar/getbukubesar')?>',
                    type : 'POST',
                    data : {start_date : start_date,
                            end_date : end_date,
                            account_number : account_number}
                }).done(function(r){
                    $('#table').html('');
                    var group = ``;
                    let start = moment(start_date).format('DD MMMM YYYY');
                    let end   = moment(end_date).format('DD MMMM YYYY');
                    let total_db     = 0;
                    let total_cr     = 0;
                    let total_amount = 0;
                    let start_balance= 0;
                    let end_balance  = 0;
                    let mutasi        = 0;
                    $.each(r, function(i, data){
                        group += `
                                <br><br><div class="row justify-content-md-center">
                                    <div class="col-md-auto">
                                        <h2 style="text-align : center">Buku Besar - ${data.account_name}</h2>
                                        <h3 style="text-align : center">${start} - ${end}</h3>
                                    </div>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Account</th>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">No Ref</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Debet</th>
                                            <th scope="col">Kredit</th>
                                            <th scope="col">Amount</th>
                                        </tr> 
                                    </thead>
                                    <tbody class="dataLedger">`;
                        var last = null;
                        $.each(data.ledgers, function(o, dataa){
                            if (last != dataa.account_name) {
                                start_balance = parseFloat(dataa.account_initial_balance);
                                group += `<tr><td colspan="6">Start Balance</td><td class="text-right">Rp ${number(parseFloat(dataa.account_initial_balance))}</td></tr>`;
                                if (dataa.ledger_increase_to == 'Db') {
                                    group += `<tr>
                                                <td></td>
                                                <td>${moment(dataa.journal_datetime).format('DD MMMM YYYY, h:mm:ss a')}</td>
                                                <td>${dataa.journal_ref_pk}</td>
                                                <td>${dataa.journal_description}</td>
                                                <td class="text-right">Rp ${number(dataa.ledger_amount)}</td>
                                                <td></td>
                                                <td class="text-right">Rp ${number(dataa.new_balance)}</td>
                                            </tr>`;
                                    total_db = total_db + parseFloat(dataa.ledger_amount);
                                }
                                else{
                                    group += `<tr>
                                                <td></td>
                                                <td>${moment(dataa.journal_datetime).format('DD MMMM YYYY, h:mm:ss a')}</td>
                                                <td>${dataa.journal_ref_pk}</td>
                                                <td>${dataa.journal_description}</td>
                                                <td></td>
                                                <td class="text-right">Rp ${number(dataa.ledger_amount)}</td>
                                                <td class="text-right">Rp ${number(dataa.new_balance)}</td>
                                            </tr>`;
                                    total_cr = total_cr + parseFloat(dataa.ledger_amount);
                                }
                                last = dataa.account_name;
                            }
                            else{
                                if (dataa.ledger_increase_to == 'Db') {
                                    group += `<tr>
                                                <td></td>
                                                <td>${moment(dataa.journal_datetime).format('DD MMMM YYYY, h:mm:ss a')}</td>
                                                <td>${dataa.journal_ref_pk}</td>
                                                <td>${dataa.journal_description}</td>
                                                <td class="text-right">Rp ${number(dataa.ledger_amount)}</td>
                                                <td></td>
                                                <td class="text-right">Rp ${number(dataa.new_balance)}</td>
                                            </tr>`;
                                    total_db = total_db + parseFloat(dataa.ledger_amount);
                                }
                                else{
                                    group += `<tr>
                                                <td></td>
                                                <td>${moment(dataa.journal_datetime).format('DD MMMM YYYY, h:mm:ss a')}</td>
                                                <td>${dataa.journal_ref_pk}</td>
                                                <td>${dataa.journal_description}</td>
                                                <td></td>
                                                <td class="text-right">Rp ${number(dataa.ledger_amount)}</td>
                                                <td class="text-right">Rp ${number(dataa.new_balance)}</td>
                                            </tr>`;
                                    total_cr = total_cr + parseFloat(dataa.ledger_amount);
                                }
                            }
                        });
                        group +=` </tbody>
                                </table><br><br>`; 
                        $('#table').append(group);
                        if (data.account_normal_balance == 'Db') {
                            total_amount = total_amount + total_db;
                            mutasi      = total_db - total_cr;
                            end_balance = end_balance + mutasi;
                        }
                        else{
                            total_amount = total_amount + total_cr;
                            mutasi      = total_cr - total_db;
                            end_balance = end_balance + mutasi;
                        }
                        let htmlTotal = `<tr class="table-secondary">
                                            <td>Saldo Awal : </td>
                                            <td class="text-right">Rp ${number(start_balance)}</td>
                                            <td></td>
                                            <td class="text-right">Total : </td>
                                            <td class="text-right">Rp ${number(total_db)}</td>
                                            <td class="text-right">Rp ${number(total_cr)}</td>
                                            <td></td>
                                        </tr>
                                        <tr class="table-secondary">
                                            <td>Saldo Akhir : </td>
                                            <td class="text-right">Rp ${number(end_balance)}</td>
                                            <td></td>
                                            <td class="text-right">Mutasi : </td>
                                            <td class="text-right">Rp ${number(mutasi)}</td>
                                            <td colspan="2"></td>
                                        </tr>`;
                        $('.dataLedger').append(htmlTotal);
                    });
                });
            }
        });
    });
</script>