<style>
    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
            

            <section class="card">
                <header class="card-header">
                    Laporan Buku Besar
                </header>
                <div class="card-body">
                    <div class="row justify-content-md-center">
                        <div class="col">
                            <select name="" id="selectAccount" class="form-control">
                                <?php foreach ($accounts as $row) { ?>
                                    <option value="<?= $row->id?>"><?= $row->text?></option>
                                <?php }?>
                            </select>
                        </div>                                
                        <div class="col">
                            <input type="date" name="start_date" class="form-control" placeholder="Start Date">
                        </div>                                
                        <div class="col">
                            <input type="date" name="end_date" class="form-control" placeholder="End Date">
                        </div>
                        <div class="col">
                            <button class="btn btn-primary btnSubmit" type="button">Submit</button>
                        </div>
                    </div>
                    <div class="table-responsive" id="table">
                        
                    </div>
                </div>
            </section>
    </div>
</div>
