<html>
    <body>
        <div style="text-align: center">
            <h2 style="color:#3381ff;">Buku Besar</h2>
            <h4 style="color:#ed3434;"><?= date("d-m-Y", strtotime($start_date))?> - <?= date("d-m-Y", strtotime($end_date))?></h4>
        </div>
        <hr>
        <?php foreach ($laporan as $row) { ?>
            <table>
                <thead style="background-color:#d1d1d1;">
                    <tr>
                        <td colspan="8"><?= $row->account_name?></td>
                    </tr>
                    <tr>
                        <td style="text-align:center">Tanggal</td>
                        <td style="text-align:center">Tp</td>
                        <td style="text-align:center">No. Ref</td>
                        <td style="text-align:center; width:300px;">Keterangan</td>
                        <td style="text-align:center">No. Dept</td>
                        <td style="text-align:center">Debet</td>
                        <td style="text-align:center">Kredit</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($row->ledger as $rows) { ?>
                    <tr>
                        <td><?= date("d-m-Y", strtotime($rows->journal_datetime))?></td>
                        <td></td>
                        <td></td>
                        <td><?= $rows->journal_description?></td>
                        <td></td>
                        <?php if ($rows->ledger_increase_to == 'Db') { ?>
                            <td><?= $rows->ledger_amount?></td>
                            <td></td>
                        <?php } else{?>
                            <td></td>
                            <td><?= $rows->ledger_amount?></td>
                        <?php }?>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
            <br>
        <?php } ?>
    </body>
</html>