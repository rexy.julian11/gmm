<script>
    $(document).ready(function(){
        function countPayed()
        {
            var totalPay = 0;
            var sisaTagihan = 0;
            var dibayarkan = parseFloat($('.inputDibayarkan').val());
            var totalTagihan = parseFloat($('.inputTotalTagihan').val());
            $('.inputPayment').unbind().each(function(){
                var value = parseFloat($(this).val());
                if (isNaN(value)) {
                    value = 0;
                }
                totalPay = totalPay + value;
            });
            $('.totalDibayarkan').html(totalPay);
            sisaTagihan = totalTagihan - totalPay - dibayarkan;
            $('.inputSisaTagihan').val(sisaTagihan).trigger('change');
        }
        function countTotal()
        {
            $('body').on('change', '.inputPayment', function(){
                countPayed();
            });
            $('.btnRemove').unbind().click(function(){
                $(this).closest('tr').remove();
                countPayed();
            });
        }
        $('#fakturPembelian').select2({
            placeholder : 'Pilih Faktur Pembelian',
            ajax: {
                url: '<?= site_url('admin/pembayaran-pembelian/getOrderData')?>',
                processResults: function (data) {
                return {
                    results: data
                };
                }
            }
        });

        $('.btnAddPayment').click(function(){
            $.notify("Harap Pilih Pembelian Terlebih Dahulu", "warn");
        });

        $('#fakturPembelian').unbind().change(function(){
            var id = $(this).val();
            var total = 0;
            $.ajax({
                url : '<?= site_url('admin/pembayaran-pembelian/getorderdetail/')?>' + id,
                type : 'POST'
            }).done(function(r){
                var html = ``;
                var dibayarkan = 0;
                var totalTagihan = 0;
                $('#po_date').val(moment(r.po_date).format('YYYY-MM-DD'));
                $('#po_bill').val(r.po_bill);
                $('#po_date_expired').val(moment(r.po_expired_date).format('YYYY-MM-DD'));
                $('#inputNama').val(r.supplier_name);
                $('#inputTelp').val(r.phone);
                $('#inputAlamat').val(r.address);
                $('#order_number').val(r.po_no);
                $('.tblBarang').html('');
                $('.tblPayment').html('');
                $('.totalDibayarkan').html('');
                $.each(r.item, function(i, data){
                    total = total + parseInt(data.item_total);
                    html += `<tr>
                                <td class="tblItemId" style="display:none;">${data.item_id}</td>
                                <td>${data.item_name}</td>
                                <td>${data.item_buy_price}</td>
                                <td class="tblItemQty">${data.item_qty}</td>
                                <td class="text-right">${data.item_total}</td>
                            </tr>`;
                })
                if (r.dibayarkan.payment_amount != null) {
                    dibayarkan = dibayarkan + parseFloat(r.dibayarkan.payment_amount);
                }
                totalTagihan = parseFloat(total) - dibayarkan;
                $('.tblBarang').append(html);
                $('.grandTotal').html(total);
                $('.inputTotalTagihan').val(total);
                $('.inputSisaTagihan').val(totalTagihan);
                $('.inputDibayarkan').val(dibayarkan);
                $('.btnAddPayment').unbind().click(function(){
                    var htmlPayment = `<tr>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-small btnRemove"><span class="glyphicon glyphicon-minus"></span></button>
                                            </td>
                                            <td>
                                                <select class="form-control selectPaymentType" name="" required>
                                                    <option selected disabled>Pilih Metode Pembayaran</option>
                                                    <option value="Tunai">Tunai</option>
                                                    <option value="Giro">Giro</option>
                                                    <option value="Transfer">Transfer</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control selectPayment" name="" required>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control inputPayment" style="text-align: right; " name="" id="" required>
                                            </td>
                                       </tr>`;
                    $('.tblPayment').append(htmlPayment);
                    $('.selectPayment').select2({
                        placeholder : 'Pilih Akun Pembayaran',
                        ajax: {
                            url: '<?= site_url('admin/pembayaran-pembelian/getPaymentAccount')?>',
                            processResults: function (data) {
                            return {
                                results: data
                            };
                            }
                        }
                    });
                    countTotal()
                });
                $('.btnSubmit').unbind().click(function(){  
                    var order_number    = $('#order_number').val();
                    var tblItem         = $('.tblBarang').find('tr');
                    var account_number  = [];
                    var payment_method  = [];
                    var payment_amount  = [];
                    var items           = [];
                    $('.selectPayment').unbind().each(function(){
                        account_number.push($(this).val());
                    });
                    $('.selectPaymentType').unbind().each(function(){
                        payment_method.push($(this).val());
                    });
                    $('.inputPayment').unbind().each(function(){
                        payment_amount.push(parseFloat($(this).val()));
                    });
                    tblItem.unbind().each(function(){
                        items.push({'item_id' : $(this).find('.tblItemId').html(), 'item_qty' : $(this).find('.tblItemQty').html()});
                    });
                    if (account_number.length === 0 || payment_method.length === 0 || payment_amount.length === 0) {
                        $.notify("Please fill the form", "warn");
                    }
                    else{
                        $.ajax({
                            url : '<?= site_url('admin/pembayaran-pembelian/dopayment')?>',
                            type : 'POST',
                            data : {order_number : order_number,
                                    account_number : account_number,
                                    payment_method : payment_method,
                                    payment_amount : payment_amount,
                                    items : items}
                        }).done(function(r){
                            if (r == 'true') {
                                window.location.href = "<?= site_url('admin/pembayaran-pembelian')?>";
                            }
                            else{
                                $.notify("Check Your Data", "warn");
                            } 
                        });
                    }
                });

            });
        });
    });
</script>