<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
	var datatable = null;
    $(document).ready(function(){
        function terbilang(bilangan, sufix){
            if(bilangan=="" || bilangan==null || bilangan=="null" || bilangan==undefined){
                return "";
            } else {
                bilangan = bilangan.replace(/[^,\d]/g, '');
                var kalimat="";
                var angka   = new Array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
                var kata    = new Array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
                var tingkat = new Array('','Ribu','Juta','Milyar','Triliun');
                var panjang_bilangan = bilangan.length;
        
                /* pengujian panjang bilangan */
                if(panjang_bilangan > 15){
                    kalimat = "Diluar Batas";
                }else{
                    /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
                    for(i = 1; i <= panjang_bilangan; i++) {
                        angka[i] = bilangan.substr(-(i),1);
                    }
        
                    var i = 1;
                    var j = 0;
        
                    /* mulai proses iterasi terhadap array angka */
                    while(i <= panjang_bilangan){
                        subkalimat = "";
                        kata1 = "";
                        kata2 = "";
                        kata3 = "";
        
                        /* untuk Ratusan */
                        if(angka[i+2] != "0"){
                            if(angka[i+2] == "1"){
                                kata1 = "Seratus";
                            }else{
                                kata1 = kata[angka[i+2]] + " Ratus";
                            }
                        }
        
                        /* untuk Puluhan atau Belasan */
                        if(angka[i+1] != "0"){
                            if(angka[i+1] == "1"){
                                if(angka[i] == "0"){
                                    kata2 = "Sepuluh";
                                }else if(angka[i] == "1"){
                                    kata2 = "Sebelas";
                                }else{
                                    kata2 = kata[angka[i]] + " Belas";
                                }
                            }else{
                                kata2 = kata[angka[i+1]] + " Puluh";
                            }
                        }
        
                        /* untuk Satuan */
                        if (angka[i] != "0"){
                            if (angka[i+1] != "1"){
                                kata3 = kata[angka[i]];
                            }
                        }
        
                        /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
                        if ((angka[i] != "0") || (angka[i+1] != "0") || (angka[i+2] != "0")){
                            subkalimat = kata1+" "+kata2+" "+kata3+" "+tingkat[j]+" ";
                        }
        
                        /* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
                        kalimat = subkalimat + kalimat;
                        i = i + 3;
                        j = j + 1;
                    }
        
                    /* mengganti Satu Ribu jadi Seribu jika diperlukan */
                    if ((angka[5] == "0") && (angka[6] == "0")){
                        kalimat = kalimat.replace("Satu Ribu","Seribu");
                    }
                }
                return sufix == undefined ? kalimat : kalimat + sufix;
            }
        }


        $('.table-hidden').hide();
        datatable = $('.datatables').DataTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ajax": {
                "url": '<?php echo site_url('admin/pembayaran-pembelian/datatables'); ?>',
                "type": "POST"
            },
            "order": [[ 1, 'asc' ]],
            "columnDefs": [
                                { "searchable": false, 
                                  "targets"   : 0 }
                            ],
            "columns": [
                {"data":'',"sortable":false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {"data": "id"},
                {"data": "po_date"},
                {"data": "po_bill"},
                {"data": "po_expired_date"},
                {"data": "supplier_name"},
                {"data": "account_name"},
                {"data": "payment_method"},
                {"data": "payment_amount"}
                // {"data": "action","ordering":false},
                // {
                //     "className":      'po_no',
                //     "orderable":      false,
                //     "data":           'invoice',
                //     "defaultContent": ''
                // },
            ]
        });
        

        $('.datatables tbody').on('click', 'td.po_no', function () {
            var tr = $(this).closest('tr');
            var row = datatable.row( tr );
            var id = row.data().id;
            $.ajax({
                url : '<?= site_url('admin/pesanan-pembelian/getinvoice/')?>' + id,
                type : 'GET'
            }).done(function(r){
                console.log(r);
                $('#modalInvoice').html(r.po_no);
                $('#modalSupplier').html(r.supplier_name);
                $('#modalPhone').html(r.phone);
                $('#modalAddress').html(r.address);
                $('#modalDate').html('<strong>Tanggal : </strong>' + r.po_date);
                $('#modalDateExpired').html('<strong>Tanggal Kadaluarsa : </strong>' + r.po_expired_date);
                var htmlItem = ``;
                var htmlService = ``;
                var subTotalItem = 0;
                var subTotalService = 0;
                $.each(r.items, function(i, data){
                    htmlItem += `<tr>
                                    <td>${data.item_name}</td>
                                    <td>${data.item_price}</td>
                                    <td>${data.item_qty}</td>
                                    <td>${data.item_total}</td>
                                </tr>`;
                    subTotalItem = subTotalItem + parseInt(data.item_total);
                });
                $.each(r.services, function(i, data){
                    htmlService += `<tr>
                                    <td>${data.service_name}</td>
                                    <td>${data.pos_price}</td>
                                    <td>${data.pos_unit}</td>
                                    <td>${data.pos_qty}</td>
                                    <td>${data.pos_duration}</td>
                                    <td>${data.pos_total}</td>
                                </tr>`;
                    subTotalService = subTotalService + parseInt(data.pos_total);
                });
                let grandTotal = subTotalItem + subTotalService;
                $('#modalSubItem').html(subTotalItem);
                $('#modalSubService').html(subTotalService);
                $('#modalTotal').html(grandTotal);
                // $('#modalTerbilang').html(terbilang(grandTotal.toString(), 'Rupiah'));
                $('#modalTblItem').html(htmlItem);
                $('#modalTblService').html(htmlService);
                $('#modalDetail').modal('show');
            });
        } );
        

    });
	
</script>