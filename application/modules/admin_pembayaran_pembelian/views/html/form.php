<style>
    .center{
        width: 150px;
        margin: 40px auto;   
    }
</style>
<div class="page-content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Form Pembayaran Pembelian
                </div>
                <div class="card-body">
                    <?= form_open();?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Pembelian *</label>
                                    <div class="col-sm-8">
                                        <select name="order_number" class="form-control" id="fakturPembelian">
                                        </select>
                                    </div>
                                </div>               
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal PO *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="po_date" id="po_date" value="" readonly>
                                        <?php echo form_error('po_date', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Total Tagihan *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="po_bill" id="po_bill" value="" readonly>
                                        <?php echo form_error('po_bill', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Tanggal Jatuh Tempo *</label>
                                    <div class="col-sm-8">
                                        <input type="date" class="form-control" name="po_date_expired" id="po_date_expired" value="" readonly>
                                        <?php echo form_error('po_date_expired', '<span class="text-danger" style="form-size: 10px">','</span>');?>
                                    </div>
                                </div>            
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Supplier </label>
                                    <div class="col-sm-8">
                                    </div>
                                </div>
                                <div class="alert alert-dismissible alert-secondary">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputNama" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Telepon </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputTelp" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Alamat </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" id="inputAlamat" disabled>
                                        </div>
                                    </div>
                                    <input type="hidden" id="order_number" value="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="divSection">
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col">Barang</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col" style="width:150px">Kuantiti</th>
                                        <th scope="col" class="text-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblBarang">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right grandTotal"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <hr>
                        <div class="divSection">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-primary pull-right btnAddPayment"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <table class="table table-hover">
                                <thead class="table-active">
                                    <tr>
                                        <th scope="col">-</th>
                                        <th scope="col" style="width:250px">Metode Pembayaran</th>
                                        <th scope="col" style="width:200px">Akun</th>
                                        <th scope="col" class="text-right">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody class="tblPayment">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="table-active">Sub Total</td>
                                        <td class="table-active pull-right totalDibayarkan"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <hr>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Total Tagihan</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control text-right inputTotalTagihan" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Dibayarkan</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control text-right inputDibayarkan" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-2">
                                <label class="col-form-label pull-right">Sisa Tagihan</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control text-right inputSisaTagihan" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary pull-right btnSubmit">Submit</button>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
</div>