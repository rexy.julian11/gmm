<?php defined('BASEPATH') or exit('No direct script access allowed');
class Admin_pembayaran_pembelian_model extends CI_Model
{
    private $table = '';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        
    }
    public function getOrderData()
    {
        return $this->db->select('po_no as id, po_no as text')
                        ->where('is_paid', 0)
                        ->get('purchase_orders')->result();
    }
    public function getOrderDetail($po_no)
    {
        $order = $this->db->select('po.*, CONCAT(supplier.first_name , " ", supplier.last_name) as supplier_name, supplier.phone, supplier.address')
                          ->join('users as supplier', 'supplier.id = po.supplier_id')
                          ->where('po.po_no', $po_no)
                          ->get('purchase_orders as po')->row();
        $item = $this->db->select('p.*, i.item_name')
                         ->join('items as i', 'i.item_id = p.item_id')
                         ->where('p.po_id', $po_no)
                         ->get('purchase_order_item_details as p')->result();
        $dibayarkan = $this->db->select_sum('payment_amount')
                               ->where('order_number', $po_no)
                               ->get('payment')->row();
        $order->item = $item; 
        $order->dibayarkan = $dibayarkan; 
        return $order;
    }
    public function getPaymentAccount()
    {
        return $this->db->select('account_number as id, account_name as text')
                        ->where('account_type', 'Account')
                        ->get('accounts')->result();
    }
    public function doPayment($data)
    {
        return $this->db->insert('payment', $data);
    }
    public function checkPayment($order_number)
    {
        return $this->db->select_sum('payment_amount')
                        ->where('order_number', $order_number)
                        ->group_by('order_number')
                        ->get('payment')->row();
    }
    public function totalOrder($order_number)
    {
        return $this->db->select('po_bill')
                        ->where('po_no', $order_number)
                        ->get('purchase_orders')->row();
    }
    public function updateOrder($order_number, $data)
    {
        return $this->db->where('po_no', $order_number)
                        ->update('purchase_orders', $data);
    }
    public function getItemData($item_id)
    {
        return $this->db->where('item_id', $item_id)
                        ->get('items')->row();
    }
    public function updateStock($item_id, $data)
    {
        return $this->db->where('item_id', $item_id)
                        ->update('items', $data);
    }
}