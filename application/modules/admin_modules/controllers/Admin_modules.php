<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');}

class Admin_modules extends MY_Controller
{
    protected $exclude = array(
        'admin_dashboard',
        'admin_modules',
        'admin_groups',
        'admin_users',
        'auth',
        'template'
    );

   
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('directory');
        $this->load->library('ion_auth');
		$this->load->library('ion_auth_acl');

        
    }
    final public function index()
    {
       
         //get permissions for each method
        if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
            redirect('errors/error_403','refresh');
        }
        
        $data['modules'] = $this->get_modules();
        $this->load->view('admin_modules/v_index', $data);
    }
    
    public function get_modules(){
        $list = glob('application/modules/*', GLOB_ONLYDIR);
        $data = array();
        $no = 1;
        foreach ($list as $module) {
            $no++;
            $row = array();
            $module_title = str_replace('application/modules/', '', $module);
            $row['module'] = '<a href="'.$module_title.'" target="_blank">'.$module_title.'</a>';
            //add html for action
            if(in_array($module_title, $this->exclude)){
                $row['action'] = '<button class="btn btn-sm btn-danger uninstall" disabled title="Hapus"><i class="glyphicon glyphicon-trash"></i> Uninstall</button>';
            }
            else{
                $row['action'] = '<button class="btn btn-sm btn-danger uninstall" onCLick="javascript:uninstall(\''.$module.'\')" title="Hapus"><i class="glyphicon glyphicon-trash"></i> Uninstall</button>';
            }
            $data[] = $row;
        }
        return $data;
    }
	/**
	 * List folder modules
	 *
	 **/
    public function ajax_get_module()
    {
        $list = glob('application/modules/*', GLOB_ONLYDIR);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $module) {
            $no++;
            $row = array();
            $module_title = str_replace('application/modules/', '', $module);
            $row[] = '<a href="'.$module_title.'" target="_blank">'.$module_title.'</a>';
            //add html for action
            if(in_array($module_title, $this->exclude)){
                $row[] = '<button class="btn btn-sm btn-danger uninstall" disabled title="Hapus"><i class="glyphicon glyphicon-trash"></i> Uninstall</button>';
            }
            else{
                $row[] = '<button class="btn btn-sm btn-danger uninstall" onCLick="javascript:uninstall(\''.$module.'\')" title="Hapus"><i class="glyphicon glyphicon-trash"></i> Uninstall</button>';
            }
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => count(directory_map(APPPATH.'modules/', 1)),
                        "recordsFiltered" => count(directory_map(APPPATH.'modules/', 1)),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    
    public function ajax_gen_modul()
    {
        $modules_name=$this->input->post('mod_name');
        $mod_con_name=$this->input->post('mod_con_name');
        $mod_model_name=$this->input->post('mod_model_name');
        
        $mod_dir=array('controllers','models','views');

        $controller='controllers';
        $models='models';

        $controller_file=$mod_con_name.'.php';
        $model_file=$mod_model_name.'.php';

        $main_modules=APPPATH.'modules/'.$modules_name;
        //$views_path=APPPATH.'modules/main_modules/views/'.$theme_name;
        $isi = '';
        $view_file = array(
            'index.php',
            'add.php',
            'edit.php',
        );
        $js_file = array(
            'datatables.php',
        );

		if (!file_exists($main_modules)) 
		{
            $create_dir=mkdir($main_modules, 0777, true);
            
			if ($create_dir) 
			{
				foreach ($mod_dir as $dir) 
				{
                    mkdir($main_modules.'/'.$dir, 0644, true);
                }
                mkdir($main_modules.'/views/html/', 0644, true);
                mkdir($main_modules.'/views/js/', 0644, true);
                write_file($main_modules.'/controllers/'.$controller_file, $this->isi_controller($mod_con_name,$mod_model_name,$modules_name));
                write_file($main_modules.'/models/'.$model_file, $this->isi_model($mod_model_name));
				foreach ($view_file as $view_file) 
				{
                    write_file($main_modules.'/views/html/'.$view_file, $isi);
                }
                foreach ($js_file as $js_file) 
				{
                    write_file($main_modules.'/views/js/'.$js_file, $this->isi_js($modules_name));
                }



                echo json_encode(array("status" => true));
            }
		}
		else 
		{
            echo json_encode(array("status" => false));
        }
    }
    
    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            if(rmdir($dir)){
                return TRUE;
            }
            else{
                return FALSE;
            }
        }
        else{
            return FALSE;
        }
    } 
    
    public function ajax_delete_modul()
    {
        $module=$this->input->post('modul');
        $module = explode('/', $module);

        $main_modules=APPPATH.'modules/'.$module[array_key_last($module)];

        
		if (file_exists($main_modules)) 
		{
            if($this->rrmdir($main_modules))
            {
                echo json_encode(array("status" => true));
            }
            else{
                echo json_encode(array("status" => false));
            }

           
				
		}
		else 
		{
            echo json_encode(array("status" => false));
        }
    }
    
    public function isi_controller($mod_con_name,$mod_model_name,$modules_name)
    {
        $title = explode("_",$modules_name);
        return $isi_controller ="<?php defined('BASEPATH') or exit('No direct script access allowed');
        class " . $mod_con_name . " extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                \$this->load->model('$mod_model_name');
                \$this->breadcrumb->add('". ucfirst(implode(" ",$title))."',base_url().'".$modules_name."');
                \$this->data['title'] = '". ucfirst(implode(" ",$title))."';
            }
            final public function index()
            {
                //get permissions for this method
                if(! \$this->ion_auth_acl->has_permission(\$this->router->fetch_class().\"/\".\$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                \$this->data['header'] = array(); //load aditional stylesheets
                \$this->data['js_library'] = array(); //load aditional js library
                \$this->data['js_script'] = array('".$modules_name."/js/datatables'); //load aditional js script
                \$this->_render_page('".$modules_name."/html/index', \$this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
                if(\$this->input->server('REQUEST_METHOD')=='POST'){
                    \$this->load->library('Datatables');
                    \$this->datatables->select('');
                    \$this->datatables->from('');
                    \$this->datatables->add_column(
                        'action',
                        '<a class=\"btn btn-icon btn-sm btn-success mr-1\" href=\"\" title=\"edit\">
                         <i class=\"fa fa-pencil\">
                         </i>
                         <a class=\"btn btn-icon btn-sm btn-danger mr-1\" href=\"\" title=\"delete\">
                         <i class=\"fa fa-trash-o\">
                         </i>',
                        \"id\");
                    \$this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(\$this->datatables->generate());
                }else{
                    \$this->output
                        ->set_content_type('application/json')
                        ->set_status_header(401)
                        ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
                }
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! \$this->ion_auth_acl->has_permission(\$this->router->fetch_class().\"/\".\$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                //start your code here

            }
            final public function edit(\$id)
            {
                //get permissions for this method
                if(! \$this->ion_auth_acl->has_permission(\$this->router->fetch_class().\"/\".\$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! \$id ){
                    if (\$this->agent->is_referral())
                    {
                        redirect(\$this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            final public function delete(\$id)
            {
                //get permissions for this method
                if(! \$this->ion_auth_acl->has_permission(\$this->router->fetch_class().\"/\".\$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! \$id ){
                    if (\$this->agent->is_referral())
                    {
                        redirect(\$this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            
        }";
    }
    
    public function isi_model($mod_model_name)
    {
        return $isi_model ="<?php defined('BASEPATH') or exit('No direct script access allowed');
        class " . $mod_model_name . " extends CI_Model
        {
            private \$table = '';
            public function __construct()
            {
                parent::__construct();
            }
            public function index()
            {
                
            }
            
        }";
    }

    public function isi_js($modules_name){
        return $isi_js = "<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
        <script>
        var datatable = null;
        $(document).ready(function(){
    
            datatable = $('.datatables').DataTable({ 
                processing: true, //Feature control the processing indicator.
                serverSide: true, //Feature control DataTables' server-side processing mode.
                ajax: {
                    url: '<?php echo site_url('".$modules_name."/datatables'); ?>',
                    type: \"POST\"
                },
                order: [[ 1, 'asc' ]],
                columns: [
                    {\"data\":'',\"sortable\":false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }  
                    },
                    {\"data\": \"action\",\"ordering\":false}
                ], 
            });
    
    
        });
        </script>";
    }
}

/* End of file */
/* Location: ./application/controllers/ */
