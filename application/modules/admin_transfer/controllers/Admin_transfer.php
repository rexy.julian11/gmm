<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_transfer extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_transfer_model');
                $this->breadcrumb->add('Admin transfer',base_url().'admin_transfer');
                $this->data['title'] = 'Admin transfer';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_transfer/js/datatables'); //load aditional js script
                $this->_render_page('admin_transfer/html/index', $this->data);
            }
            
            /**
             *Data Services 
            **/
            public function datatables()
            {
                
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                //start your code here

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            
        }