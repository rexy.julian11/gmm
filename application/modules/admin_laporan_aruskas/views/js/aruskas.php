<script>
    $(document).ready(function(){
        var datatable  = $('.datatables').DataTable({
            "bPaginate": false,
            "columnDefs": [
                { "visible": false, "targets": 0 },
                {
                    "render": $.fn.dataTable.render.number( ',', '.', 2, 'Rp.' ) ,
                    "targets": 2
                },
                { "targets": 2, "className": 'text-right' }
            ],
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="3">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
                
            },
            "ordering": false
        });
        $('#selectAccount').select2(
            {
                placeholder : 'Select Account',
                multiple : true
            }
        );
        $('.btnSubmit').click(function(){
            var account_number = $('#selectAccount').val();
            var start_date = $('input[name="start_date"]').val();
            var end_date   = $('input[name="end_date"]').val();
            if (start_date.length === 0 || end_date.length === 0 || account_number == null) {
                $.notify("Please fill the form", "warn");
            }
            else{
                $.ajax({
                    url : '<?= site_url('admin/laporan-aruskas/getaruskas')?>',
                    type : 'POST',
                    data : {start_date : start_date,
                            end_date : end_date,
                            account_number : account_number}
                }).done(function(r){
                    datatable.clear().draw();
                    $.each(r, function(i, data){
                        datatable.row.add([data.parent_name, '&ensp;&ensp;' +data.account_name, data.balance]).draw();
                    });

                    datatable.draw();
                });
            }
        });
    });
</script>