<style>
    tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
           

            <section class="card">
                <header class="card-header">
                    Laporan Laba Rugi
                </header>
                <div class="card-body">
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <div class="row">
                                <div class="col">
                                    <input type="date" name="start_date" class="form-control" placeholder="Start Date">
                                </div>                                
                                <div class="col">
                                    <input type="date" name="end_date" class="form-control" placeholder="End Date">
                                </div>
                                <div class="col">
                                    <button class="btn btn-primary btnSubmit" type="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover datatables">
                            <thead>
                                <tr>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Account</th>
                                    <th scope="col">Balance</th>
                                </tr> 
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
    </div>
</div>
