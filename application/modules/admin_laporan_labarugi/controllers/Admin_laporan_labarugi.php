<?php defined('BASEPATH') or exit('No direct script access allowed');
        class Admin_laporan_labarugi extends Backend_Controller
        {
            public function __construct()
            {
                parent::__construct();
                $this->load->model('Admin_laporan_labarugi_model');
                $this->breadcrumb->add('Admin laporan labarugi',base_url().'admin_laporan_labarugi');
                $this->data['title'] = 'Admin laporan labarugi';
            }
            final public function index()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                
                $this->data['header'] = array(); //load aditional stylesheets
                $this->data['js_library'] = array(); //load aditional js library
                $this->data['js_script'] = array('admin_laporan_labarugi/js/datatables'); //load aditional js script
                $this->_render_page('admin_laporan_labarugi/html/index', $this->data);
            }
            public function getLabaRugi()
            {
                $data = $this->input->post(NULL);
                $laporan = $this->Admin_laporan_labarugi_model->labarugi($data['start_date'], $data['end_date']);    
                $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output(json_encode($laporan));
            }
            /**
             *Data Services 
            **/
            public function datatables()
            {
                if($this->input->server('REQUEST_METHOD')=='POST'){
                    $this->load->library('Datatables');
                    $this->datatables->select('');
                    $this->datatables->from('');
                    $this->datatables->add_column(
                        'action',
                        '<a class="btn btn-icon btn-sm btn-success mr-1" href="" title="edit">
                         <i class="fa fa-pencil">
                         </i>
                         <a class="btn btn-icon btn-sm btn-danger mr-1" href="" title="delete">
                         <i class="fa fa-trash-o">
                         </i>',
                        "id");
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(200)
                        ->set_output($this->datatables->generate());
                }else{
                    $this->output
                        ->set_content_type('application/json')
                        ->set_status_header(401)
                        ->set_output(json_encode(['message'=>'Cannot serve data.','error'=>'Method not allowed']));
                }
            }

            /**
             * we use final to indicate that this function will be included to ACL table 
             **/

            final public function add()
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }

                //start your code here

            }
            final public function edit($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            final public function delete($id)
            {
                //get permissions for this method
                if(! $this->ion_auth_acl->has_permission($this->router->fetch_class()."/".$this->router->fetch_method())){
                    redirect('errors/error_403','refresh');
                }
                if( ! $id ){
                    if ($this->agent->is_referral())
                    {
                        redirect($this->agent->referrer());
                    }
                    else{
                        redirect('admin_dashboard');
                    }
                }

                //start your code here
            }
            
        }