-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Des 2020 pada 00.53
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `frozen-pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'member', 'Member');

-- --------------------------------------------------------

--
-- Struktur dari tabel `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT current_timestamp(),
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1608248993, 1608248993),
(2, 2, 1, 0, 1608248993, 1608248993),
(3, 1, 2, 1, 1608248994, 1608248994),
(4, 2, 2, 0, 1608248994, 1608248994),
(5, 1, 3, 1, 1608248994, 1608248994),
(6, 2, 3, 0, 1608248994, 1608248994),
(7, 1, 4, 1, 1608248994, 1608248994),
(8, 2, 4, 0, 1608248994, 1608248994),
(9, 1, 5, 1, 1608248994, 1608248994),
(10, 2, 5, 0, 1608248994, 1608248994),
(11, 1, 6, 1, 1608248994, 1608248994),
(12, 2, 6, 0, 1608248994, 1608248994),
(13, 1, 7, 1, 1608248994, 1608248994),
(14, 2, 7, 0, 1608248994, 1608248994),
(15, 1, 8, 1, 1608248994, 1608248994),
(16, 2, 8, 0, 1608248994, 1608248994),
(17, 1, 9, 1, 1608248994, 1608248994),
(18, 2, 9, 0, 1608248994, 1608248994),
(19, 1, 10, 1, 1608248994, 1608248994),
(20, 2, 10, 0, 1608248994, 1608248994),
(21, 1, 11, 1, 1608248994, 1608248994),
(22, 2, 11, 0, 1608248994, 1608248994),
(23, 1, 12, 1, 1608248994, 1608248994),
(24, 2, 12, 0, 1608248994, 1608248994),
(25, 1, 13, 1, 1608248994, 1608248994),
(26, 2, 13, 0, 1608248994, 1608248994),
(27, 1, 14, 1, 1608248994, 1608248994),
(28, 2, 14, 0, 1608248994, 1608248994),
(29, 1, 15, 1, 1608248994, 1608248994),
(30, 2, 15, 0, 1608248994, 1608248994),
(31, 1, 16, 1, 1608248994, 1608248994),
(32, 2, 16, 0, 1608248994, 1608248994),
(33, 1, 17, 1, 1608248994, 1608248994),
(34, 2, 17, 0, 1608248994, 1608248994),
(35, 1, 18, 1, 1608248994, 1608248994),
(36, 2, 18, 0, 1608248994, 1608248994),
(37, 1, 19, 1, 1608248994, 1608248994),
(38, 2, 19, 0, 1608248994, 1608248994),
(39, 1, 20, 1, 1608248994, 1608248994),
(40, 2, 20, 0, 1608248994, 1608248994),
(41, 1, 21, 1, 1608248994, 1608248994),
(42, 2, 21, 0, 1608248994, 1608248994),
(43, 1, 22, 1, 1608248994, 1608248994),
(44, 2, 22, 0, 1608248994, 1608248994),
(45, 1, 23, 1, 1608248994, 1608248994),
(46, 2, 23, 0, 1608248994, 1608248994),
(47, 1, 24, 1, 1608248994, 1608248994),
(48, 2, 24, 0, 1608248994, 1608248994),
(49, 1, 25, 1, 1608248994, 1608248994),
(50, 2, 25, 0, 1608248994, 1608248994),
(51, 1, 26, 1, 1608248994, 1608248994),
(52, 2, 26, 0, 1608248994, 1608248994),
(53, 1, 27, 1, 1608248994, 1608248994),
(54, 2, 27, 0, 1608248994, 1608248994),
(55, 1, 28, 1, 1608248994, 1608248994),
(56, 2, 28, 0, 1608248994, 1608248994),
(57, 1, 29, 1, 1608248994, 1608248994),
(58, 2, 29, 0, 1608248994, 1608248994),
(59, 1, 30, 1, 1608248994, 1608248994),
(60, 2, 30, 0, 1608248994, 1608248994),
(61, 1, 31, 1, 1608248994, 1608248994),
(62, 2, 31, 0, 1608248994, 1608248994),
(63, 1, 32, 1, 1608248994, 1608248994),
(64, 2, 32, 0, 1608248994, 1608248994),
(65, 1, 33, 1, 1608248994, 1608248994),
(66, 2, 33, 0, 1608248994, 1608248994),
(67, 1, 34, 1, 1608248994, 1608248994),
(68, 2, 34, 0, 1608248994, 1608248994),
(69, 1, 35, 1, 1608248994, 1608248994),
(70, 2, 35, 0, 1608248994, 1608248994),
(71, 1, 36, 1, 1608248994, 1608248994),
(72, 2, 36, 0, 1608248994, 1608248994),
(73, 1, 37, 1, 1608248994, 1608248994),
(74, 2, 37, 0, 1608248994, 1608248994),
(75, 1, 38, 1, 1608248994, 1608248994),
(76, 2, 38, 0, 1608248994, 1608248994),
(77, 1, 39, 1, 1608248994, 1608248994),
(78, 2, 39, 0, 1608248994, 1608248994),
(79, 1, 40, 1, 1608248994, 1608248994),
(80, 2, 40, 0, 1608248994, 1608248994),
(81, 1, 41, 1, 1608248994, 1608248994),
(82, 2, 41, 0, 1608248994, 1608248994);

-- --------------------------------------------------------

--
-- Struktur dari tabel `group_menu`
--

CREATE TABLE `group_menu` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `group_menu`
--

INSERT INTO `group_menu` (`id`, `group_id`, `menu_id`, `active`, `created`, `updated`) VALUES
(1, 1, 1, 1, '2020-09-08 20:33:16', '0000-00-00 00:00:00'),
(2, 2, 1, 0, '2020-09-08 20:33:16', '0000-00-00 00:00:00'),
(3, 3, 1, 0, '2020-09-08 20:33:16', '0000-00-00 00:00:00'),
(4, 1, 2, 1, '2020-09-08 20:33:53', '0000-00-00 00:00:00'),
(5, 2, 2, 0, '2020-09-08 20:33:53', '0000-00-00 00:00:00'),
(6, 3, 2, 0, '2020-09-08 20:33:53', '0000-00-00 00:00:00'),
(7, 1, 3, 1, '2020-09-08 20:34:20', '0000-00-00 00:00:00'),
(8, 2, 3, 0, '2020-09-08 20:34:20', '0000-00-00 00:00:00'),
(9, 3, 3, 0, '2020-09-08 20:34:20', '0000-00-00 00:00:00'),
(10, 1, 4, 1, '2020-09-08 20:34:34', '0000-00-00 00:00:00'),
(11, 2, 4, 0, '2020-09-08 20:34:34', '0000-00-00 00:00:00'),
(12, 3, 4, 0, '2020-09-08 20:34:34', '0000-00-00 00:00:00'),
(13, 1, 5, 1, '2020-09-08 20:34:50', '0000-00-00 00:00:00'),
(14, 2, 5, 0, '2020-09-08 20:34:50', '0000-00-00 00:00:00'),
(15, 3, 5, 0, '2020-09-08 20:34:50', '0000-00-00 00:00:00'),
(16, 1, 6, 1, '2020-09-09 06:40:38', '0000-00-00 00:00:00'),
(17, 2, 6, 0, '2020-09-09 06:40:38', '0000-00-00 00:00:00'),
(18, 1, 7, 1, '2020-09-09 06:40:54', '0000-00-00 00:00:00'),
(19, 2, 7, 0, '2020-09-09 06:40:54', '0000-00-00 00:00:00'),
(20, 1, 8, 1, '2020-09-09 06:53:14', '0000-00-00 00:00:00'),
(21, 2, 8, 0, '2020-09-09 06:53:14', '0000-00-00 00:00:00'),
(22, 1, 9, 1, '2020-09-09 06:53:59', '0000-00-00 00:00:00'),
(23, 2, 9, 0, '2020-09-09 06:53:59', '0000-00-00 00:00:00'),
(24, 1, 10, 1, '2020-09-09 06:54:24', '0000-00-00 00:00:00'),
(25, 2, 10, 0, '2020-09-09 06:54:24', '0000-00-00 00:00:00'),
(26, 1, 11, 1, '2020-09-09 06:54:50', '0000-00-00 00:00:00'),
(27, 2, 11, 0, '2020-09-09 06:54:50', '0000-00-00 00:00:00'),
(28, 1, 12, 1, '2020-09-09 06:55:09', '0000-00-00 00:00:00'),
(29, 2, 12, 0, '2020-09-09 06:55:09', '0000-00-00 00:00:00'),
(30, 1, 13, 1, '2020-09-09 06:55:33', '0000-00-00 00:00:00'),
(31, 2, 13, 0, '2020-09-09 06:55:33', '0000-00-00 00:00:00'),
(32, 1, 14, 1, '2020-09-09 06:56:01', '0000-00-00 00:00:00'),
(33, 2, 14, 0, '2020-09-09 06:56:01', '0000-00-00 00:00:00'),
(34, 1, 15, 1, '2020-09-09 06:56:24', '0000-00-00 00:00:00'),
(35, 2, 15, 0, '2020-09-09 06:56:24', '0000-00-00 00:00:00'),
(36, 1, 16, 1, '2020-09-09 06:57:58', '0000-00-00 00:00:00'),
(37, 2, 16, 1, '2020-09-09 06:57:58', '0000-00-00 00:00:00'),
(38, 1, 17, 1, '2020-09-09 06:59:30', '0000-00-00 00:00:00'),
(39, 2, 17, 0, '2020-09-09 06:59:30', '0000-00-00 00:00:00'),
(40, 1, 18, 1, '2020-09-09 07:43:17', '0000-00-00 00:00:00'),
(41, 2, 18, 0, '2020-09-09 07:43:17', '0000-00-00 00:00:00'),
(42, 1, 19, 1, '2020-09-09 09:40:06', '0000-00-00 00:00:00'),
(43, 2, 19, 0, '2020-09-09 09:40:06', '0000-00-00 00:00:00'),
(44, 1, 20, 1, '2020-09-09 09:41:27', '0000-00-00 00:00:00'),
(45, 2, 20, 0, '2020-09-09 09:41:27', '0000-00-00 00:00:00'),
(46, 1, 21, 1, '2020-09-09 10:00:24', '0000-00-00 00:00:00'),
(47, 2, 21, 1, '2020-09-09 10:00:24', '0000-00-00 00:00:00'),
(48, 1, 22, 1, '2020-09-16 14:27:18', '0000-00-00 00:00:00'),
(49, 2, 22, 0, '2020-09-16 14:27:18', '0000-00-00 00:00:00'),
(50, 1, 23, 1, '2020-09-16 14:28:10', '0000-00-00 00:00:00'),
(51, 2, 23, 0, '2020-09-16 14:28:10', '0000-00-00 00:00:00'),
(52, 1, 24, 1, '2020-09-16 16:09:49', '0000-00-00 00:00:00'),
(53, 2, 24, 0, '2020-09-16 16:09:49', '0000-00-00 00:00:00'),
(54, 1, 25, 1, '2020-09-23 16:05:55', '0000-00-00 00:00:00'),
(55, 2, 25, 0, '2020-09-23 16:05:55', '0000-00-00 00:00:00'),
(56, 1, 26, 1, '2020-10-12 19:56:44', '0000-00-00 00:00:00'),
(57, 2, 26, 0, '2020-10-12 19:56:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `group_widget`
--

CREATE TABLE `group_widget` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `widget_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `group_widget`
--

INSERT INTO `group_widget` (`id`, `group_id`, `widget_id`, `active`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL,
  `has_child` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `menu_order` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `parent`, `has_child`, `active`, `menu_order`, `created`, `updated`) VALUES
(3, 'User Management', 'fa-users', '', 0, 1, 1, 1, '2020-09-08 20:34:20', '2020-09-16 09:36:54'),
(4, 'Users', '', 'admin/users', 3, 0, 1, 0, '2020-09-08 20:34:34', '2020-10-20 01:41:15'),
(6, 'System Configuration', 'fa-cogs', '', 0, 1, 1, 0, '2020-09-09 06:40:38', '2020-09-09 01:52:38'),
(7, 'Menu Management', '', 'admin/menu', 6, 0, 1, 0, '2020-09-09 06:40:54', '2020-12-18 06:51:10'),
(8, 'Permissions', 'fa-lock', '', 0, 1, 1, 2, '2020-09-09 06:53:14', '2020-09-16 09:37:18'),
(9, 'permissions', '', 'admin/permissions', 8, 0, 1, 0, '2020-09-09 06:53:59', '2020-12-18 06:51:25'),
(10, 'Categories', '', 'admin/permissions_categories', 8, 0, 1, 0, '2020-09-09 06:54:24', '2020-12-18 06:51:41'),
(25, 'Groups', '', 'admin-groups', 3, 0, 1, 0, '2020-09-23 16:05:55', '0000-00-00 00:00:00'),
(26, 'Site Options', '', 'admin/site-options', 6, 0, 1, 0, '2020-10-12 19:56:44', '2020-12-18 06:50:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(100) NOT NULL,
  `perm_name` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `perm_key`, `perm_name`, `category_id`) VALUES
(1, 'admin_groups/index', 'Index', 2),
(2, 'admin_groups/add', 'Add', 2),
(3, 'admin_groups/edit', 'Edit', 2),
(4, 'admin_groups/delete', 'Delete', 2),
(5, 'admin_groups/update_permission', 'Update_permission', 2),
(6, 'admin_menu/index', 'Index', 3),
(7, 'admin_menu/add', 'Add', 3),
(8, 'admin_menu/edit', 'Edit', 3),
(9, 'admin_menu/delete', 'Delete', 3),
(10, 'admin_modules/index', 'Index', 4),
(11, 'admin_permissions/index', 'Index', 5),
(12, 'admin_permissions/rebuild_acl', 'Rebuild_acl', 5),
(13, 'admin_permissions/flush_acl', 'Flush_acl', 5),
(14, 'admin_permissions/add', 'Add', 5),
(15, 'admin_permissions/edit', 'Edit', 5),
(16, 'admin_permissions/delete', 'Delete', 5),
(17, 'admin_permissions_categories/index', 'Index', 6),
(18, 'admin_permissions_categories/add', 'Add', 6),
(19, 'admin_permissions_categories/edit', 'Edit', 6),
(20, 'admin_permissions_categories/delete', 'Delete', 6),
(21, 'admin_site_options/index', 'Index', 8),
(22, 'admin_site_options/save_info', 'Save_info', 8),
(23, 'admin_site_options/save_greeting', 'Save_greeting', 8),
(24, 'admin_site_options/save_smtp', 'Save_smtp', 8),
(25, 'admin_users/index', 'Index', 9),
(26, 'admin_users/activate', 'Activate', 9),
(27, 'admin_users/deactivate', 'Deactivate', 9),
(28, 'admin_users/create_user', 'Create_user', 9),
(29, 'admin_users/edit_user', 'Edit_user', 9),
(30, 'admin_users/delete_user', 'Delete_user', 9),
(31, 'admin_users/create_group', 'Create_group', 9),
(32, 'admin_users/edit_group', 'Edit_group', 9),
(33, 'admin_wablast/index', 'Index', 10),
(34, 'admin_wablast/kirim', 'Kirim', 10),
(35, 'admin_wablast/edit', 'Edit', 10),
(36, 'admin_wablast/delete', 'Delete', 10),
(37, 'admin_widgets/index', 'Index', 11),
(38, 'admin_widgets/add', 'Add', 11),
(39, 'admin_widgets/edit', 'Edit', 11),
(40, 'admin_widgets/delete', 'Delete', 11),
(41, 'widget/index', 'Index', 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions_categories`
--

CREATE TABLE `permissions_categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(30) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `permissions_categories`
--

INSERT INTO `permissions_categories` (`id`, `cat_name`, `created`, `updated`) VALUES
(1, 'Admin_dashboard', '2020-12-18 06:49:53', '0000-00-00 00:00:00'),
(2, 'Admin_groups', '2020-12-18 06:49:53', '0000-00-00 00:00:00'),
(3, 'Admin_menu', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(4, 'Admin_modules', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(5, 'Admin_permissions', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(6, 'Admin_permissions_categories', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(7, 'Admin_profile', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(8, 'Admin_site_options', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(9, 'Admin_users', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(10, 'Admin_wablast', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(11, 'Admin_widgets', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(12, 'Frontend_errors', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(13, 'Home', '2020-12-18 06:49:54', '0000-00-00 00:00:00'),
(14, 'Widget', '2020-12-18 06:49:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `site_options`
--

CREATE TABLE `site_options` (
  `id` int(11) NOT NULL,
  `option_key` varchar(20) NOT NULL,
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`option_value`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `site_options`
--

INSERT INTO `site_options` (`id`, `option_key`, `option_value`) VALUES
(1, 'site_info', '{\"name\":\"\",\"address\":\"\",\"phone\":\"\",\"email\":\"\",\"logo\":\"\"}'),
(2, 'smtp', '{\"host\":\"smtp.gmail.com\",\"username\":\"\",\"password\":\"\",\"security\":\"tls\",\"port\":\"587\",\"format\":\"html\",\"from_email\":\"\",\"from_name\":\"\",\"reply_to\":\"\",\"reply_to_name\":\"\"}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$2rLvbvYSBERX6/OEXkod2uBYxnl3cR67u4pu/wZCY1s6739sTfScC', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1603163773, 1, 'Admin', 'istrator', 'ADMIN', '0', 'avatar13.png'),
(2, '::1', 'member', '$2y$10$80aZ4rlPNVxHAMjVJszXR.7S.c37eKS9zAGdpwvYmKNqnpf8V2Qx2', 'member@email.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1603150930, NULL, 1, 'member', 'Member', '', '', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `widgets`
--

INSERT INTO `widgets` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'member');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indeks untuk tabel `group_menu`
--
ALTER TABLE `group_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `group_widget`
--
ALTER TABLE `group_widget`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indeks untuk tabel `permissions_categories`
--
ALTER TABLE `permissions_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cat_name` (`cat_name`);

--
-- Indeks untuk tabel `site_options`
--
ALTER TABLE `site_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `option_key` (`option_key`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_email` (`email`) USING BTREE,
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`) USING BTREE,
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`) USING BTREE,
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`) USING BTREE;

--
-- Indeks untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  ADD KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE;

--
-- Indeks untuk tabel `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- Indeks untuk tabel `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT untuk tabel `group_menu`
--
ALTER TABLE `group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT untuk tabel `group_widget`
--
ALTER TABLE `group_widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT untuk tabel `permissions_categories`
--
ALTER TABLE `permissions_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `site_options`
--
ALTER TABLE `site_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
