# ci-gt-starter

Boilerplate untuk semua project Gink Technology
diharapkan menjadi standar yang bisa diterapkan untuk project-project selanjutnya untuk membantu pengerjaan agar lebih cepat

## INFO

> Boilerplate atau project starter ini dibangun menggunakan bahasa pemrograman PHP dan framework Codeigniter 3.1.10

>Arsitektur
1. PHP v7.2 atau diatasnya
2. MariaDB v10 atau diatasnya
3. Konsep HMVC
4. Codeigniter version 3.1.10

### Codeigniter Library
1. [Ion Auth](http://benedmunds.com/ion_auth/)












**Semua insan Gink Technology berhak berkontribusi pada boilerplate ini**